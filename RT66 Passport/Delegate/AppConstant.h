//---------------------------------------------------------------------------------
#pragma mark - COLORs
//---------------------------------------------------------------------------------
#define		HEXCOLOR(c) [UIColor colorWithRed:((c>>24)&0xFF)/255.0 green:((c>>16)&0xFF)/255.0 blue:((c>>8)&0xFF)/255.0 alpha:((c)&0xFF)/255.0]
#define		COLOR_BACKGROUND                    HEXCOLOR(0x22A4DAFF)
#define		COLOR_NAVIGATIONBAR                 HEXCOLOR(0xF5605CFF)
#define		COLOR_NAVIGATIONBARTEXT             HEXCOLOR(0xfbffe2FF)
#define     COLOR_NAV_BAR                       [UIColor colorWithRed:(41.0/255.0) green:(171.0/255) blue:(226.0/255) alpha:1.0]
#define     COLOR_HOME_BG                       [UIColor colorWithRed:(235.0/255.0) green:(235.0/255) blue:(235.0/255) alpha:1.0]
#define     COLOR_NAV_BLUE                      [UIColor colorWithRed:(0.0/255.0) green:(120.0/255) blue:(255.0/255) alpha:1.0]
#define     COLOR_NAV_BLUE                      [UIColor colorWithRed:(0.0/255.0) green:(120.0/255) blue:(255.0/255) alpha:1.0]
#define     COLOR_NAV_WHITE                     [UIColor colorWithRed:(255.0/255.0) green:(255.0/255) blue:(255.0/255) alpha:1.0]
#define     COLOR_SLIDER_BG                     [UIColor colorWithRed:(41.0/255.0) green:(41.0/255) blue:(41.0/255) alpha:1.0]
#define     COLOR_BTN_BACKGROUND                [UIColor colorWithRed:126/225.0 green:201/255.0 blue:55/255.0 alpha:1.0]
//------------------------------------------------------------------------------------------------------

#pragma mark - SYSTEM_VERSION
//---------------------------------------------------------------------------------
#define		SYSTEM_VERSION								[[UIDevice currentDevice] systemVersion]
#define		SYSTEM_VERSION_EQUAL_TO(v)					([SYSTEM_VERSION compare:v options:NSNumericSearch] == NSOrderedSame)
#define		SYSTEM_VERSION_GREATER_THAN(v)				([SYSTEM_VERSION compare:v options:NSNumericSearch] == NSOrderedDescending)
#define		SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)	([SYSTEM_VERSION compare:v options:NSNumericSearch] != NSOrderedAscending)
#define		SYSTEM_VERSION_LESS_THAN(v)					([SYSTEM_VERSION compare:v options:NSNumericSearch] == NSOrderedAscending)
#define		SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)		([SYSTEM_VERSION compare:v options:NSNumericSearch] != NSOrderedDescending)
//---------------------------------------------------------------------------------

#pragma mark - APP KEYs
//---------------------------------------------------------------------------------
#define     FIREBASE_STORAGE                    @"gs://rt66-passport.appspot.com"
#define     PASSPORT_STORAGE                    @"passport_picture"
#define     PROFILEPHOTO_STORAGE                @"profile_picture"
#define     SOCIALSHARE_STORAGE                 @"socialshare_picture"
#define		ONESIGNAL_APPID						@"dfa23d25-0358-4c9f-9f69-352065815bf8"
#define		GOOGLEMAPKEY                        @"AIzaSyCsBc8Je45gUrhysLQdOlCLpB2SXqxgypE"
//---------------------------------------------------------------------------------

#pragma mark - Firebase Analytics
//---------------------------------------------------------------------------------
#define     CRASH_NETWORKERROR                  @"network_error"
#define     ANALY_EVENT_ENTERGEOFENCE           @"enter_geofence"
#define     ANALY_PARAM_ENTEREDLOCATION_NAME    @"Location"
#define     ANALY_EVENT_TAKEPHOTO               @"take_photo"
#define     ANALY_EVENT_EXITGEOFENCE            @"leave_geofence"
#define     ANALY_EVENT_INTERVAL_GEOGENCE       @"interval"
#define     ANALY_PARAM_INTERVAL                @"Interval"
#define     ANALY_USER_NATIONALITY              @"Country_origin"
#define     ANALY_EVENT_ENTERBEACON             @"enter_beacon"
#define     ANALY_EVENT_EXITBEACON              @"leave_beacon"
#pragma mark - Languages
//---------------------------------------------------------------------------------
#define     ENGLISH                             0                       //  Int
#define     SPANISH                             1                       //  Int
#define     FRENCH                              2                       //  Int
#define     ITALIAN                             3                       //  Int
#define     CHINESE                             4                       //  Int
#define     JAPANESE                            5                       //  Int
#pragma mark - FKEYs
//---------------------------------------------------------------------------------
#define		FOBJECTID                           @"objectId"				//	String*
#define		FLATITUDE                           @"latitude"				//	String*
#define		FLONGITUDE                          @"longitude"			//	String*
//---------------------------------------------------------------------------------

#pragma mark - FUSER
//---------------------------------------------------------------------------------
#define		FUSER_PATH							@"User"					//	Path name*
#define		FUSER_OBJECTID						@"objectId"				//	String*
#define		FUSER_EMAIL							@"email"				//	String*
#define		FUSER_FULLNAME						@"fullname"				//	String*
#define		FUSER_GENDER					    @"gender"				//	String*
#define		FUSER_BIRTHDAY					    @"birthday"				//	String*

#define		FUSER_COUNTRYCODE					@"country_code"         //	String
#define		FUSER_COUNTRYNAME					@"country_name"			//	String*
#define		FUSER_ADDRESS						@"address"				//	String*
#define		FUSER_MYTRIPS						@"mytrips"				//	Array*
#define		FUSER_MYSTAMPS						@"mystamps"				//	Array*
#define		FUSER_MYREQUESTPASSPORTS            @"myrequestpassports"	//	Array*
#define		FUSER_PICTURE						@"picture"				//	String*
#define		FUSER_WASEMAILED					@"wasemailed"			//	String*
#define		FUSER_ATTRACTIONPHOTOS              @"attractionphotos"     //  array*
#define     FUSER_STARTLOCATIONID               @"startlocationId"      //  String*
#define     FUSER_NOTIFICATIONALLOW             @"notificationallow"    //  BOOL
#define     FUSER_LANGUAGE                      @"language"             //  Int
#define     FUSER_CREATEDAT                     @"createdAt"            //  Timestamp*
#define     FUSER_UPDATEDAT                     @"updatedAt"            //  Timestamp*
// Survey
//---------------------------------------------------------------------------------
#define     FUSER_CITY                          @"city"                 //  String*
#define     FUSER_STATE                         @"state"                //  String*

#define     FUSER_NUM_ADULT                     @"adultnum"             //  Int
#define     FUSER_NUM_MINOR                     @"minornum"             //  Int

#define     FUSER_STARTLOCATION                 @"startlocation"        //  String*
#define     FUSER_ENDLOCATION                   @"endlocation"          //  String*

#define     FUSER_FOLLOWLOCATIONS               @"followlocations"      //  Array

#define     FUSER_SPENDNIGHTS                   @"spendnights"          //  String*

#define     FUSER_TRAVELDAY                     @"traveldays"           //  Int

#define     FUSER_NUMOFARIZONA                  @"numofarizona"         //  Int
#define     FUSER_ISDESTINATION                 @"isdestination"        //  BOOL
#define     FUSER_DESTINATION                   @"destination"          //  String*
#define     FUSER_COMMENTVISIT                  @"commentofvisiting"    //  String*
//---------------------------------------------------------------------------------

#pragma mark - FLOCATION
//---------------------------------------------------------------------------------
#define		FLOCATION_PATH                      @"Location"             //	Path name
#define		FLOCATION_ADDRESS                   @"address"              //	String*
#define		FLOCATION_NAME                      @"name"                 //	String*
#define		FLOCATION_PHOTOS                    @"photos"               //	Array*
#define     FLOCATION_DESC                      @"desc"                 //  String*
#define		FLOCATION_WEBSITE                   @"website"              //	String*
#define		FLOCATION_PHONE                     @"phone"                //	String*
#define		FLOCATION_SECTOR					@"sector"				//	Int
#define     FLOCATION_ORDER                     @"order"                //  Long
#define     FLOCATION_OPENHOURS                 @"openHours"            //  Array*

//---------------------------------------------------------------------------------
#define     FCOMMUNITY_PATH                     @"Community"            //    Path name
#define     FCOMMUNITY_NAME                     @"name"                 //    String*
#define     FCOMMUNITY_DESC                     @"desc"                 //    String*
#define     FCOMMUNITY_ICON                     @"icon"                 //    String*
//---------------------------------------------------------------------------------

#pragma mark - FSTAMP
//---------------------------------------------------------------------------------
#define		FSTAMP_PATH                         @"Stamp"                //	Path name
#define		FSTAMP_NAME                         @"name"                 //	String*
#define		FSTAMP_DISPLAY_NAME                 @"display_name"         //	String*
#define		FSTAMP_DESC                         @"desc"                 //	String*
#define		FSTAMP_ICON                         @"icon"                 //	String*
#define		FSTAMP_LOCATIONID                   @"locationId"           //	String*
#define		FSTAMP_WEBSITE                      @"website"              //	String*
//---------------------------------------------------------------------------------

#pragma mark - FBEACON
//---------------------------------------------------------------------------------
#define     FBEACON_PATH                        @"Beacon"               //    Path name
#define     FBEACON_LOCATIONID                  @"locationId"           //    String*
#define     FBEACON_UUID                        @"uuid"                 //    String*
#define     FBEACON_MAJOR                       @"major"                //    int
#define     FBEACON_MINOR                       @"minor"                //    int

#pragma mark - NOTIFICATION KEYs
//---------------------------------------------------------------------------------
#define		NOTI_APP_STARTED			        @"NotificationAppStarted"
#define		NOTI_USER_LOGGED_IN			        @"NotificationUserLoggedIn"
#define		NOTI_USER_LOGGED_OUT		        @"NotificationUserLoggedOut"
#define		NOTI_MEONLINE     			        @"meonline"
#define		NOTI_MEOFFLINE     			        @"meoffline"
#define		NOTI_APP_TIMERESET			        @"ApptimeReset"
#define     NOTI_GOMAPSCREEN                    @"gomapscreen"
#define     NOTI_GONEXTSTOP                     @"gonextstop"
#define		NOTI_GEOFENCE                       @"eventbusgeofence"
#define     NOTI_GEOFENCEADDED                  @"eventbusgeofenceadded"
#define     NOTI_UPDATELOCATION                 @"updatelocation"

#define     NOTI_QUESTIONNEXT                   @"questionnext"
#define     NOTI_QUESTIONBACK                   @"questionback"
#define     NOTI_QUESTIONDONE                   @"questiondone"
#define     NOTI_GOCOUNTRYVIEW                  @"gocountryview"

#define     NOTI_CHANGEPROGRESS                 @"changeprogress"
#define     NOTI_SHOWPICKER                     @"showpicker"
#define     NOTI_SELECTSTARTLOCATION            @"selectedstartlocation"
#define     NOTI_SELECTENDLOCATION              @"selectedendlocation"
#define     NOTI_HIDESURVEYTITLE                @"hidetitleofsurvey"
//-----------------------------------------------------------------------------------
#pragma mark - GUIDEs

//-----------------------------------------------------------------------------------

#pragma mark - DEVICE INFO
//---------------------------------------------------------------------------------
#define     IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define     Fontcell (IS_IPAD ? 70 : 45)

#define		SCREEN_WIDTH						[UIScreen mainScreen].bounds.size.width
#define		SCREEN_HEIGHT						[UIScreen mainScreen].bounds.size.height
//-----------------------------------------------------------------------------------

#pragma mark - ERRORs
//---------------------------------------------------------------------------------
#define     ERROR_NETWORK                       @"Sorry, no internet connection"
//-----------------------------------------------------------------------------------

#pragma mark - INSTANCE
//---------------------------------------------------------------------------------
#define     GLOBALINS                           ((Global *)[Global instance])
#define     USERINS                             ((CurrentUser *)[CurrentUser instance])
//-----------------------------------------------------------------------------------

#pragma mark - APP DEFAULT VALUEs
//---------------------------------------------------------------------------------
#define     FUSER_DEFAULTADDRESS                @"SAN FRANCISCO CALIFONIA"
#define     FUSER_DEFAULTLATITUDE               @"37.785870"
#define     FUSER_DEFAULTLONGITUDE              @"-122.406494"

#define		VIDEO_LENGTH						5
#define		PASSWORD_LENGTH						6
#define		DOWNLOAD_TIMEOUT					300

#define		STATUS_LOADING						1
#define		STATUS_SUCCEED						2
#define		STATUS_MANUAL						3

#define		NETWORK_MANUAL						1
#define		NETWORK_WIFI						2
#define		NETWORK_ALL							3

#define		DEL_ACCOUNT_NONE					1
#define		DEL_ACCOUNT_ONE						2

#define		LOGIN_EMAIL							@"Email"
#define		LOGIN_FACEBOOK						@"Facebook"
#define		LOGIN_GOOGLE						@"Google"

#define     GEORADIUS                           500
#define     OWNEREMAIL                          @"anzelmo.melissa@gmail.com" //@"team@packerlabs.com"
#define     MAPZOOM_DEFAUL                      17.0
#define     MAPZOOM_DETAIL                      18.0
#define     SENTENCEPOINT                       @"&&&&&&&&&&"
//---------------------------------------------------------------------------------

#pragma mark - APIs
//---------------------------------------------------------------------------------
#define		API_IPCOUNTRY                       @"http://freegeoip.net/json/"
#define		API_COUNTRYFLAG                     @"https://raw.githubusercontent.com/stevenrskelton/flag-icon/master/png/75/country-squared/"

#define		API_OVERAYMAP                       @"http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&appid=e801b6cea4d8eec053e2bab23c0afbca"
#define		API_WHEATHERICON                    @"http://openweathermap.org/img/w/%@.png"

#pragma mark - OTHERS
#define     ENTER_LOCATIONNAME                  @"enterlocationname"
#define     ENTER_STAMP                         @"enterstamp"
#define     ENTER_ISENTER                       @"enterisenter"
#define     STAMPPHOTOS                         @"stampphotos"          //    String*

#pragma mark - USERDEFAULT KEY
#define     DEFAULT_GEOFENCEMODEL               @"geofencemodel"          // String*

// jpacker@siu.edu | A5kgodwhy.

// dispatch_async(dispatch_queue_create(nil, DISPATCH_QUEUE_SERIAL), ^{
//
// });

//dispatch_async(dispatch_get_main_queue(), ^{
//
//});

//dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//    dispatch_async(dispatch_get_main_queue(), ^{
//    });
//});

//dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC);
//dispatch_after(time, dispatch_get_main_queue(), ^(void){
//
//});

//static dispatch_once_t once;
//dispatch_once(&once, ^{
//});

//[self performSelector:@selector(delayedReload) withObject:nil afterDelay:0.25];
