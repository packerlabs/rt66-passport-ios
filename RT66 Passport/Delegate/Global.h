//
//  Global.h
//  PWFM
//
//  Created by My Star on 5/8/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "utilities.h"
#import "DBLocation.h"
#import "DBStamp.h"
#import "QuestionVC.h"
@interface Global : NSObject
#pragma mark - UIs
@property UIViewController *mainVC;
@property CGFloat naviHeight;

#pragma mark - Global Parameters
@property NSInteger badgeNum;
@property NSInteger selectedQuestionViewIndex;
@property NSArray *locationNames;
@property NSArray *communityNames;
@property NSArray *originLanguages;
@property NSArray *languages;
@property NSArray *langImages;
@property int userLanguage;
#pragma mark - APP FLAGs
@property BOOL isAppStart;
@property BOOL isEnterLocation;

#pragma mark - REALM
@property RLMResults *dbstamps;
@property RLMResults *dblocations;
@property NSInteger stampCount;
@property NSInteger locationCount;

#pragma mark - GEOFENCE
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

#pragma mark - MAP SET
@property GMSMarker *redeemedMarker;
@property GMSMarker *unredeemedMarker;
@property NSMutableArray *locationMarkers;

#pragma mark - APP CONSTANT
@property UIImage *placeholder;

#pragma mark - USER INFO
@property NSMutableArray *stampPhotos;

#pragma mark - CLASS METHODs
+(Global *)instance;
+(void)roundBorderSet:(id)senser borderColor:(UIColor *)color;
+(void)roundCornerSet:(id)senser;
+(BOOL) validEmail:(NSString *) strEmail;
+(void)confirmMessage :(UIViewController*)taget  :(NSString *)title :(NSString *)message;
+(void)confirmMessage:(NSString *)message;
+(NSString *)getDateFromTimestamp:(long long)timestamp;
+(NSString *)imageToNSString:(UIImage *)image;
+(UIImage *)stringToUIImage:(NSString *)string;
+(void)localNofiticatonWithTitle:(NSString *)title badgNumber :(int)num;
+(void)goPushFromNVC :(UIViewController *)fromNVC to:(UIViewController*)toVC;
+(void)goPushFromVC :(UIViewController *)fromVC to:(UIViewController*)toVC;
+(void)gotoPresentFromVC :(UIViewController *)fromVC to:(UIViewController*)toVC;
+(void)popupVCwithFrom: (UIViewController *)fromVC to:(UIViewController *)toVC;

#pragma mark - INSTANCE METHODs
-(void)parameterInit;
-(void)badgeInit;
@end
