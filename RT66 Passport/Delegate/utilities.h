

#ifndef app_utilities_h
#define app_utilities_h

#import <Contacts/Contacts.h>
#import <CoreSpotlight/CoreSpotlight.h>
#import <CoreData/CoreData.h>

@import Firebase;
@import FirebaseDatabase;
@import FirebaseAuth;
//LIB
#import <UIKit/UIKit.h>
#pragma mark - Facebook SDK
#import <FBSDKLoginKit/FBSDKLoginManagerLoginResult.h>
#import <FBSDKLoginKit/FBSDKLoginManager.h>
#import <FBSDKCoreKit/FBSDKGraphRequest.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareLinkContent.h>
#import <FBSDKShareKit/FBSDKSharePhoto.h>
#import <FBSDKShareKit/FBSDKSharePhotoContent.h>
#import <FBSDKShareKit/FBSDKShareButton.h>
#import <FBSDKShareKit/FBSDKShareDialog.h>
#import <FBSDKShareKit/FBSDKLikeControl.h>
#import <FBSDKShareKit/FBSDKShareAPI.h>
#import <FBSDKSharekit/FBSDKShareDialogMode.h>
#import <FBSDKAccessToken.h>
#import <Social/Social.h>
#pragma mark - Twitter SDK
#import <TwitterKit/TwitterKit.h>
#pragma mark - Google Map SDK

#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import <LMAddress.h>
#import <LMGeocoder.h>

#pragma mark - Pods
#import <Reachability.h>
#import <Realm/Realm.h>
#import <OneSignal/OneSignal.h>
#import <Firebase/Firebase.h>
#import <AFNetworking.h>
#import <JSONModel.h>

#pragma mark - UI Pods
#import <UIView+Toast.h>
#import <CustomIOSAlertView.h>
#import <MBProgressHUD.h>
#import <ProgressHUD.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <AutoScrollLabel/CBAutoScrollLabel.h>
#import <MMPulseView.h>
#import <TTCardView.h>
#import <IDMPhotoBrowser.h>
#import "HTPressableButton.h"
#import "UIColor+HTColor.h"
#import "CardScrollView.h"
#import <STPopup/STPopup.h>
#import "DraggableViewBackground.h"
#import "ACFloatingTextField.h"
#import <ActionSheetPicker.h>
#pragma mark - Delegates
#import "Global.h"
#import "AppConstant.h"
#import "AppHeadings.h"
#import "Loading.h"

#pragma mark - general1
#import "NotificationCenter.h"
#import "NSDate+Util.h"
#import "NSDictionary+Util.h"
#import "UserDefaults.h"

#pragma mark - general2
#import "Connection.h"
#import "Location.h"

#pragma mark - general3
#import "Dir.h"

#pragma mark - general4
#import "camera.h"
#import "converter.h"
#import "common.h"
#import "PictureView.h"

#pragma mark - backend1
#import "FObject.h"
#import "FUser.h"
#import "CurrentUser.h"
#import "FUser+Util.h"
#import "NSError+Util.h"
#import "MyLabel.h"
#pragma mark - backend2

#import "Locations.h"
#import "Stamps.h"
#import "Communities.h"
#import "Beacons.h"

#pragma mark - backend3
#import "Image.h"

#pragma mark - realms
#import "DBStamp.h"
#import "DBLocation.h"
#import "DBCountry.h"
#import "DBCommunity.h"
#import "DBBeacon.h"
#pragma mark - JSONModel
#import "RequModel.h"
#import "PhotoModel.h"
#import "GeofenceModel.h"
#pragma mark - Beacon
#import "AIBBeaconRegionAny.h"
#import "AIBUtils.h"
#pragma mark - Controllers
#import "NavigationController.h"
#import "MainVC.h"
#import "HomeVC.h"
#import "HomeCell.h"
#import "LocationDetailVC.h"
#import "MapVC.h"
#import "UrPassVC.h"
#import "ProfileVC.h"
#import "EditProfileVC.h"
#import "RequestPassportVC.h"
#import "ViewPhotosVC.h"
#import "DetailPhotoVC.h"
#import "LanguageVC.h"
#endif

