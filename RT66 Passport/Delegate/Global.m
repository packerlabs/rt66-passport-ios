

#import "Global.h"
#import <UIKit/UIKit.h>
@implementation Global

#pragma mark - CLASS METHODs
#pragma mark - Instance
+ (Global *)instance
{
    static Global *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[Global alloc] init];
    });
    return sharedInstance;
}
#pragma mark - Costomize UIs
+(void)roundBorderSet:(id)senser borderColor:(UIColor *)color
{
    [[senser layer] setCornerRadius:5.0f];
    [[senser layer] setMasksToBounds:YES];
    
    [[senser layer] setBorderWidth: 1.f];
    [[senser layer] setBorderColor:color.CGColor];
}
+(void)roundCornerSet:(id)senser
{
    [[senser layer] setCornerRadius:5.0f];
    [[senser layer] setMasksToBounds:YES];
}
#pragma mark - Error/Confirm Alert
+(void)confirmMessage :(UIViewController*)taget  :(NSString *)title :(NSString *)message
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                   }];
    
    
    [alertController addAction:cancelAction];
    
    [taget presentViewController:alertController animated:YES completion:nil];
}
+(void)confirmMessage:(NSString *)message{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}
#pragma mark - Vailed Email Checking
+ (BOOL) validEmail:(NSString *) strEmail {
    NSArray * array = [strEmail componentsSeparatedByString:@"@"];
    if (array.count == 2) {
        array = [array[1] componentsSeparatedByString:@"."];
        if (array.count > 1) {
            return YES;
        }
    }
    return NO;
}
#pragma mark - Get Date from Time stamp
+(NSString *)getDateFromTimestamp:(long long)timestamp{
    NSTimeInterval seconds = timestamp / 1000;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:seconds];
    NSLog(@"ans : %@",date);    // your get answer like  ans :  2015-01-13
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSLog(@"result: %@", [dateFormatter stringFromDate:date]);
    NSString *dateStr = [dateFormatter stringFromDate:date];
    return dateStr;
}

#pragma mark - IMAGE CONVERT
+ (NSString *)imageToNSString:(UIImage *)image
{
    NSData *imageData = UIImagePNGRepresentation(image);
    return [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

+ (UIImage *)stringToUIImage:(NSString *)string
{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:string
                                                      options:NSDataBase64DecodingIgnoreUnknownCharacters];    
    return [UIImage imageWithData:data];
}
#pragma mark - GoToViewController
+(void)goPushFromNVC :(UIViewController *)fromNVC to:(UIViewController*)toVC{
    [fromNVC.navigationController pushViewController:toVC animated:YES];
}
+(void)goPushFromVC :(UIViewController *)fromVC to:(UIViewController*)toVC{
    NavigationController *navController = [[NavigationController alloc] initWithRootViewController:fromVC];
    [navController.navigationController pushViewController:toVC animated:YES];
}
+(void)gotoPresentFromVC :(UIViewController *)fromVC to:(UIViewController*)toVC{
    [fromVC presentViewController:toVC animated:YES completion:nil];
}
#pragma mark - Popup

+(void)popupVCwithFrom: (UIViewController *)fromVC to:(UIViewController *)toVC{
    [STPopupNavigationBar appearance].barTintColor = [UIColor colorWithRed:0.20 green:0.60 blue:0.86 alpha:1.0];
    [STPopupNavigationBar appearance].tintColor = [UIColor whiteColor];
    [STPopupNavigationBar appearance].barStyle = UIBarStyleDefault;
    [STPopupNavigationBar appearance].titleTextAttributes = @{ NSFontAttributeName: [UIFont systemFontOfSize:17],
                                                               NSForegroundColorAttributeName: [UIColor whiteColor] };
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:toVC];
    popupController.containerView.layer.cornerRadius = 4;
    popupController.navigationBar.draggable = YES;
    [popupController presentInViewController:fromVC];    
}
+(void)popbottomVCwithFrom: (UIViewController *)fromVC to:(UIViewController *)toVC{
}
+(void)pophorizontalVCwithFrom: (UIViewController *)fromVC to:(UIViewController *)toVC{
}

#pragma mark - Local Notification.
+(void)localNofiticatonWithTitle:(NSString *)title badgNumber :(int)num
{
    if (!USERINS.notificationallow) return;
    GLOBALINS.badgeNum ++;
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:0];
    notification.alertBody = title;
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.soundName = UILocalNotificationDefaultSoundName;
    notification.applicationIconBadgeNumber = GLOBALINS.badgeNum;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

#pragma mark - INSTANCE METHODs
#pragma mark -
- (void)parameterInit
{
    self.isAppStart = NO;
    self.isEnterLocation = NO;
    self.stampCount = -1;
    self.locationCount = -1;
    self.placeholder = [UIImage imageNamed:@"placeholder"];
    self.locationMarkers = [[NSMutableArray alloc] init];
    self.stampPhotos = [[UserDefaults objectForKey:STAMPPHOTOS] mutableCopy];
    if (!self.stampPhotos) {
        self.stampPhotos = [NSMutableArray new];
    }
    self.originLanguages = [NSArray arrayWithObjects:@"English",@"Español",@"Français",@"Italiano",@"中文",@"日本語", nil];
    self.languages = [NSArray arrayWithObjects:@"English",@"Spanish",@"French",@"Italian",@"Chinese",@"Japanese", nil];
    self.langImages = [NSArray arrayWithObjects:@"english",@"spain",@"french",@"Italy",@"china",@"japanese", nil];
    NSDictionary *dictionary = [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentUser"];
    if (dictionary[FUSER_LANGUAGE]!=nil) {
        self.userLanguage = [dictionary[FUSER_LANGUAGE] intValue];
    }else{
        self.userLanguage = 0;
    }
}
-(void)badgeInit{
    self.badgeNum = 0;
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}
@end
