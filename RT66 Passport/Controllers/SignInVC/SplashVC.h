
#import <UIKit/UIKit.h>
#import "utilities.h"
@interface SplashVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *signupBtn;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UIButton *languageBtn;
@end
