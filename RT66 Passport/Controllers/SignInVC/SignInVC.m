
#import "SignInVC.h"
#import "MainVC.h"
@implementation SignInVC
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUIs];
}
-(void)setUIs{
    self.passwordF.secureTextEntry = YES;
    [self setFontFamily:@"Maiden Orange" forView:self.view andSubViews:YES];
    [self setTranslations];
}
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}
- (void) setTranslations{
    if (GLOBALINS.userLanguage<0) return;
    [self.signinTL setTitle:Log_InT[GLOBALINS.userLanguage] forState:UIControlStateNormal];
    self.signupTL.text = SignUpT[GLOBALINS.userLanguage];
    self.userNameTL.text = EmailT[GLOBALINS.userLanguage];
    self.passTL.text = PasswordT[GLOBALINS.userLanguage];
    self.donthaveTL.text = DontHaveT[GLOBALINS.userLanguage];
    self.emailF.placeholder = EmailT[GLOBALINS.userLanguage];
    self.passwordF.placeholder = PasswordT[GLOBALINS.userLanguage];
}
- (IBAction)signBtn:(id)sender {
    
    if (![Global validEmail:self.emailF.text]) {
        [ProgressHUD showError:InvalidEmailT[GLOBALINS.userLanguage]];
        return;
    }
    if (!self.passwordF.text.length) {
        [ProgressHUD showError:InvalidPassword[GLOBALINS.userLanguage]];
        return;
    }
    [ProgressHUD show:nil Interaction:NO];
    [FUser signInWithEmail:self.emailF.text password:self.passwordF.text completion:^(FUser *user, NSError *error)
     {
         if (error == nil)
         {
             [self gotoMainScreen];
         }
         else{
             if ([error.domain isEqualToString:@"FIRAuthErrorDomain"]) {
                 [ProgressHUD showError:[error description]];
             }else{
                 [ProgressHUD showError:[error description]];
             }
         }
     }];
}
#pragma mark TEXTFIELD DELEGETE.

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (void)gotoMainScreen{
    dispatch_async(dispatch_queue_create(nil, DISPATCH_QUEUE_SERIAL), ^{
        Loading *loading = [[Loading alloc] init];
        loading.fromVC = self;
        [loading initialize];
    });
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}
@end
