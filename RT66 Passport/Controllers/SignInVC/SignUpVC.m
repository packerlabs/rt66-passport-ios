//
//  ViewController.m
//  SIEclipse
//
//  Created by My Star on 7/3/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "SignUpVC.h"
#import "MainVC.h"
@interface SignUpVC ()<UIImagePickerControllerDelegate, UITextFieldDelegate>{
    UIImage *user_img;
    FUser *newUser;
}

@end

@implementation SignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    newUser = [[FUser alloc] init];
    [self setUIs];
    [self setFontFamily:@"Maiden Orange" forView:self.view andSubViews:YES];
}

- (void)setUIs{
    self.passwordF.secureTextEntry = YES;
    
    [_userImg setNeedsLayout];
    [_userImg layoutIfNeeded];
    user_img = [UIImage imageNamed:@"imgPlaceholder.png"];
    [Global roundBorderSet:self.containV borderColor:HEXCOLOR(0x454E65FF)];
    self.userImg.layer.cornerRadius = self.userImg.bounds.size.width/2;
    [[self.userImg layer] setBorderWidth: 2.0f];
    self.userImg.layer.borderColor = HEXCOLOR(0x2B6A9DFF).CGColor;
    self.userImg.layer.masksToBounds = YES;
    [self setTranslations];
}
- (void) setTranslations{
    if (GLOBALINS.userLanguage<0) return;
    [self.signupBtn setTitle:SignUpT[GLOBALINS.userLanguage] forState:UIControlStateNormal];
    self.singinTL.text = Log_InT[GLOBALINS.userLanguage];
    self.completeTL.text = CompleteProfielT[GLOBALINS.userLanguage];
    self.dohaveTL.text = DoUhaveT[GLOBALINS.userLanguage];
    self.firstNameF.placeholder = FirstNameT[GLOBALINS.userLanguage];
    self.lastNameF.placeholder = LastNameT[GLOBALINS.userLanguage];
    self.emailF.placeholder = EmailT[GLOBALINS.userLanguage];
    self.passwordF.placeholder = PasswordT[GLOBALINS.userLanguage];
}
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}
#pragma mark - BUTTON ACTIONS
- (IBAction)cameraBtn:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:TakephotoT[GLOBALINS.userLanguage] style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) { PresentPhotoCamera(self, YES); }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:ChooseGalleryT[GLOBALINS.userLanguage] style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) { PresentPhotoLibrary(self, YES); }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:CancelT[GLOBALINS.userLanguage] style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:action1]; [alert addAction:action2]; [alert addAction:action3];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)signupBtn:(id)sender
{
    NSString *firstName = self.firstNameF.text;
    NSString *lastName = self.lastNameF.text;
    if (!firstName.length) {
        [ProgressHUD showError:InvalidNameT[GLOBALINS.userLanguage]];
        return;
    }
    if (!lastName.length) {
        [ProgressHUD showError:InvalidNameT[GLOBALINS.userLanguage]];
        return;
    }
    if (![Global validEmail:self.emailF.text]) {
        [ProgressHUD showError:InvalidEmailT[GLOBALINS.userLanguage]];
        return;
    }
    if (!self.passwordF.text.length) {
        [ProgressHUD showError:InvalidPassword[GLOBALINS.userLanguage]];
        return;
    }
    if (self.passwordF.text.length<PASSWORD_LENGTH) {
        [ProgressHUD showError:InvalidPassword[GLOBALINS.userLanguage]];
        return;
    }
    [ProgressHUD show:nil Interaction:NO];    
    [FUser createUserWithEmail:self.emailF.text password:self.passwordF.text completion:^(FUser *user, NSError *error)
     {
         if (error == nil)
         {
             newUser = user;
             
             newUser[FUSER_FULLNAME] = [NSString stringWithFormat:@"%@ %@",firstName, lastName];
             [self upLoadUserImg:user_img];
             NSLog(@"Success Sign UP");
         }else{
             [ProgressHUD showError:[error description]];
         }
     }];
}
#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    user_img = info[UIImagePickerControllerEditedImage];
    self.userImg.image = user_img;
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)upLoadUserImg:(UIImage *)image
{
    UIImage *imagePicture = [Image square:image size:140];
    NSData *dataPicture = UIImageJPEGRepresentation(imagePicture, 0.6);

    long long timestamp = [[NSDate date] timestamp];
    FIRStorage *storage = [FIRStorage storage];
    FIRStorageReference *reference = [[storage referenceForURL:FIREBASE_STORAGE] child:Filename(PROFILEPHOTO_STORAGE,timestamp, @"jpg")];
    FIRStorageUploadTask *task = [[FIRStorageUploadTask alloc] init];
    task = [reference putData:dataPicture metadata:nil completion:^(FIRStorageMetadata *metadata, NSError *error)
            {
                [task removeAllObservers];
                if (error == nil)
                {
                    NSString *linkPicture = metadata.downloadURL.absoluteString;
                    newUser[FUSER_PICTURE] = linkPicture;
                    [self getUserNationality];
                }
                else [ProgressHUD showError:@"Message sending failed."];
            }];
    
    [task observeStatus:FIRStorageTaskStatusProgress handler:^(FIRStorageTaskSnapshot *snapshot)
     {

     }];
}
-(void)getUserNationality{
    NSString *url = API_IPCOUNTRY;
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        newUser[FUSER_COUNTRYCODE] = [responseObject[FUSER_COUNTRYCODE] lowercaseString];
        newUser[FUSER_COUNTRYNAME] = responseObject[FUSER_COUNTRYNAME];
        [self createNewUserTable];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [ProgressHUD showError:error.description];
    }];
}

-(void)createNewUserTable{
    newUser[FUSER_EMAIL] = self.emailF.text;
    newUser[FUSER_NOTIFICATIONALLOW] = @(YES);
    [FIRAnalytics setUserPropertyString:ANALY_USER_NATIONALITY forName:newUser[FUSER_COUNTRYNAME]];
    [newUser saveInBackground:^(NSError * _Nullable error) {
        if (error == nil) {
            [self gotoMainScreen];
        }
    }];
}
- (void)gotoMainScreen{
    dispatch_async(dispatch_queue_create(nil, DISPATCH_QUEUE_SERIAL), ^{
        Loading *loading = [[Loading alloc] init];
        loading.fromVC = self;
        [loading initialize];
    });
//    LoadingVC *loadingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoadingVC"];
//    NavigationController *navController = [[NavigationController alloc] initWithRootViewController:loadingVC];
//    [self presentViewController:navController animated:YES completion:nil];
}
#pragma mark TEXTFIELD DELEGETE.

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
