
#import "SplashVC.h"
#import "SignInVC.h"
#import "SignUpVC.h"
#import "MainVC.h"
#import "Loading.h"
#import "utilities.h"

@implementation SplashVC{
    
}
#pragma mark - Super Class Methods
- (void)viewDidLoad {[super viewDidLoad];}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}
- (void)viewWillAppear:(BOOL)animated{
    [self initialize];
    [self setUIs];
}
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}
#pragma mark - Initialize
- (void)initialize{
    [self setFontFamily:@"Maiden Orange" forView:self.view andSubViews:YES];   
}

- (void)setUIs{
    // Set Buttons' titles.
    if ([FIRAuth auth].currentUser.uid) {
        FUser *user = [FUser currentUser];
        NSLog(@"%d",GLOBALINS.userLanguage);
        NSString *buttonTitle = [NSString stringWithFormat:@"%@ %@",ContinueT[GLOBALINS.userLanguage],user[FUSER_FULLNAME]?user[FUSER_FULLNAME]:[FIRAuth auth].currentUser.email];
        
        [self.signupBtn setTitle:buttonTitle forState:UIControlStateNormal];
        [self.loginBtn setTitle:Log_OutT[GLOBALINS.userLanguage] forState:UIControlStateNormal];
        
    }else{
        [self.signupBtn setTitle:Create_AccountT[GLOBALINS.userLanguage] forState:UIControlStateNormal];
        [self.loginBtn setTitle:Log_InT[GLOBALINS.userLanguage] forState:UIControlStateNormal];
    }
    
    // Set Language button image.
    if ((int)GLOBALINS.userLanguage > -1) {
        UIImage *languageImg = [UIImage imageNamed:(NSString *)GLOBALINS.langImages[GLOBALINS.userLanguage]];
        [self.languageBtn setImage:languageImg forState:UIControlStateNormal];
    }
    [self setTranslation];
}
-(void) setTranslation{
    if ((int)GLOBALINS.userLanguage<0) return;
//    create_account_button.setText(Constant.Create_AccountT[Global.userLanguage]);
//    if (CurrentUser.fullname.length() > 1) {
//        continue_button.setText(Constant.ContinueT[Global.userLanguage]  + " "+ CurrentUser.fullname);
//    }
//    login_button.setText(Constant.Log_InT[Global.userLanguage]);
//    logout_button.setText(Constant.Log_OutT[Global.userLanguage]);
}
#pragma mark - Button actions
- (IBAction)createAccountBtn:(id)sender
{
    if ([FIRAuth auth].currentUser.uid) {
        [ProgressHUD show:nil Interaction:NO];
        dispatch_async(dispatch_get_main_queue(), ^{
            Loading *loading = [[Loading alloc] init];
            loading.fromVC = self;
            [loading initialize];
        });
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            SignUpVC *sWRevealViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpVC"];
            [self presentViewController:sWRevealViewController animated:NO completion:nil];
        });
    }
}

- (IBAction)loginAccount:(id)sender
{
    if ([FIRAuth auth].currentUser.uid) {
        NSError *signOutError;
        BOOL status = [[FIRAuth auth] signOut:&signOutError];
        if (!status) {
            NSLog(@"Error signing out: %@", signOutError);
            return;
        }else{
            [self.signupBtn setTitle:Create_AccountT[GLOBALINS.userLanguage] forState:UIControlStateNormal];
            [self.loginBtn setTitle:Log_InT[GLOBALINS.userLanguage] forState:UIControlStateNormal];
        }
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            SignInVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInVC"];
            [self presentViewController:vc animated:NO completion:nil];
        });
    }
}

- (IBAction)selectlanguageBtn:(id)sender {
    LanguageVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LanguageVC"];
    vc.contentSizeInPopup = CGSizeMake(250, 320);
    vc.landscapeContentSizeInPopup = CGSizeMake(250, 320);
    [Global popupVCwithFrom:self to:vc];
}
@end
