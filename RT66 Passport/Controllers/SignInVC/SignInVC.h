//
//  SignInVC.h
//  SIEclipse
//
//  Created by My Star on 7/4/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
@interface SignInVC : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *emailF;
@property (weak, nonatomic) IBOutlet UITextField *passwordF;

@property (weak, nonatomic) IBOutlet UILabel *userNameTL;
@property (weak, nonatomic) IBOutlet UILabel *passTL;
@property (weak, nonatomic) IBOutlet UILabel *donthaveTL;
@property (weak, nonatomic) IBOutlet UILabel *signupTL;
@property (weak, nonatomic) IBOutlet UIButton *signinTL;
@end
