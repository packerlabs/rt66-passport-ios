
#import <UIKit/UIKit.h>
#import "utilities.h"
@interface SignUpVC : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *firstNameF;
@property (weak, nonatomic) IBOutlet UITextField *lastNameF;

@property (weak, nonatomic) IBOutlet UITextField *emailF;
@property (weak, nonatomic) IBOutlet UITextField *passwordF;

@property (weak, nonatomic) IBOutlet UIImageView *userImg;

@property (weak, nonatomic) IBOutlet UIView *containV;

@property (weak, nonatomic) IBOutlet UILabel *completeTL;
@property (weak, nonatomic) IBOutlet UILabel *dohaveTL;
@property (weak, nonatomic) IBOutlet UILabel *singinTL;
@property (weak, nonatomic) IBOutlet UIButton *signupBtn;

@end

