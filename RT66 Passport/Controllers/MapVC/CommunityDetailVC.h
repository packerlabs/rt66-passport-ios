//
//  BufferVC.h
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
#import "ZMImageSliderViewController.h"
@interface CommunityDetailVC : UIViewController<ZMImageSliderViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *containV;
@property(strong, nonatomic) ZMImageSliderView *imageSliderView;

@property DBCommunity *dbCommunity;
@property (weak, nonatomic) IBOutlet UILabel *nameL;
@property (weak, nonatomic) IBOutlet UILabel *descriptionL;
@property (weak, nonatomic) IBOutlet UILabel *displayLabel;
@property (weak, nonatomic) IBOutlet UIImageView *photoNamBackIv;


- (void)imageSliderViewSingleTap:(UITapGestureRecognizer *)tap;
- (void)imageSliderViewImageSwitch:(NSInteger)index count:(NSInteger)count imageUrl:(NSString *)imageUrl;
@end
