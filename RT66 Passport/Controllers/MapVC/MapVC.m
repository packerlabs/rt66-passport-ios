//
//  MapVC.m
//  SIEclipse
//
//  Created by My Star on 7/5/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "MapVC.h"
#import "MarkerV.h"
#import "HelpfulAlertV.h"
#import "ASIHTTPRequest.h"
#import "CommunityDetailVC.h"
#import "LocationListVC.h"
@import GoogleMaps;
@interface MapVC ()<GMSMapViewDelegate>{
    MarkerV *markV;
    NSTimer *timerWeater;
}
@end

@implementation MapVC
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
    if ([CLLocationManager locationServicesEnabled]){
        
        NSLog(@"Location Services Enabled");
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            NSLog(@"authorizationStatus Services Enabled");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                                                            message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }else{
            [Location start];
        }
    }
    [self updateWeather];
    timerWeater = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(updateWeather) userInfo:nil repeats:YES];
    [self setFontFamily:@"Maiden Orange" forView:self.view andSubViews:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    GLOBALINS.mainVC.navigationItem.title = MapT[USERINS.userLanguage];
    [self setTranslations];
}
- (void) setTranslations{
    self.attractionTL.text = AttractionT[USERINS.userLanguage];
    self.stamplocationTL.text = StampLocationT[USERINS.userLanguage];
    self.redeemstampTL.text = Redeemed_AttractionT[USERINS.userLanguage];
}

-(void)initialize{
    [NotificationCenter addObserver:self selector:@selector(changeMapMarker:) name:NOTI_GEOFENCEADDED];
    [NotificationCenter addObserver:self selector:@selector(navigateNextStop:) name:NOTI_GONEXTSTOP];
    [self mapSet];
    [self specificRTRoad];
    [self performSelector:@selector(viewLocationlist:) withObject:nil afterDelay:2.0];
}

-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}
#pragma mark - MAP SET/ DELEGATE
- (void)mapSet
{
    self.mapView.delegate = self;
    self.mapView.myLocationEnabled=YES;
    self.mapView.settings.rotateGestures = NO;
    self.mapView.settings.tiltGestures = NO;
    // Map Camera
    GMSCameraPosition *mylocation = [GMSCameraPosition cameraWithLatitude:Location.latitude
                                                                    longitude:Location.longitude
                                                                         zoom:MAPZOOM_DEFAUL];
    [self.mapView setCamera:mylocation];
    
    // Map Style
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"retro_eco" withExtension:@"json"];
    NSError *error;
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    self.mapView.mapStyle = style;
    
    // Show Markers
    for (int i = 0; i< GLOBALINS.locationMarkers.count ; i++){
        GMSMarker *marker = GLOBALINS.locationMarkers[i];
        marker.map = self.mapView;
    }
    // Show Way
    
}
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    NSInteger index = marker.zIndex;
    if (index<0) {
        return NO;
    }
    if (index < GLOBALINS.dblocations.count) {
        DBLocation *dblocation = GLOBALINS.dblocations[index];
        LocationDetailVC *locationDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LocationDetailVC"];
        locationDetailVC.dblocation = dblocation;
        [GLOBALINS.mainVC.navigationController pushViewController:locationDetailVC animated:YES];
        return YES;
    }else{
        DBCommunity *dbCommunity = DBCommunity.allObjects[index - GLOBALINS.dblocations.count];
        CommunityDetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CommunityDetailVC"];
        vc.dbCommunity = dbCommunity;
        [GLOBALINS.mainVC.navigationController pushViewController:vc animated:YES];
        return YES;
    }
}


-(void)addMytrip:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    NSInteger index = btn.tag;
    DBLocation *dblocation = GLOBALINS.dblocations[index];
    NSString *objectId = dblocation.objectId;
    NSMutableArray *mytrips = USERINS.mytrips;
    if (!mytrips) mytrips = [[NSMutableArray alloc] init];
    
    if ([mytrips containsObject:objectId]) {
        [self.view makeToast:AlreadyAttractionT[USERINS.userLanguage]];
        return;
    }
    [mytrips addObject:objectId];
    FUser *user = FUser.currentUser;
    user[FUSER_MYTRIPS] = mytrips;
    [ProgressHUD show:nil Interaction:NO];
    [user saveInBackground:^(NSError * _Nullable error) {
        if (error) {
            [ProgressHUD showError:error.description];
        }else{
            [ProgressHUD dismiss];
        }
    }];
}
#pragma mark - MAP WAY SPECIFIC
-(void)specificRTRoad{
    for (int i = 0; i<DBLocation.allObjects.count - 1; i++) {
        DBLocation *startLocation = GLOBALINS.dblocations[i];
        DBLocation *endLocation = GLOBALINS.dblocations[i+1];
        NSString *startLat = startLocation.latitude;
        NSString *startLon = startLocation.longitude;
        NSString *endLat = endLocation.latitude;
        NSString *endLon = endLocation.longitude;
        [self addDirectionOnMapWithStartLat:startLat
                                   startLon:startLon
                                     endLat:endLat
                                     endLon:endLon];
    }

}
int wayNodeCount = 0;
-(void)addDirectionOnMapWithStartLat:(NSString *)startLat
                            startLon:(NSString *)startLon
                              endLat:(NSString *)endLat
                              endLon:(NSString *)endLon{
    NSString *urlString = [NSString stringWithFormat:
                           @"%@?origin=%@,%@&destination=%@,%@&sensor=false",
                           @"https://maps.googleapis.com/maps/api/directions/json",
                           startLat,
                           startLon,
                           endLat,
                           endLon];
    NSURL *directionsURL = [NSURL URLWithString:urlString];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:directionsURL];
        [request startSynchronous];
        NSError *error = [request error];
        if (!error) {
            //        NSString *response = [request responseString];
            //        NSLog(@"%@",response);
            NSDictionary *json =[NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableContainers error:&error];
            dispatch_async(dispatch_get_main_queue(), ^{
                @try{
                    GMSPath *path =[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
                    GMSPolyline *singleLine = [GMSPolyline polylineWithPath:path];
                    singleLine.strokeWidth = 7;
                    singleLine.strokeColor = HEXCOLOR(0xDC3E05FF);
                    singleLine.map = self.mapView;
                }@catch(NSException *e){

                }

            });
            wayNodeCount++;
            if (wayNodeCount == DBLocation.allObjects.count-1) {
//                [ProgressHUD dismiss];
            }
            
        }else{
            NSLog(@"%@",[request error]);
            [ProgressHUD dismiss];
        }
    });
}

#pragma mark - BUTTON ACTIONs
- (IBAction)mylocationBtn:(id)sender {
    CLLocation *location = self.mapView.myLocation;
    if (location) {
        double latitude = [Location latitude];
        double longitude = [Location longitude];
        GMSCameraPosition *sydney = [GMSCameraPosition cameraWithLatitude:latitude
                                                                longitude:longitude
                                                                     zoom:16.0];
        
        [self.mapView setCamera:sydney];
    }
}

- (IBAction)viewLocationlist:(id)sender {
    [self viewLocationList:YES];
}
- (IBAction)viewCommunitylist:(id)sender {
    [self viewLocationList:NO];
}
- (void)viewLocationList: (BOOL)isLocationList{
    LocationListVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LocationListVC"];
    vc.isLocationList = isLocationList;
    vc.contentSizeInPopup = CGSizeMake(250, 320);
    vc.landscapeContentSizeInPopup = CGSizeMake(250, 320);
    [Global popupVCwithFrom:self to:vc];
}
#pragma mark - UPDATE WEATHER
-(void)updateWeather
{
    NSString *url = [NSString stringWithFormat:API_OVERAYMAP,Location.latitude,Location.longitude];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSString *weatherIcon = responseObject[@"weather"][0][@"icon"];
        double temp = ([responseObject[@"main"][@"temp"] doubleValue] - 273.15) *1.8 + 32;

        self.tempL.text = [NSString stringWithFormat:@"%d°F",(int)temp];
        self.weatherIconImg.image = [UIImage imageNamed:weatherIcon];

    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [ProgressHUD showError:error.description];
    }];
}
#pragma mark - OTHERs
-(void)changeMapMarker:(id)obj
{
    GeofenceModel *geofenceModel = (GeofenceModel*)[obj object];
    if (!geofenceModel.isEntered) {return;}
    
    NSPredicate *locationPredicate = [NSPredicate predicateWithFormat:@"objectId == %@",geofenceModel.dbLocationId];
    DBLocation *dblocation = [[GLOBALINS.dblocations objectsWithPredicate:locationPredicate] sortedResultsUsingKeyPath:FLOCATION_NAME ascending:YES].firstObject;
    NSInteger index = [GLOBALINS.dblocations indexOfObject:dblocation];
    GMSMarker *marker = GLOBALINS.locationMarkers[index];
    marker.icon = [UIImage imageNamed:@"enteredmarker"];
    marker.map = self.mapView;
    marker.zIndex = (int)index;
    [GLOBALINS.locationMarkers replaceObjectAtIndex:index withObject:marker];
}
-(void)navigateNextStop:(id)obj{
    NSString *nextStopLocationId = (NSString*)[obj object];
    float latitu,longitu;
    NSPredicate *locationPredicate = [NSPredicate predicateWithFormat:@"objectId == %@",nextStopLocationId];
    DBLocation *dblocation = [DBLocation objectsWithPredicate:locationPredicate].firstObject;
    if (dblocation.address) {
        latitu = [dblocation.latitude floatValue];
        longitu = [dblocation.longitude floatValue];
    }else{
        DBCommunity *dbCommunity = [DBCommunity objectsWithPredicate:locationPredicate].firstObject;
        latitu = [dbCommunity.latitude floatValue];
        longitu = [dbCommunity.longitude floatValue];
    }  
    GMSCameraPosition *nextStopPostion = [GMSCameraPosition cameraWithLatitude:latitu
                                                                longitude:longitu
                                                                     zoom:MAPZOOM_DETAIL];
    [self.mapView setCamera:nextStopPostion];
}
- (void)didReceiveMemoryWarning {    [super didReceiveMemoryWarning];}

@end
