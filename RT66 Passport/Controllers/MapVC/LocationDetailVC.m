//
//  BufferVC.m
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "LocationDetailVC.h"
#import "OpenHourCell.h"
@interface LocationDetailVC ()<UITableViewDataSource>{
    BOOL isMytrip;
    NSArray *openHours;
    NSInteger weekDayInt;
    NSArray *descs;
}

@end

@implementation LocationDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setFontFamily:@"Maiden Orange" forView:self.view andSubViews:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    [self initialize];
    [self setUIs];
}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs
{
    self.navigationItem.title = self.dblocation.name;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btnBackMain"]
                                                                             style:UIBarButtonItemStylePlain target:self action:@selector(actionBack)];
    
    
    self.nameL.text = self.dblocation.name;
    
    self.addressL.text = self.dblocation.address;
    [Global roundCornerSet:self.photoNamBackIv];
    NSString *images = self.dblocation.photos;
    NSArray * imageUrls = [images componentsSeparatedByString:@","];
    
    self.imageSliderView = [[ZMImageSliderView alloc] initWithOptions:0 imageUrls:imageUrls];
    _imageSliderView.translatesAutoresizingMaskIntoConstraints = NO;
    _imageSliderView.delegate = self;
    [self imageSliderViewImageSwitch:0 count:[imageUrls count] imageUrl:[imageUrls objectAtIndex:0]];
    
    [self.containV addSubview:self.imageSliderView];
    [self setImageSliderViewConstraints];

    [self setTranslations];
}
- (void) setTranslations{
    if ([USERINS.mytrips containsObject:self.dblocation.objectId]) {
        [self.deletBtn setTitle:RemoveTripT[USERINS.userLanguage] forState:UIControlStateNormal];
        isMytrip = YES;
    }else{
        [self.deletBtn setTitle:AddTripT[USERINS.userLanguage] forState:UIControlStateNormal];
        isMytrip = NO;
    }
    self.descriptionL.text = (NSString *)descs[USERINS.userLanguage];
    self.hoursTL.text = HoursT[USERINS.userLanguage];
}
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}
- (void) initialize
{
    NSString *descriptions = self.dblocation.desc;
    descs = [descriptions componentsSeparatedByString:SENTENCEPOINT];
    NSString *openHours_ = self.dblocation.openHours;
    openHours = [openHours_ componentsSeparatedByString:@","];
    weekDayInt = [[NSCalendar currentCalendar] component:NSCalendarUnitWeekday
                                                       fromDate:[NSDate date]];
}

#pragma mark - BUTTON ACTIONS
- (void)actionBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)callBtn:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",self.dblocation.phone]]];
}
- (IBAction)navigateBtn:(id)sender {
    NSString *str1 = [self.dblocation.address stringByReplacingOccurrencesOfString:@" " withString:@",+"];
    NSString *str2 = [str1 stringByReplacingOccurrencesOfString:@",+,+" withString:@",+"];
    NSString *str3 = [str2 stringByReplacingOccurrencesOfString:@",," withString:@","];
    NSString *addressString = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@",str3];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:addressString]];
}
- (IBAction)websiteBtn:(id)sender {
    NSURL *url = [NSURL URLWithString:self.dblocation.website];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}
- (IBAction)deleteBtn:(id)sender {
    if (isMytrip) {
        [self deletfromMytrip];
    }else{
        [self addtoMytrip];
    }

}
-(void)deletfromMytrip{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:WantRemoveAttractionT[USERINS.userLanguage]
                                                                  message:nil
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:YesT[USERINS.userLanguage]
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    NSString *objectId = self.dblocation.objectId;
                                    NSMutableArray *mytrips = USERINS.mytrips;
                                    
                                    [mytrips removeObject:objectId];
                                    FUser *user = FUser.currentUser;
                                    user[FUSER_MYTRIPS] = mytrips;
                                    [ProgressHUD show:nil Interaction:NO];
                                    [user saveInBackground:^(NSError * _Nullable error) {
                                        if (error) {
                                            [ProgressHUD showError:error.description];
                                        }else{
                                            [ProgressHUD dismiss];
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }
                                    }];
                                    
                                }];
    
    UIAlertAction* noButton = [UIAlertAction actionWithTitle:CancelT[USERINS.userLanguage]
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)addtoMytrip{
    
    NSString *objectId = self.dblocation.objectId;
    NSMutableArray *mytrips = USERINS.mytrips;
    if (!mytrips) mytrips = [[NSMutableArray alloc] init];
    
    if ([mytrips containsObject:objectId]) {
        [self.view makeToast:AlreadyAttractionT[USERINS.userLanguage]];
        return;
    }
    [mytrips addObject:objectId];
    FUser *user = FUser.currentUser;
    user[FUSER_MYTRIPS] = mytrips;
    [ProgressHUD show:nil Interaction:NO];
    [user saveInBackground:^(NSError * _Nullable error) {
        if (error) {
            [ProgressHUD showError:error.description];
        }else{
            [ProgressHUD dismiss];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}
#pragma mark - TABLEVIEW DATASOURCE

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return openHours.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OpenHourCell *cell = (OpenHourCell *)[tableView dequeueReusableCellWithIdentifier:@"OpenHourCell" forIndexPath:indexPath];
    if (indexPath.row == 0) {
        cell.openhourL.font = [UIFont fontWithName:@"Maiden Orange" size:17];
    }else{
        cell.openhourL.font = [UIFont fontWithName:@"Maiden Orange" size:14];
    }
    cell.openhourL.text = openHours[(indexPath.row + weekDayInt +5)%7];
    return cell;
}
-(NSString *)getOpenHourString :(NSInteger) index{


    return nil;
}
#pragma mark - SET SLIDERVIEW CONSTRAINTS
- (void)setImageSliderViewConstraints {
    NSArray *imageSliderViewHConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[imageSliderView]-0-|"
                                                                                   options:0
                                                                                   metrics:nil
                                                                                     views:@{@"imageSliderView": self.imageSliderView}];
    
    [self.view addConstraints:imageSliderViewHConstraints];
    
    
    
    NSArray *imageSliderViewVConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[imageSliderView]-0-|"
                                                                                   options:0
                                                                                   metrics:nil
                                                                                     views:@{@"imageSliderView": self.imageSliderView}];
    
    [self.view addConstraints:imageSliderViewVConstraints];
}


#pragma mark - ZMImageSliderViewDelegate
- (void)imageSliderViewSingleTap:(UITapGestureRecognizer *)tap {
    //    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imageSliderViewImageSwitch:(NSInteger)index count:(NSInteger)count imageUrl:(NSString *)imageUrl {
    self.displayLabel.text = [[NSString alloc] initWithFormat:@"%td／%td", index + 1, count];
}

@end
