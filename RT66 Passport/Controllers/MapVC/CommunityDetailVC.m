//
//  BufferVC.m
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "CommunityDetailVC.h"
@interface CommunityDetailVC (){
    NSArray *descs;
}

@end

@implementation CommunityDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setFontFamily:@"Maiden Orange" forView:self.view andSubViews:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    [self initialize];
    [self setUIs];
}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs
{
    self.navigationItem.title = self.dbCommunity.name;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btnBackMain"]
                                                                             style:UIBarButtonItemStylePlain target:self action:@selector(actionBack)];
    
    
    self.nameL.text = self.dbCommunity.name;
    self.descriptionL.text = (NSString *)descs[USERINS.userLanguage];
    [Global roundCornerSet:self.photoNamBackIv];
    
    
    NSString *image = self.dbCommunity.icon;
    NSArray * imageUrls = [NSArray arrayWithObjects:image, nil];
    
    self.imageSliderView = [[ZMImageSliderView alloc] initWithOptions:0 imageUrls:imageUrls];
    _imageSliderView.translatesAutoresizingMaskIntoConstraints = NO;
    _imageSliderView.delegate = self;
    [self imageSliderViewImageSwitch:0 count:[imageUrls count] imageUrl:[imageUrls objectAtIndex:0]];
    
    [self.containV addSubview:self.imageSliderView];
    [self setImageSliderViewConstraints];

}
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}
- (void) initialize
{
    NSString *descriptions = self.dbCommunity.desc;
    descs = [descriptions componentsSeparatedByString:SENTENCEPOINT];
}

#pragma mark - BUTTON ACTIONS
- (void)actionBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - SET SLIDERVIEW CONSTRAINTS
- (void)setImageSliderViewConstraints {
    NSArray *imageSliderViewHConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[imageSliderView]-0-|"
                                                                                   options:0
                                                                                   metrics:nil
                                                                                     views:@{@"imageSliderView": self.imageSliderView}];
    
    [self.view addConstraints:imageSliderViewHConstraints];
    
    
    
    NSArray *imageSliderViewVConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[imageSliderView]-0-|"
                                                                                   options:0
                                                                                   metrics:nil
                                                                                     views:@{@"imageSliderView": self.imageSliderView}];
    
    [self.view addConstraints:imageSliderViewVConstraints];
}


#pragma mark - ZMImageSliderViewDelegate
- (void)imageSliderViewSingleTap:(UITapGestureRecognizer *)tap {
    //    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imageSliderViewImageSwitch:(NSInteger)index count:(NSInteger)count imageUrl:(NSString *)imageUrl {
    self.displayLabel.text = [[NSString alloc] initWithFormat:@"%td／%td", index + 1, count];
}

@end
