//
//  MarkerV.h
//  ITAGG
//
//  Created by My Star on 2/25/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserMarkerV : UIView
@property (weak, nonatomic) IBOutlet UIImageView *weatherImg;
@property (weak, nonatomic) IBOutlet UILabel *tempL;
@end
