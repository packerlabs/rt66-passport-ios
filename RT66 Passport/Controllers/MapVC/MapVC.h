//
//  MapVC.h
//  SIEclipse
//
//  Created by My Star on 7/5/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"

@interface MapVC : UIViewController
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;

@property (weak, nonatomic) IBOutlet UIView *weatherContainV;
@property (weak, nonatomic) IBOutlet UIImageView *weatherIconImg;
@property (weak, nonatomic) IBOutlet UILabel *tempL;

@property (weak, nonatomic) IBOutlet UILabel *attractionTL;
@property (weak, nonatomic) IBOutlet UILabel *stamplocationTL;
@property (weak, nonatomic) IBOutlet UILabel *redeemstampTL;
@end
