//
//  MarkerV.h
//  ITAGG
//
//  Created by My Star on 2/25/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarkerV : UIView
@property (weak, nonatomic) IBOutlet UIView *weatherview;
@property (weak, nonatomic) IBOutlet UIImageView *weatherimg;
@property (weak, nonatomic) IBOutlet UILabel *tempL;

@end
