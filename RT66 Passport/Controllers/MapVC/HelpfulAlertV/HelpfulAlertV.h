//
//  MapProfileV.h
//  ITAGG
//
//  Created by My Star on 3/30/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpfulAlertV : UIView
@property (weak, nonatomic) IBOutlet UIImageView *userIv;
@property (weak, nonatomic) IBOutlet UILabel *nameL;

@property (weak, nonatomic) IBOutlet UILabel *descL;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UIView *redeemedContainV;
@property (weak, nonatomic) IBOutlet UILabel *redeemedDateL;

@end
