//
//  SettingCell.h
//  RT66 Passport
//
//  Created by My Star on 8/6/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *itemL;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@end
