//
//  Loading.h
//  RT66 Passport
//
//  Created by My Star on 10/31/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "utilities.h"
#import "MarkerV.h"
#import "UserMarkerV.h"
@interface Loading : NSObject
#pragma mark - GOFENCE
@property UIViewController *fromVC;
- (void) initialize;
@end
