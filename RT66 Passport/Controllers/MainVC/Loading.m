//
//  Loading.m
//  RT66 Passport
//
//  Created by My Star on 10/31/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "Loading.h"
@interface Loading ()
@property (nonatomic) NSMutableSet *insideGeofenceIds;
@property (nonatomic) NSMutableArray *rightBarButtonItems;
@property (nonatomic) CLLocationManager *locationManager;

@end

@implementation Loading{
    NSTimer *timer;
}
- (void) initialize
{
    FUser *user = [FUser currentUser];
    [user fetchInBackground:^(NSError * _Nullable error) {
        if (error) {
            [ProgressHUD showError:ERROR_NETWORK];
        }else{
            timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(refreshRealmData) userInfo:nil repeats:YES];
        }
    }];
    [self setCountryRealm];
    [Locations shared];
    [Communities shared];
    [Stamps shared];
    [Beacons shared];
    [NotificationCenter post:NOTI_APP_STARTED];
}

#pragma mark - SET REALMs
-(void)setCountryRealm{
    NSMutableArray *countries = [[NSMutableArray alloc] initWithContentsOfFile:[Dir application:@"countries.plist"]];
    for (int i=0; i<countries.count; i++) {
        NSDictionary *dic = (NSDictionary *)countries[i];
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [DBCountry createOrUpdateInRealm:realm withValue:dic];
        [realm commitWriteTransaction];
    }
}
- (void)refreshRealmData
{
    GLOBALINS.dbstamps = [DBStamp allObjects];
    GLOBALINS.dblocations = [[DBLocation allObjects] sortedResultsUsingKeyPath:FLOCATION_ORDER ascending:YES];
    
    NSLog(@"%lu",GLOBALINS.dbstamps.count);
    NSLog(@"%lu",GLOBALINS.dblocations.count);
    NSLog(@"%lu",GLOBALINS.stampCount);
    NSLog(@"%lu",GLOBALINS.locationCount);
    if (GLOBALINS.stampCount<0||GLOBALINS.locationCount<0) {
        return;
    }
    
    if ([GLOBALINS.dbstamps count]>=GLOBALINS.stampCount&&[GLOBALINS.dblocations count]>=GLOBALINS.locationCount)
    {
        NSLog(@"%lu",[GLOBALINS.dbstamps count]);
        NSLog(@"%lu",[GLOBALINS.dblocations count]);
        // Get Location Name Array
        NSMutableArray *array = [NSMutableArray new];
        for (int i = 0; i<DBLocation.allObjects.count; i++) {
            DBLocation *dbLocation = GLOBALINS.dblocations[i];
            [array addObject:dbLocation.name];
        }
        GLOBALINS.locationNames = [NSArray arrayWithArray:array];
        
        // Get Community Name Array
        array = [NSMutableArray new];
        for (int i = 0; i<DBCommunity.allObjects.count; i++) {
            DBCommunity *dbCommunity = DBCommunity.allObjects[i];
            [array addObject:dbCommunity.name];
        }
        GLOBALINS.communityNames = [NSArray arrayWithArray:array];
        // Set user Language
        if ((int)GLOBALINS.userLanguage>-1) {
            [CurrentUser saveUserInfo:FUSER_LANGUAGE :@(GLOBALINS.userLanguage)];
        }        
        [timer invalidate];
        [self setLocationMarker];             
    }
}

#pragma mark - MAP MARKER
-(void)setLocationMarker
{
//    MarkerV *markV;
//    markV = [[[NSBundle mainBundle] loadNibNamed:@"MarkerV" owner:self options:nil] objectAtIndex:0];
//    UIImage *markerIcon = [self imageFromView:markV];
//    GMSMarker *marker = [[GMSMarker alloc]init];
//    marker.icon = markerIcon;
    NSMutableArray *mystamps = [USERINS.mystamps mutableCopy];
    for (int i = 0; i< GLOBALINS.dblocations.count ; i++){
        DBLocation *dblocation = GLOBALINS.dblocations[i];
        float latitu = [dblocation.latitude floatValue];
        float longitu = [dblocation.longitude floatValue];
        GMSMarker *marker = [[GMSMarker alloc]init];
        BOOL wasredeemed = NO;
        for (NSDictionary *map in mystamps){
            NSLog(@"%@",map);
            if ([map.allKeys containsObject:dblocation.objectId]){
                wasredeemed = YES;
                break;
            }
        }
        if (wasredeemed) {
            marker.icon = [UIImage imageNamed:@"enteredmarker"];
        }else{
            marker.icon = [UIImage imageNamed:@"marker"];
        }
        marker.position = CLLocationCoordinate2DMake(latitu, longitu);
        marker.zIndex = i;
        [GLOBALINS.locationMarkers addObject:marker];
        if (i >= GLOBALINS.dblocations.count-1){
            [self goMainScreen];
        }
    }
    for (int i = (int)GLOBALINS.dblocations.count; i< GLOBALINS.dblocations.count + DBCommunity.allObjects.count ; i++){
        int index = i - (int)GLOBALINS.dblocations.count;
        DBCommunity *dbCommunity =  DBCommunity.allObjects[index];
        float latitu = [dbCommunity.latitude floatValue];
        float longitu = [dbCommunity.longitude floatValue];
        GMSMarker *marker = [[GMSMarker alloc]init];
        marker.icon = [UIImage imageNamed:@"community"];
        marker.position = CLLocationCoordinate2DMake(latitu, longitu);
        marker.zIndex = i;
        [GLOBALINS.locationMarkers addObject:marker];
    }
}
- (UIImage *)imageFromView:(UIView *)view
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
#pragma mark - GO TO MAIN SCREEN
-(void)goMainScreen{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MainVC *mainVC = [storyboard instantiateViewControllerWithIdentifier:@"MainVC"];
    NavigationController *navController = [[NavigationController alloc] initWithRootViewController:mainVC];
    [self.fromVC presentViewController:navController animated:NO completion:nil];
}
@end

