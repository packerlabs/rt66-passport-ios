
#import "MainVC.h"
#import "NextStopPopuView.h"
#import "SettingCell.h"
#import "ZLTabBarItem.h"
#import <objc/runtime.h>
#import "ASIHTTPRequest.h"
@import CoreLocation;
@interface MainVC ()<CLLocationManagerDelegate,CBCentralManagerDelegate>{
    CustomIOSAlertView *nextStopalertView;
    NSString *nextStopLocationId;
    
    // Beacon
    NSDictionary*        beaconsDict;
    CLLocationManager*   locationManager;
    NSArray*             listUUID;
    CLBeacon*            selectedBeacon;
}

@end
@implementation MainVC
#pragma mark - Super Class
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!GLOBALINS.isAppStart) GLOBALINS.isAppStart = YES;
    else return;
    
    [self reloadGeofences];
    [self setGeoFenceList];
    [self setupViewControllers];
    [self setUIs];
    [self initialize];
    [self setFontFamily:@"Maiden Orange" forView:self.view andSubViews:YES];
}
- (void)beaconSetting{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    
    listUUID=[[NSArray alloc] init];
    beaconsDict=[[NSMutableDictionary alloc] init];
    AIBBeaconRegionAny *beaconRegionAny = [[AIBBeaconRegionAny alloc] initWithIdentifier:@"Any"];
    [locationManager requestWhenInUseAuthorization];
    [locationManager startRangingBeaconsInRegion:beaconRegionAny];
}
//-(void)viewWillAppear:(BOOL)animated{[self.view bringSubviewToFront:self.settingTv];}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}
#pragma mark - Initialize
- (void) initialize
{   
    GLOBALINS.naviHeight = self.navigationController.navigationBar.frame.size.height;
    GLOBALINS.mainVC = self;   
    [self checkingBlutoothStatus];
}
-(void)checkingBlutoothStatus{
    if (!self.manager) {
        self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    }
    [self centralManagerDidUpdateState:self.manager];
}
- (void)centralManagerDidUpdateState:(CBCentralManager *)central{
    if (central.state == CBCentralManagerStatePoweredOn) {
        [self beaconSetting];
//        [self.view makeToast:CanBluetoothT[USERINS.userLanguage] duration:2.0 position:CSToastPositionCenter];
    } else if(central.state == CBCentralManagerStatePoweredOff) {
        [self performSelector:@selector(delayedMessage) withObject:nil afterDelay:2.0];
//        [self.view makeToast:CannotBluetoothT[USERINS.userLanguage] duration:2.0 position:CSToastPositionCenter];
        NSLog(@"ONOFF");
    }
}
-(void)delayedMessage{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:BluetoothAllowT[USERINS.userLanguage] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}
-(void)setupViewControllers
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HomeVC *homeVC_ = [storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    MapVC *mapVC_ = [storyboard instantiateViewControllerWithIdentifier:@"MapVC"];
    UrPassVC *urPassVC_ = [storyboard instantiateViewControllerWithIdentifier:@"UrPassVC"];
    ProfileVC *profileVC_ = [storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
    
    [self setViewControllers:@[homeVC_, mapVC_, urPassVC_, profileVC_]];
    [self customizeTabBarForController:self];
    [self setSelectedIndex:[self selectedIndex]];
    [self setTabBarHidden:self.isTabBarHidden animated:NO];
}
- (void)customizeTabBarForController:(ZLTabBarController *)tabBarController {
    
    NSArray *tn = @[@"home_untap", @"map_untap", @"stamp_untap", @"profile_untap"];
    NSArray *ta = @[@"home_tap", @"map_tap", @"stamp_tap", @"profile_tap"];
    
    [tabBarController tabBar].backgroundColor = COLOR_NAVIGATIONBAR;
    [tabBarController tabBar].isShowStrokeAnimaiton = YES;
    [tabBarController tabBar].barItemLineWidth  = 1.0f;
    [tabBarController tabBar].barItemStrokeColor = [UIColor whiteColor];
    NSInteger index = 0;
    for (ZLTabBarItem *item in [[tabBarController tabBar] items]) {
        [item setFinishedSelectedImage:[UIImage imageNamed:ta[index]] withFinishedUnselectedImage:[UIImage imageNamed:tn[index]]];
        index++;
    }
}
-(void)setUIs{
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"more"]
//                                                                              style:UIBarButtonItemStylePlain
//                                                                             target:self
//                                                                             action:@selector(showHideSettingView)];
    // Screen settings   

//    [Global roundCornerSet:self.settingTv];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                      NSFontAttributeName:[UIFont fontWithName:@"Maiden Orange" size:21]}];
    
}

-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}
#pragma mark - Beacon
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    NSLog(@"locationManagerDidChangeAuthorizationStatus: %d", status);
    
    [UIAlertController alertControllerWithTitle:@"Authoritzation Status changed"
                                        message:[[NSString alloc] initWithFormat:@"Location Manager did change authorization status to: %d", status]
                                 preferredStyle:UIAlertControllerStyleAlert];
    
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
    NSLog(@"locationManager:%@ didRangeBeacons:%@ inRegion:%@",manager, beacons, region);
    
    NSMutableArray* listUuid=[[NSMutableArray alloc] init];
    NSMutableDictionary* beaconsDict=[[NSMutableDictionary alloc] init];
    for (CLBeacon* beacon in beacons) {
        NSString* uuid=[beacon.proximityUUID UUIDString].lowercaseString;
        if (![listUUID containsObject:uuid]) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@",uuid];
            DBBeacon *dbbeacon = [DBBeacon objectsWithPredicate:predicate].firstObject;
            if (!dbbeacon) return;
            NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"objectId == %@",dbbeacon.locationId];
            DBLocation *dblocation = [DBLocation objectsWithPredicate:predicate1].firstObject;
            [self setGeofenceResponce:dblocation.name isEntered:YES isGeofence:NO];
        }
        NSMutableArray* list=[beaconsDict objectForKey:uuid];
        if (list==nil){
            list=[[NSMutableArray alloc] init];
            [listUuid addObject:uuid];
            [beaconsDict setObject:list forKey:uuid];
        }
        [list addObject:beacon];
    }
    [listUuid sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSString* string1=obj1;
        NSString* string2=obj2;
        return [string1 compare:string2];
    }];

    if (listUUID.count > listUuid.count) {
        NSString *uuid = listUUID.lastObject;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@",uuid];
        DBBeacon *dbbeacon = [DBBeacon objectsWithPredicate:predicate].firstObject;
        if (dbbeacon == nil) {
            return;
        }
        predicate = [NSPredicate predicateWithFormat:@"objectId == %@",dbbeacon.locationId];
        DBLocation *dblocation = [DBLocation objectsWithPredicate:predicate].firstObject;
        [self setGeofenceResponce:dblocation.name isEntered:NO isGeofence:NO];
    }
    listUUID = listUuid;
    beaconsDict = beaconsDict;
}

- (void)locationManager:(CLLocationManager *)manager rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error
{
    NSLog(@"locationManager:%@ rangingBeaconsDidFailForRegion:%@ withError:%@", manager, region, error);
    
    [UIAlertController alertControllerWithTitle:@"Ranging Beacons fail"
                                        message:[[NSString alloc] initWithFormat:@"Ranging beacons fail with error: %@", error]
                                 preferredStyle:UIAlertControllerStyleAlert];
}


#pragma mark - Firebase Analytics Events Log
-(void) logEnterLocationEvent:(NSString *)locationName isGeofence:(BOOL)isGeofence{
    NSMutableDictionary *param = [NSMutableDictionary new];
    param[ANALY_PARAM_ENTEREDLOCATION_NAME] = locationName;
    if (isGeofence) {
        [FIRAnalytics logEventWithName:ANALY_EVENT_ENTERGEOFENCE
                            parameters:param];
    }else{
        [FIRAnalytics logEventWithName:ANALY_EVENT_ENTERBEACON
                            parameters:param];
    }
    
}

-(void) logExitLocationEvent:(NSString *)locationName isGeofence:(BOOL)isGeofence{
    NSMutableDictionary *param = [NSMutableDictionary new];
    param[ANALY_PARAM_ENTEREDLOCATION_NAME] = locationName;
    if (isGeofence) {
        [FIRAnalytics logEventWithName:ANALY_EVENT_EXITGEOFENCE
                            parameters:param];
    }else{
        [FIRAnalytics logEventWithName:ANALY_EVENT_EXITBEACON
                            parameters:param];
    }
}

-(void) logIntervalEvent:(NSString *)locationName entereTime:(long long)enterTime{
    long time = [[NSDate date] timestamp];
    int differentTime = (int)((time - enterTime)/1000.0/60.0);// minute

    NSMutableDictionary *param = [NSMutableDictionary new];
    param[ANALY_PARAM_INTERVAL] = @(differentTime);
    param[ANALY_PARAM_ENTEREDLOCATION_NAME] = locationName;
    [FIRAnalytics logEventWithName:ANALY_EVENT_INTERVAL_GEOGENCE
                        parameters:param];
}
#pragma mark - GEOFIRE
- (void)reloadGeofences
{
    self.managedObjectContext = GLOBALINS.managedObjectContext;
    self.insideGeofenceIds = [NSMutableSet set];
    
    QKGeofenceManager *geofenceManager = [QKGeofenceManager sharedGeofenceManager];
    geofenceManager.delegate = self;
    geofenceManager.dataSource = self;
    [geofenceManager reloadGeofences];
}
- (void)setGeoFenceList
{
    for (int i = 0; i<GLOBALINS.dbstamps.count; i++)
    {
        DBStamp *dbstamp = GLOBALINS.dbstamps[i];
        NSPredicate *locationPredicate = [NSPredicate predicateWithFormat: @"objectId == %@", dbstamp.locationId];
        DBLocation *dbLocation = [GLOBALINS.dblocations objectsWithPredicate:locationPredicate].firstObject;
        
        NSString *locationName = dbLocation.name;
        NSString *latitu = dbLocation.latitude;
        NSString *longitu = dbLocation.longitude;
        float latitude = [latitu floatValue];
        float longitude = [longitu floatValue];
        
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
        NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
        
        [newManagedObject setValue:[NSDate date] forKey:@"timeStamp"];
        [newManagedObject setValue:@(latitude)   forKey:@"lat"];
        [newManagedObject setValue:@(longitude)  forKey:@"lon"];
        [newManagedObject setValue:@(GEORADIUS)  forKey:@"radius"];
        
        NSString *identifier = locationName;
        [newManagedObject setValue:identifier forKey:@"identifier"];
        
        // Save the context.
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        [self reloadGeofences];
    }
    [self reloadGeofences];
}

#pragma mark - Fetched results controller
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Geofence" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeStamp" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return _fetchedResultsController;
}

#pragma mark - Geofence Manager
- (NSArray *)geofencesForGeofenceManager:(QKGeofenceManager *)geofenceManager
{
    NSArray *fetchedObjects = [self.fetchedResultsController fetchedObjects];
    NSMutableArray *geofences = [NSMutableArray arrayWithCapacity:[fetchedObjects count]];
    for (NSManagedObject *object in fetchedObjects)
    {
        NSString *identifier = [object valueForKey:@"identifier"];
        CLLocationDegrees lat = [[object valueForKey:@"lat"] doubleValue];
        CLLocationDegrees lon = [[object valueForKey:@"lon"] doubleValue];
        CLLocationDistance radius = [[object valueForKey:@"radius"] doubleValue];
        CLLocationCoordinate2D center = CLLocationCoordinate2DMake(lat, lon);
        CLCircularRegion *geofence = [[CLCircularRegion alloc] initWithCenter:center radius:radius identifier:identifier];
        [geofences addObject:geofence];
    }
    return geofences;
}

- (void)geofenceManager:(QKGeofenceManager *)geofenceManager isInsideGeofence:(CLRegion *)geofence
{
    NSLog(@"%@",geofence.identifier);
    [self.insideGeofenceIds addObject:geofence.identifier];
    [self setGeofenceResponce:geofence.identifier isEntered:YES isGeofence:YES];
}

- (void)geofenceManager:(QKGeofenceManager *)geofenceManager didExitGeofence:(CLRegion *)geofence
{
    NSLog(@"%@",geofence.identifier);
    [self.insideGeofenceIds removeObject:geofence.identifier ];
    [self setGeofenceResponce:geofence.identifier isEntered:NO isGeofence:YES];
}

- (void)geofenceManager:(QKGeofenceManager *)geofenceManager didFailWithError:(NSError *)error
{
    NSString *msg = [error localizedDescription];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];    
}

#pragma mark - Enter or Exit Progress
-(void)setGeofenceResponce :(NSString *)locationName isEntered:(BOOL)isEnter isGeofence:(BOOL)isGeofence
{
    GeofenceModel *geofenceModel = [self setGeofenceModel:isEnter locaitonName:locationName];
    NSPredicate *locationPredicate = [NSPredicate predicateWithFormat:@"name == %@",locationName];
    DBLocation *dbLocation = [[GLOBALINS.dblocations objectsWithPredicate:locationPredicate] sortedResultsUsingKeyPath:FLOCATION_NAME ascending:YES].firstObject;
    NSString *notifiStr;
    if (geofenceModel.isEntered) {
        notifiStr = [NSString stringWithFormat:@"%@%@. %@",WeleComeToT[USERINS.userLanguage],dbLocation.name,TapToT[USERINS.userLanguage]];
        [Global confirmMessage:notifiStr];
        GLOBALINS.isEnterLocation = YES;
        [self logEnterLocationEvent:locationName isGeofence:isGeofence];
    } else {
        notifiStr = [NSString stringWithFormat:@"%@%@.",ThanksForStopT[USERINS.userLanguage],dbLocation.name];
        GLOBALINS.isEnterLocation = NO;
        [self logExitLocationEvent:locationName isGeofence:isGeofence];
        [self showNextStopPopuVCwithGeofenchModel:geofenceModel];
        NSError *error;
        GeofenceModel *geofenceModel_ = [[GeofenceModel alloc] initWithDictionary:(NSDictionary *)[UserDefaults objectForKey:DEFAULT_GEOFENCEMODEL] error:&error];
        [self logIntervalEvent:locationName entereTime:geofenceModel_.milliSecons];
    }
    [Global localNofiticatonWithTitle:notifiStr badgNumber:0];
    
    NSMutableArray *mystamps = [USERINS.mystamps mutableCopy];
    if (!mystamps) mystamps = [NSMutableArray new];
    BOOL canAdd = YES;
    for (NSDictionary *map in mystamps){
        if ([map.allKeys containsObject:dbLocation.objectId]){
            canAdd = NO;
            break;
        }
    }
    if (canAdd) {
        NSMutableDictionary *redeemStamp = [NSMutableDictionary new];
        long timestamp = (long)([[NSDate date] timeIntervalSince1970] *1000);
        redeemStamp[dbLocation.objectId] = [NSNumber numberWithLong:timestamp];
        [mystamps addObject:redeemStamp];

        FUser *user = [FUser currentUser];
        user[FUSER_MYSTAMPS] = mystamps;
        if ((int)mystamps.count == 1){
            user[FUSER_STARTLOCATION] = locationName;
        }else if((int)mystamps.count == [DBLocation allObjects].count){
            user[FUSER_ENDLOCATION] = locationName;
        }
        [user saveInBackground:^(NSError * _Nullable error) {
            if (error == nil) {
                [NotificationCenter post:NOTI_GEOFENCEADDED object:geofenceModel];
            }else{
                [ProgressHUD showError:error.description];
            }
        }];
    }else{
        [NotificationCenter post:NOTI_GEOFENCEADDED object:geofenceModel];
    }
}

-(GeofenceModel *) setGeofenceModel: (BOOL)isEntered locaitonName :(NSString *)locationName
{
    long long time = [[NSDate date] timestamp];
    NSString *dateStr = [Global getDateFromTimestamp:time];
    
    NSPredicate *locationPredicate = [NSPredicate predicateWithFormat:@"name == %@",locationName];
    DBLocation *dbLocation = [[GLOBALINS.dblocations objectsWithPredicate:locationPredicate] sortedResultsUsingKeyPath:FLOCATION_NAME ascending:YES].firstObject;
    NSPredicate *stampPredicate = [NSPredicate predicateWithFormat:@"locationId == %@",dbLocation.objectId];
    DBStamp *dbStamp = [GLOBALINS.dbstamps objectsWithPredicate:stampPredicate].firstObject;

    GeofenceModel *geofenceModel = [GeofenceModel new];
    geofenceModel.milliSecons = time;
    geofenceModel.isEntered = isEntered;
    geofenceModel.dateStr = dateStr;
    geofenceModel.dbLocationId = dbLocation.objectId;
    geofenceModel.dbStampId = dbStamp.objectId;
    // Save to Paper DB
    if (isEntered){
        [UserDefaults setObject:[geofenceModel toDictionary] forKey:DEFAULT_GEOFENCEMODEL];
    }
    return  geofenceModel;
}
- (void)showNextStopPopuVCwithGeofenchModel:(GeofenceModel *)geofenceModel{
    // get RlmObects
    NSPredicate *locationPredicate = [NSPredicate predicateWithFormat:@"objectId == %@",geofenceModel.dbLocationId];
    DBLocation *dblocation = [[DBLocation objectsWithPredicate:locationPredicate] sortedResultsUsingKeyPath:FLOCATION_NAME ascending:YES].firstObject;
    NSPredicate *stampPredicate = [NSPredicate predicateWithFormat:@"objectId == %@",geofenceModel.dbStampId];
    DBStamp *dbStamp = [[DBStamp objectsWithPredicate:stampPredicate] sortedResultsUsingKeyPath:FLOCATION_NAME ascending:YES].firstObject;
    // get Next location info
    long order = dblocation.order;
    NSPredicate *nextlocationPredicate = [NSPredicate predicateWithFormat:@"order == %d",order+1];
    DBLocation *nextlocation = [[DBLocation objectsWithPredicate:nextlocationPredicate] sortedResultsUsingKeyPath:FLOCATION_NAME ascending:YES].firstObject;
    nextStopLocationId = nextlocation.objectId;
    // Get distance
    NSString *startLat = dblocation.latitude;
    NSString *startLon = dblocation.longitude;
    NSString *endLat = nextlocation.latitude;
    NSString *endLon = nextlocation.longitude;
    NSString *urlString = [NSString stringWithFormat:
                           @"%@?origin=%@,%@&destination=%@,%@&sensor=false",
                           @"https://maps.googleapis.com/maps/api/directions/json",
                           startLat,
                           startLon,
                           endLat,
                           endLon];
    NSLog(@"URL : %@",urlString);
    NSURL *directionsURL = [NSURL URLWithString:urlString];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:directionsURL];
        [request startSynchronous];
        NSError *error = [request error];
        if (!error) {
            NSDictionary *json =[NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableContainers error:&error];
            if (json == nil) return;
            if (json[@"routes"] == nil) return;
            if (!((NSArray *)json[@"routes"]).count) return;
            if (json[@"routes"][0][@"legs"] == nil) return;
            NSArray *array = json[@"routes"][0][@"legs"];
            if (array == nil) return;
            
            NSDictionary *dic = array[0];
            NSString *miles = ((NSString *)dic[@"distance"][@"text"]).lowercaseString;
            if ([miles containsString:@"mi"]) {
                miles = [miles stringByReplacingOccurrencesOfString:@"mi" withString:@"miles away"];
            }else if([miles containsString:@"ft"]){
                miles = [miles stringByReplacingOccurrencesOfString:@"ft" withString:@"fts away"];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showNextStopPopupVwithStampName:dbStamp.name nextLocationName:nextlocation.name leftMiles:miles];
            });
            
        }else{
            [self showNextStopPopupVwithStampName:dbStamp.name nextLocationName:nextlocation.name leftMiles:@""];
            NSLog(@"%@",[request error]);
            [ProgressHUD dismiss];
        }
    });
    // Popup vc init
}
-(void)showNextStopPopupVwithStampName:(NSString *)stampName
                      nextLocationName:(NSString *)nextlocation
                             leftMiles:(NSString *)letfMiles{
    NextStopPopuView *view = [[[NSBundle mainBundle] loadNibNamed:@"NextStopPopuView" owner:self options:nil] objectAtIndex:0];
    view.titleL.text = [NSString stringWithFormat:@"%@%@",ThanksVisitT[USERINS.userLanguage],stampName];
    view.readyTL.text = ReadyNextT[USERINS.userLanguage];
    view.nextStopNameL.text = [NSString stringWithFormat:@"%@%@",LetsHeadT[USERINS.userLanguage],nextlocation];
    [view.viewNextStopBtn setTitle:ViewNextT[USERINS.userLanguage] forState:UIControlStateNormal];
    [view.takeMeThereBtn setTitle:TakeMeThereT[USERINS.userLanguage] forState:UIControlStateNormal];
    view.leftMileL.text = letfMiles;
    [view.takeMeThereBtn addTarget:self action:@selector(goNextStopWithAddress) forControlEvents:UIControlEventTouchUpInside];
    [view.viewNextStopBtn addTarget:self action:@selector(viewNextStopBtn) forControlEvents:UIControlEventTouchUpInside];
    if (!nextStopalertView) {
        nextStopalertView = [[CustomIOSAlertView alloc] init];
    }
    [nextStopalertView setContainerView:view];
    [nextStopalertView setBackgroundColor:[UIColor clearColor]];
    [nextStopalertView setButtonTitles:NULL];
    nextStopalertView.closeOnTouchUpOutside = YES;
    [nextStopalertView show];
}
- (void)viewNextStopBtn{
    [NotificationCenter post:NOTI_GOMAPSCREEN];
    [NotificationCenter post:NOTI_GONEXTSTOP object:nextStopLocationId];
    [nextStopalertView close];
}
-(void)goNextStopWithAddress{
    if(nextStopLocationId == nil) return;
    NSPredicate *locationPredicate = [NSPredicate predicateWithFormat:@"objectId == %@",nextStopLocationId];
    DBLocation *dblocation = [[DBLocation objectsWithPredicate:locationPredicate] sortedResultsUsingKeyPath:FLOCATION_NAME ascending:YES].firstObject;
    NSString *str1 = [dblocation.address stringByReplacingOccurrencesOfString:@" " withString:@",+"];
    NSString *str2 = [str1 stringByReplacingOccurrencesOfString:@",+,+" withString:@",+"];
    NSString *str3 = [str2 stringByReplacingOccurrencesOfString:@",," withString:@","];
    NSString *addressString = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@",str3];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:addressString]];
    nextStopLocationId = nil;
    [nextStopalertView close];
}

@end
