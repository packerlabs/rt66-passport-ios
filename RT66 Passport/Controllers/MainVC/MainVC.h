//
//  MainVC.h
//  PWFM
//
//  Created by My Star on 5/18/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
#import <QKGeofenceManager.h>
#import "ZLTabBarController.h"
#import <CoreBluetooth/CoreBluetooth.h>
@interface MainVC : ZLTabBarController<NSFetchedResultsControllerDelegate,QKGeofenceManagerDataSource,QKGeofenceManagerDelegate,CLLocationManagerDelegate>
//@property (weak, nonatomic) IBOutlet UITableView *settingTv;
#pragma mark - GOFENCE
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic) NSMutableSet *insideGeofenceIds;
@property  CBCentralManager *manager;
@end
