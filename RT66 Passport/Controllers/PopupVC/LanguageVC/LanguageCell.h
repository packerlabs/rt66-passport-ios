//
//  LanguageCell.h
//  RT66 Passport
//
//  Created by My Star on 1/5/18.
//  Copyright © 2018 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguageCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *countryImg;
@property (strong, nonatomic) IBOutlet UIImageView *checkImg;
@property (strong, nonatomic) IBOutlet UILabel *languageL;
@property (strong, nonatomic) IBOutlet UILabel *englishL;
@end
