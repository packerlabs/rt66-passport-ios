//
//  LanguageVC.m
//  RT66 Passport
//
//  Created by My Star on 1/5/18.
//  Copyright © 2018 My Star. All rights reserved.
//

#import "LanguageVC.h"
#import "utilities.h"
#import "LanguageCell.h"
@interface LanguageVC ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation LanguageVC
#pragma mark - Super Class Methods
- (void)viewDidLoad {[super viewDidLoad];}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}
- (void)viewWillAppear:(BOOL)animated{
    [self initialize];
    [self setUIs];
}
#pragma mark - Initialize
- (void)initialize{
    [self refreshTableView];
}
- (void)setUIs{
    self.title = [NSString stringWithFormat:@"  %@",SelectLanguageT[USERINS.userLanguage]];
}
#pragma mark - TABLEVIEW DELEGATE
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return GLOBALINS.languages.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LanguageCell *cell = (LanguageCell *)[tableView dequeueReusableCellWithIdentifier:@"LanguageCell" forIndexPath:indexPath];
    NSInteger index = indexPath.row;
    cell.countryImg.image = [UIImage imageNamed:(NSString *)GLOBALINS.langImages[index]];
    cell.languageL.text = GLOBALINS.originLanguages[index];
    cell.englishL.text = [NSString stringWithFormat:@"(%@)", GLOBALINS.languages[index]];
    if (index == GLOBALINS.userLanguage) {
        cell.checkImg.hidden = NO;
    }else{
        cell.checkImg.hidden = YES;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger index = indexPath.row;
    GLOBALINS.userLanguage = (int)index;
    FUser *user = [FUser currentUser];
    if (user.objectId != nil) {
        user[FUSER_LANGUAGE] = @(GLOBALINS.userLanguage);
        [user saveInBackground:^(NSError * _Nullable error) {
            if (error != nil) {
                [ProgressHUD showError:error.description];
            }
        }];
    }
    [self refreshTableView];
}

-(void)refreshTableView{
    dispatch_async(dispatch_get_main_queue(), ^{ [self.tableView reloadData];});
}

@end
