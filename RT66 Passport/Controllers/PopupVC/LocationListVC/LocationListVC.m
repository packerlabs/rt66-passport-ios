//
//  LocationListVC.m
//  RT66 Passport
//
//  Created by My Star on 2/13/18.
//  Copyright © 2018 My Star. All rights reserved.
//

#import "LocationListVC.h"
#import "utilities.h"
#import "LanguageCell.h"
@interface LocationListVC ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation LocationListVC

- (void)viewDidLoad {
    [super viewDidLoad];    
}

- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

- (void)viewWillAppear:(BOOL)animated{
    [self initialize];
    [self setUIs];
}
#pragma mark - Initialize
- (void)initialize{
    [self refreshTableView];
}
- (void)setUIs{
    if (self.isLocationList) {
        self.title = [NSString stringWithFormat:@"  %@",StampLocationT[USERINS.userLanguage]];
    }else{
        self.title = [NSString stringWithFormat:@"  %@",AttractionT[USERINS.userLanguage]];
    }
}
#pragma mark - TABLEVIEW DELEGATE
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.isLocationList) {
        return GLOBALINS.locationCount;
    }else{
        return [DBCommunity allObjects].count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LanguageCell *cell = (LanguageCell *)[tableView dequeueReusableCellWithIdentifier:@"LocationListCell" forIndexPath:indexPath];
    NSInteger index = indexPath.row;
    NSString *url, *name;
    if (self.isLocationList) {
        cell.checkImg.hidden = NO;
        DBLocation *dblocation = GLOBALINS.dblocations[index];
        NSString *images = dblocation.photos;
        NSArray * array = [images componentsSeparatedByString:@","];
        url = (NSString *)array[0];
        name = dblocation.name;
        BOOL wasredeemed = NO;
        for (NSDictionary *map in USERINS.mystamps){
            NSLog(@"%@",map);
            if ([map.allKeys containsObject:dblocation.objectId]){
                wasredeemed = YES;
                break;
            }
        }
        if (wasredeemed) {
            cell.checkImg.image = [UIImage imageNamed:@"enteredmarker"];
        }else{
            cell.checkImg.image = [UIImage imageNamed:@"marker"];
        }
    }else{
        cell.checkImg.hidden = YES;
        DBCommunity *dbCommunity = DBCommunity.allObjects[index];
        url = dbCommunity.icon;
        name = dbCommunity.name;
    }
    cell.countryImg.layer.cornerRadius = cell.countryImg.bounds.size.width/2;
    cell.countryImg.layer.masksToBounds = YES;
    [cell.countryImg sd_setImageWithURL:[NSURL URLWithString:url]];
    cell.languageL.text = name;

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger index = indexPath.row;
    NSString *locationId;
    if (self.isLocationList) {
        DBLocation *dblocation = GLOBALINS.dblocations[index];
        locationId = dblocation.objectId;
    }else{
        DBCommunity *dbCommunity = DBCommunity.allObjects[index];
        locationId = dbCommunity.objectId;
    }
    [NotificationCenter post:NOTI_GONEXTSTOP object:locationId];
    [self dismissModalViewControllerAnimated:YES];
}
-(void)refreshTableView{
    dispatch_async(dispatch_get_main_queue(), ^{ [self.tableView reloadData];});
}
@end
