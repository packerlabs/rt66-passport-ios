//
//  LocationListVC.h
//  RT66 Passport
//
//  Created by My Star on 2/13/18.
//  Copyright © 2018 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationListVC : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property BOOL isLocationList;
@end
