//
//  SharingVC.m
//  ProjectH
//
//  Created by My Star on 10/22/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "SharingVC.h"

@interface SharingVC ()<FBSDKSharingDelegate,UIDocumentInteractionControllerDelegate>

@end

@implementation SharingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setFontFamily:@"Maiden Orange" forView:self.view andSubViews:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [self setUIs];
    [self initialize];
}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs{
    self.title = ChooseShareT[USERINS.userLanguage];
}
- (void) initialize
{
}
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}
#pragma mark - BUTTON ACTIONS

- (IBAction)facebookBtn:(id)sender {
    FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
    photo.image = self.image;
    photo.userGenerated = YES;
    FBSDKSharePhotoContent *contentPhoto = [[FBSDKSharePhotoContent alloc] init];
    contentPhoto.photos = @[photo];
    [FBSDKShareDialog showFromViewController:self withContent:contentPhoto delegate:self];
}
- (IBAction)twitterBtn:(id)sender {
    TWTRComposer *composer = [[TWTRComposer alloc] init];
//    [composer setText:@"Test share"];
    [composer setImage:self.image];
    
    // Called from a UIViewController
    [composer showFromViewController:self completion:^(TWTRComposerResult result) {
        if (result == TWTRComposerResultCancelled) {
            NSLog(@"Tweet composition cancelled");
        }
        else {
            NSLog(@"Sending Tweet!");
        }
    }];
}
- (IBAction)instagramBtn:(id)sender {
    UIImage *image = self.image;
    NSData *imagedata = UIImageJPEGRepresentation(image, 0.5);
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"insta.igo"]];
    [fileManager createFileAtPath:fullPath contents:imagedata attributes:nil];
    //    NSURL *_instagramURL = [NSURL URLWithString:[NSString stringWithFormat: @"instagram://app"]];
    CGRect rect = CGRectMake(0 ,0 , 0, 0);
    NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", fullPath]];
    self.dic.UTI = @"com.instagram.photo";
    self.dic = [self setupControllerWithURL:igImageHookFile usingDelegate:self];
    self.dic=[UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
    [self.dic presentOpenInMenuFromRect: rect    inView: self.view animated: YES ];
}
- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    return interactionController;
}
#pragma mark  Facebook Shareing Delegate
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
    
}
- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    
}
- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    
}
@end
