//
//  MapProfileV.m
//  ITAGG
//
//  Created by My Star on 3/30/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "DetailStampAlertV.h"

@implementation DetailStampAlertV
- (void)awakeFromNib {
    [super awakeFromNib];
    [self setFontFamily:@"Maiden Orange" forView:self andSubViews:YES];
}
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}
@end
