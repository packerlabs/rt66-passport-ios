//
//  MapProfileV.h
//  ITAGG
//
//  Created by My Star on 3/30/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
@interface LanguageV : UIView

@property (weak, nonatomic) IBOutlet UIImageView *iConIv;
@property (weak, nonatomic) IBOutlet UILabel *nameL;

@property (weak, nonatomic) IBOutlet UILabel *descL;

@property (weak, nonatomic) IBOutlet UIView *redeemdateContainV;
@property (weak, nonatomic) IBOutlet UILabel *redeemeddateL;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UIButton *viewPhotoBtn;

@end
