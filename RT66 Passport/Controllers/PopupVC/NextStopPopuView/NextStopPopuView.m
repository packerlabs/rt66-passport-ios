//
//  NextStopPopuVC.m
//  RT66 Passport
//
//  Created by My Star on 11/29/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "NextStopPopuView.h"
#import "utilities.h"
@implementation NextStopPopuView
#pragma mark - Super Class
- (void)awakeFromNib {
    [super awakeFromNib];
    [Global roundCornerSet:self.containView];
}
@end
