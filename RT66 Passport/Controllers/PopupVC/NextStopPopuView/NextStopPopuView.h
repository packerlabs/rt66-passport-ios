//
//  NextStopPopuVC.h
//  RT66 Passport
//
//  Created by My Star on 11/29/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NextStopPopuView : UIView
@property (weak, nonatomic) IBOutlet UIView *containView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *nextStopNameL;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *takeMeThereBtn;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *viewNextStopBtn;
@property (weak, nonatomic) IBOutlet UILabel *leftMileL;

@property (weak, nonatomic) IBOutlet UILabel *readyTL;
@end
