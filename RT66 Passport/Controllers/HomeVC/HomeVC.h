//
//  UserListVC.h
//  ITAGG
//
//  Created by My Star on 5/2/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
@interface HomeVC : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *passportImg;
@property (weak, nonatomic) IBOutlet UIView *scrollContainV;
@property (weak, nonatomic) IBOutlet UILabel *mytripItemNameL;
@property (weak, nonatomic) IBOutlet UIView *getStartView;

@property (weak, nonatomic) IBOutlet UILabel *myTripTL;
@property (weak, nonatomic) IBOutlet UILabel *getStartTL;
@property (weak, nonatomic) IBOutlet UIButton *findLocationBtn;

@end
