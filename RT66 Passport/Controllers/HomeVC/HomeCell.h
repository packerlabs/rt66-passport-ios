//
//  UserListCell.h
//  ITAGG
//
//  Created by My Star on 5/2/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
@interface HomeCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userImg;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *locationNameL;
@property (weak, nonatomic) IBOutlet UIView *containV;
@end
