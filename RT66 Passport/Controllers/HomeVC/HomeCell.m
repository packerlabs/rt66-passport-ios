//
//  UserListCell.m
//  ITAGG
//
//  Created by My Star on 5/2/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "HomeCell.h"

@implementation HomeCell
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.locationNameL.textAlignment = NSTextAlignmentLeft;
    self.locationNameL.textColor = [UIColor blackColor];
    self.locationNameL.font = [UIFont systemFontOfSize:12];
    self.locationNameL.labelSpacing = 35;
    self.locationNameL.pauseInterval = 1.5;
    self.locationNameL.scrollSpeed = 30;
    self.locationNameL.fadeLength = 12.f;
}

@end
