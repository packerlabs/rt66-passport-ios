//
//  UserListVC.m
//  ITAGG
//
//  Created by My Star on 5/2/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "HomeVC.h"
#import "CardScrollView.h"
@interface HomeVC ()<CardScrollViewDelegate>{
    RLMResults *mytrips;
    NSInteger currentMytripIndex;
    NSMutableArray *myTripIvs;
}

@end

@implementation HomeVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setFontFamily:@"Maiden Orange" forView:self.view andSubViews:YES];
}
- (void)viewWillAppear:(BOOL)animated{
    
    [self initialize];
    if (![USERINS.mytrips count]) {
//        [self.view makeToast:@"You don't have no any trips."];
        self.mytripItemNameL.text = @"";
        mytrips = nil;
    }
    NSPredicate *betweenPredicate = [NSPredicate predicateWithFormat: @"objectId IN %@", USERINS.mytrips];
    mytrips = [GLOBALINS.dblocations objectsWithPredicate:betweenPredicate];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setUIs];
    });
    [self performSelector:@selector(delayedReload) withObject:nil afterDelay:0.25];
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs{
    GLOBALINS.mainVC.navigationItem.title = HomeT[USERINS.userLanguage];
    if (mytrips.count>0) {
        [self.getStartView setHidden:YES];
    }else{
        [self.getStartView setHidden:NO];
    }
    NSArray *viewsToRemove = [self.scrollContainV subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    [self setTranslations];
//    NSString *url = @"https://content.niagaracruises.com/sites/default/files/styles/hero_large/public/images/hero/carousel/HNC-NiagaraFalls-BannerSlider_1.jpg";
//    [self.passportImg sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"eventscell"]];
    if (!mytrips.count) {
        return;
    }
    [self sliderViewSet];
    
}
- (void) setTranslations{
    [self.findLocationBtn setTitle:FindLocationT[USERINS.userLanguage] forState:UIControlStateNormal];
    self.myTripTL.text = My_TripsT[USERINS.userLanguage];
    self.getStartTL.text = GetStartT[USERINS.userLanguage];
}
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}


-(void)sliderViewSet
{
    DBLocation *location = mytrips[0];
    self.mytripItemNameL.text = location.name;
    [self.scrollContainV setNeedsLayout];
    [self.scrollContainV layoutIfNeeded];
    CGFloat width = self.scrollContainV.bounds.size.width;
    CGFloat height = self.scrollContainV.bounds.size.height;
    CGFloat pointX = width/15.0;
    CGFloat width_ = width - pointX*4;
    CGFloat height_ = height - pointX;
    NSMutableArray *views = [[NSMutableArray alloc] init];
    myTripIvs = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<[mytrips count]; i++)
    {
        DBLocation *location = mytrips[i];
        NSString *images = location.photos;
        NSArray * array = [images componentsSeparatedByString:@","];
        NSString *url = (NSString *)array[0];
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width_, height_)];
        UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width_, height_)];
        [imageV sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"eventscell"]];
        [Global roundCornerSet:imageV];
        [view addSubview:imageV];
        
        [views addObject:view];
        [myTripIvs addObject:imageV];
    }
    CardScrollView *cardScroll = [[CardScrollView alloc] initWithViews:views atPoint:CGPointMake(width/15.0, 0)];
    cardScroll.delegate =  self;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    
    [cardScroll addGestureRecognizer:singleFingerTap];
    [self.scrollContainV addSubview:cardScroll];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
//    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    [self goLocationDetailVC:nil];
    //Do stuff here...
}
-(void)initialize
{

}
#pragma mark - BUTTON ACTION

- (IBAction)goMapVC:(id)sender {
    [NotificationCenter post:NOTI_GOMAPSCREEN];    
}

-(void)delayedReload{
    [ProgressHUD dismiss];
}
- (void)goLocationDetailVC:(id)sender{    
    DBLocation *location = mytrips[currentMytripIndex];
    LocationDetailVC *locationDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LocationDetailVC"];
    locationDetailVC.dblocation = location;
    [GLOBALINS.mainVC.navigationController pushViewController:locationDetailVC animated:YES];
}
-(void)goIDMPhotoBrowser:(id)sender{
    UIButton *btn = (UIButton*)sender;
    NSInteger index = btn.tag;
    DBLocation *location = mytrips[index];
    UIImageView *imageV = myTripIvs[index];
    NSMutableArray *iDMPhotos = [[NSMutableArray alloc] init];
    NSString *images = location.photos;
    NSString *flagUrl = [NSString stringWithFormat:@"%@us.png",API_COUNTRYFLAG];
    images = [NSString stringWithFormat:@"%@,%@",images,flagUrl];
    NSArray * imageUrls = [images componentsSeparatedByString:@","];
    for (NSString *photoUrl in imageUrls)
    {
        IDMPhoto *photo = [IDMPhoto photoWithURL:[NSURL URLWithString:photoUrl]];
        [iDMPhotos addObject:photo];
    }
    
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:iDMPhotos animatedFromView:imageV];
    browser.scaleImage = imageV.image;
    //    browser.delegate = self;
    browser.displayCounterLabel = YES;
    browser.displayActionButton = NO;
    [self.navigationController presentViewController:browser animated:YES completion:nil];
}
#pragma mark - CardScrollViewDelegate

- (void)pagingScrollView:(CardScrollView *)pagingScrollView scrolledToPage:(NSInteger)currentPage {
    currentMytripIndex = currentPage;
    DBLocation *location = mytrips[currentPage];
    self.mytripItemNameL.text = location.name;
}
@end
