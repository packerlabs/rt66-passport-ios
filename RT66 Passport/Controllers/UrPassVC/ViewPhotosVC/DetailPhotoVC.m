//
//  DetailPhotoVC.m
//  RT66 Passport
//
//  Created by My Star on 11/2/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "DetailPhotoVC.h"

@interface DetailPhotoVC ()

@end

@implementation DetailPhotoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setFontFamily:@"Maiden Orange" forView:self.view andSubViews:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    [self initialize];
    [self setUIs];
}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs{
    CGRect rect = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    DraggableViewBackground *draggableBackground = [[DraggableViewBackground alloc]initWithFrame:rect photoArray:self.photos startIndex:self.index];
    [self.view addSubview:draggableBackground];
}
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}
- (void) initialize
{
}
#pragma mark - BUTTON ACTIONS


@end
