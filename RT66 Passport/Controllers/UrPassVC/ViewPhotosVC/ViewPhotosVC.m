//
//  BufferVC.m
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "ViewPhotosVC.h"
#import "PassPortCell.h"
#import <MessageUI/MessageUI.h>

@interface ViewPhotosVC ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *photos;
}
@end

@implementation ViewPhotosVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUIs];
    [self initialize];
    [self setFontFamily:@"Maiden Orange" forView:self.view andSubViews:YES];
}
-(void)viewWillAppear:(BOOL)animated{

}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs{
    self.navigationItem.title = PhotosT[USERINS.userLanguage];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btnBackMain"]
                                                                             style:UIBarButtonItemStylePlain target:self action:@selector(actionBack)];

}
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}
#pragma mark - BUTTON ACTIONS
- (void)actionBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) initialize
{    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_main_queue(),^{
        NSError *error;
        photos = [NSMutableArray new];
        for (int i = 0; i<GLOBALINS.stampPhotos.count; i++) {
            PhotoModel *photoModel = [[PhotoModel alloc] initWithDictionary:GLOBALINS.stampPhotos[i] error:&error];
            if ([photoModel.stampId isEqualToString:self.stamp.objectId]) {
                UIImage *img = [Global stringToUIImage:photoModel.photoStr];
                [photos addObject:img];
            }
        }
        [self.tableV reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}



#pragma mark - TABLEVIEW DELEGATE

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return photos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PassPortCell *cell = (PassPortCell *)[tableView dequeueReusableCellWithIdentifier:@"ViewPhotosCell" forIndexPath:indexPath];
    NSError *error;
    PhotoModel *photoModel = [[PhotoModel alloc] initWithDictionary:GLOBALINS.stampPhotos[indexPath.row] error:&error];
    cell.userImg.image = photos[indexPath.row];
    cell.locationNameL.text = self.stamp.name;
    cell.stateL.text = photoModel.date;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailPhotoVC *detailPhotoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailPhotoVC"];
    detailPhotoVC.photos = photos;
    detailPhotoVC.index = indexPath.row;
    [self.navigationController pushViewController:detailPhotoVC animated:YES];
}

-(void)refreshCollectionV{
    dispatch_async(dispatch_get_main_queue(), ^{ [self.tableV reloadData];});
}

#pragma mark - OTHERs

@end
