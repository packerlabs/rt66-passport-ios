//
//  UrPassVC.m
//  RT66 Passport
//
//  Created by My Star on 8/4/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "UrPassVC.h"
#import "StampCell.h"
#import "DetailStampAlertV.h"
#import "SharingVC.h"
#import "ZLTabBarItem.h"
#import "ZLTabBarItem.h"
@interface UrPassVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate>{
    CustomIOSAlertView *alertView;
    NSString *selectedStampId;
    UIDocumentInteractionController *dic;
    UIImage *brandImg;
    DBStamp *currentStamp;
    DBLocation *currentLocation;
}


@end

@implementation UrPassVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
    [self setFontFamily:@"Maiden Orange" forView:self.view andSubViews:YES];
//    NSArray *array = [NSArray arrayWithObjects:@"some",@"some",@"some", nil];
//    NSDictionary *dic = @{@"address":@"Some",
//                          @"photos":array,
//                          @"display_name":@"Some",
//                          @"description":@"Some",
//                          @"icon":@"Some",
//                          @"locationId":@"-KsUwpWwvjpMXTcJd63x",
//                          @"website":@"Some"};
//
//    FObject *location = [FObject objectWithPath:FSTAMP_PATH dictionary:dic];
//    [location saveInBackground:^(NSError * _Nullable error) {
//        
//    }];
}
-(void)viewWillAppear:(BOOL)animated{
    [self setUIs];
}
- (void)initialize
{
    [NotificationCenter addObserver:self selector:@selector(reloadTable:) name:NOTI_GEOFENCEADDED];
    brandImg = [UIImage imageNamed:@"bg"];
    
    // Pulse View Set
    self.pulseV.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    self.pulseV.colors = @[(__bridge id)HEXCOLOR(0xf6604dFF).CGColor,(__bridge id)HEXCOLOR(0xf6604dFF).CGColor];
    float buttonR = 50;
    self.pulseV.minRadius = buttonR/2.0;
    self.pulseV.maxRadius = buttonR;
    self.pulseV.duration = 3;
    self.pulseV.count = 1;
    self.pulseV.lineWidth = 3.0f;    
}
-(void)setUIs{
    [GLOBALINS badgeInit];
    GLOBALINS.mainVC.navigationItem.title = PassportT[USERINS.userLanguage];;
    [self.StampCV reloadData];
    if (GLOBALINS.isEnterLocation) {
        [self.takephotoBtn setHidden:NO];
        [self.pulseV startAnimation];
        NSError *error;
        GeofenceModel *geofenceModel = [[GeofenceModel alloc] initWithDictionary:(NSDictionary *)[UserDefaults objectForKey:DEFAULT_GEOFENCEMODEL] error:&error];
        NSPredicate *locationPredicate = [NSPredicate predicateWithFormat:@"objectId == %@",geofenceModel.dbLocationId];
        currentLocation = [[GLOBALINS.dblocations objectsWithPredicate:locationPredicate] sortedResultsUsingKeyPath:FLOCATION_NAME ascending:YES].firstObject;
        NSPredicate *stampPredicate = [NSPredicate predicateWithFormat:@"objectId == %@",geofenceModel.dbStampId];
        currentStamp = [[GLOBALINS.dbstamps objectsWithPredicate:stampPredicate] sortedResultsUsingKeyPath:FLOCATION_NAME ascending:YES].firstObject;
        NSString *iconUrl = currentStamp.icon;
        if (!iconUrl.length) {
            brandImg = GLOBALINS.placeholder;
        }else{
            NSURL *imageURL = [NSURL URLWithString:iconUrl];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                dispatch_async(dispatch_get_main_queue(), ^{
                    // Update the UI
                    brandImg = [UIImage imageWithData:imageData];
                });
            });
        }


    }else{
        [self.takephotoBtn setHidden:YES];
        [self.pulseV stopAnimation];
    }

}
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}
#pragma mark - BUTTON ACTION
- (IBAction)takePhotoBtn:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:TakephotoT[USERINS.userLanguage] style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {  PresentPhotoCamera(self, YES); }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:ChooseGalleryT[USERINS.userLanguage] style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) { PresentPhotoLibrary(self, YES); }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:CancelT[USERINS.userLanguage] style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:action1]; [alert addAction:action2]; [alert addAction:action3];
    [self presentViewController:alert animated:YES completion:nil];
    
    
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//
//    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Open camera" style:UIAlertActionStyleDefault
//                                                    handler:^(UIAlertAction *action) {  PresentPhotoCamera(self, YES); }];
//    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
//    [alert addAction:action1]; [alert addAction:action3];
//    [self presentViewController:alert animated:YES completion:nil];
}
-(void)goViewPhotoVC{
    NSError *error;
    BOOL hasPhoto = NO;
    for (int i = 0; i<GLOBALINS.stampPhotos.count; i++) {
        PhotoModel *photoModel = [[PhotoModel alloc] initWithDictionary:GLOBALINS.stampPhotos[i] error:&error];
        if ([photoModel.stampId isEqualToString:selectedStampId]) {
            hasPhoto = YES;
            break;
        }
    }
    if (!hasPhoto) {
        [alertView makeToast:NoPhotoYetT[USERINS.userLanguage]];
        return;
    }
    ViewPhotosVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewPhotosVC"];
    NSPredicate *betweenPredicate = [NSPredicate predicateWithFormat: @"objectId == %@", selectedStampId];
    vc.stamp = [GLOBALINS.dbstamps objectsWithPredicate:betweenPredicate].firstObject;
    [GLOBALINS.mainVC.navigationController pushViewController:vc animated:YES];
    [self alertClose];
}
#pragma mark - COLLECTIONVIEW DELEGATE
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return GLOBALINS.dbstamps.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    long getDate;
    DBStamp *dbstamp = GLOBALINS.dbstamps[indexPath.row];
    NSString *locationId = dbstamp.locationId;
    getDate = -1;
    for (NSDictionary *map in USERINS.mystamps){
        NSLog(@"%@",map);
        if ([map.allKeys containsObject:locationId]){
            getDate = [map[locationId] longLongValue];            
            break;
        }
    }
    [self showAlertView:dbstamp :getDate];
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"StampCell";
    StampCell *cell = (StampCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
//    [Global roundBorderSet:cell.cellContainV];
    DBStamp *dbstamp = GLOBALINS.dbstamps[indexPath.row];
    NSString *locationId = dbstamp.locationId;
    [cell.iv_alpha setHidden:NO];
    [cell.userImg sd_setImageWithURL:[NSURL URLWithString:dbstamp.icon] placeholderImage:[UIImage imageNamed:@"eventscell"]];
    cell.nameL.text = dbstamp.name;    
    for (NSDictionary *map in USERINS.mystamps){
        if ([map.allKeys containsObject:locationId]){
            [cell.iv_alpha setHidden:YES];
            break;
        }
    }
    if (GLOBALINS.isEnterLocation) {
        if ([currentStamp.objectId isEqualToString:dbstamp.objectId]){
            [cell.pulseV startAnimation];
        }else{
            [cell.pulseV stopAnimation];
        }
    }else{
        [cell.pulseV stopAnimation];
    }
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH - 10 )/3.0,SCREEN_WIDTH/3.0);
}
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:NO completion:nil];
    [self logTakePhotoInLocationEvent];
    [self popupSharingVC:image];
}

#pragma mark - OTHERs
-(void)saveImageToLocal:(UIImage *)image
{
    PhotoModel *photomodel = [[PhotoModel alloc] init];
    photomodel.photoStr = [Global imageToNSString:image];
    if (currentStamp) {
        photomodel.stampId = currentStamp.objectId;
        photomodel.photoName = currentStamp.name;
    }
    photomodel.date = [Global getDateFromTimestamp:[[NSDate date] timestamp]];
    NSDictionary *photoDic = [photomodel toDictionary];
    if(!GLOBALINS.stampPhotos) GLOBALINS.stampPhotos = [NSMutableArray new];
    [GLOBALINS.stampPhotos addObject:photoDic];
    [UserDefaults setObject:GLOBALINS.stampPhotos forKey:STAMPPHOTOS];
}
-(void)showAlertView:(DBStamp *)dbstamp :(long)redeemedDate{
    if (!alertView) {
        alertView = [[CustomIOSAlertView alloc] init];
    }
    selectedStampId = dbstamp.objectId;
    NSString *dateStr = [Global getDateFromTimestamp:redeemedDate];
    dispatch_async(dispatch_get_main_queue(), ^{
        DetailStampAlertV *detailStampAlertV = [[[NSBundle mainBundle] loadNibNamed:@"DetailStampAlertV" owner:self options:nil] objectAtIndex:0];
        CGRect newFrame = detailStampAlertV.frame;
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        newFrame.size.width = self.view.frame.size.width;
        newFrame.size.height = self.view.frame.size.height-20;
        newFrame.origin.x = 0;
        newFrame.origin.y = 10;
        [detailStampAlertV setFrame:newFrame];
        
        [detailStampAlertV.iConIv setNeedsLayout];
        [detailStampAlertV.iConIv layoutIfNeeded];
        detailStampAlertV.iConIv.layer.cornerRadius = detailStampAlertV.iConIv.bounds.size.width/2;
        [[detailStampAlertV.iConIv layer] setBorderWidth: 2.0f];
        detailStampAlertV.iConIv.layer.borderColor = HEXCOLOR(0x2B6A9DFF).CGColor;
        detailStampAlertV.iConIv.layer.masksToBounds = YES;
        detailStampAlertV.attractionTL.text = AttractionT[USERINS.userLanguage];
        
        if (redeemedDate<0) {
            [detailStampAlertV.redeemdateContainV setHidden:YES];
            [detailStampAlertV.viewPhotoBtn setHidden:YES];
        }else{
            [detailStampAlertV.redeemdateContainV setHidden:NO];
            [detailStampAlertV.viewPhotoBtn setHidden:NO];
            [detailStampAlertV.viewPhotoBtn setTitle:View_PhotosT[USERINS.userLanguage] forState:UIControlStateNormal];
            detailStampAlertV.redeemeddateL.text = dateStr;
        }
        NSArray *descs;
        NSString *descriptions = dbstamp.desc;
        descs = [descriptions componentsSeparatedByString:SENTENCEPOINT];
        detailStampAlertV.descL.text = (NSString *)descs[USERINS.userLanguage];
        detailStampAlertV.nameL.text = dbstamp.name;
        
        [detailStampAlertV.iConIv sd_setImageWithURL:[NSURL URLWithString:dbstamp.icon] placeholderImage:[UIImage imageNamed:@"preload"]];
        [detailStampAlertV.closeBtn addTarget:self action:@selector(alertClose) forControlEvents:UIControlEventTouchUpInside];
        [detailStampAlertV.viewPhotoBtn addTarget:self action:@selector(goViewPhotoVC) forControlEvents:UIControlEventTouchUpInside];
        [alertView setContainerView:detailStampAlertV];
        [alertView setBackgroundColor:[UIColor clearColor]];
        [alertView setButtonTitles:NULL];
        alertView.closeOnTouchUpOutside = YES;
        
//        [alertView setAlpha:0.7];
        [alertView show];
    });
}
-(void)popupSharingVC :(UIImage *)image{
    SharingVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SharingVC"];
    vc.image = [self ovelayImageOnFaceWithBranded:image];
    vc.contentSizeInPopup = CGSizeMake(220, 150);
    vc.landscapeContentSizeInPopup = CGSizeMake(220, 150);
    [Global popupVCwithFrom:GLOBALINS.mainVC to:vc];
}
-(void)alertClose{
    [alertView close];
}
-(void)reloadTable:(id)obj{
    [self.StampCV reloadData];

    GeofenceModel *geofenceModel = (GeofenceModel*)[obj object];
    NSPredicate *locationPredicate = [NSPredicate predicateWithFormat:@"objectId == %@",geofenceModel.dbLocationId];
    currentLocation = [[GLOBALINS.dblocations objectsWithPredicate:locationPredicate] sortedResultsUsingKeyPath:FLOCATION_NAME ascending:YES].firstObject;
    NSPredicate *stampPredicate = [NSPredicate predicateWithFormat:@"objectId == %@",geofenceModel.dbStampId];
    currentStamp = [[GLOBALINS.dbstamps objectsWithPredicate:stampPredicate] sortedResultsUsingKeyPath:FLOCATION_NAME ascending:YES].firstObject;

    if (geofenceModel.isEntered) {
        [self.pulseV startAnimation];
        [self.takephotoBtn setHidden:NO];
    }else{
        [self.pulseV stopAnimation];
        [self.takephotoBtn setHidden:YES];
        currentStamp = nil;
    }
}
-(void)savePhotoUrlToUser:(NSString *)url fileName:(long long)timestamp
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    NSMutableArray *attractionphotos = USERINS.attractionphotos;
    dic[[@(timestamp) stringValue]] = url;
    [attractionphotos addObject:dic];
    FUser *user = [FUser currentUser];
    user[FUSER_ATTRACTIONPHOTOS] = attractionphotos;
    [user saveInBackground:^(NSError * _Nullable error) {
        if (error) {
            [ProgressHUD showError:error.description];
        }
    }];    
}
-(void)logTakePhotoInLocationEvent{
    NSMutableDictionary *param = [NSMutableDictionary new];
    param[ANALY_PARAM_ENTEREDLOCATION_NAME] = currentLocation.name;
    [FIRAnalytics logEventWithName:ANALY_EVENT_TAKEPHOTO
                        parameters:param];
}
#pragma mark - SOCIAL CHANNEL SHAREING
- (UIImage*)ovelayImageOnFaceWithBranded :(UIImage *)backImg{
    
    CGFloat width, height;
    width=backImg.size.width;
    height=backImg.size.height;
    
    UIImage *outputImage;
    if(brandImg){
        // create a new bitmap image context at the device resolution (retina/non-retina)
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(width, height), YES, 0.0);
        
        // get context
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        // push context to make it current
        // (need to do this manually because we are not drawing in a UIView)
        UIGraphicsPushContext(context);
        
        // drawing code comes here- look at CGContext reference
        // for available operations
        // this example draws the inputImage into the context
        CGFloat brandW, brandH;
        brandW = width/5.0;
        brandH = brandW/1.0;
        [backImg drawInRect:CGRectMake(0, 0, width, height)];
        [brandImg drawInRect:CGRectMake(width-brandW-10, 10, brandW, brandH)];
        // pop context
        UIGraphicsPopContext();
        
        // get a UIImage from the image context- enjoy!!!
        outputImage = UIGraphicsGetImageFromCurrentImageContext();
        
        // clean up drawing environment
        UIGraphicsEndImageContext();
    }
    [self saveImageToLocal:outputImage];
    return outputImage;
}


- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}


@end
