//
//  UserListCell.m
//  ITAGG
//
//  Created by My Star on 5/2/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "StampCell.h"

@implementation StampCell
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.nameL.textAlignment = NSTextAlignmentLeft;
    self.nameL.textColor = [UIColor blackColor];
    self.nameL.font = [UIFont systemFontOfSize:12];
    self.nameL.labelSpacing = 35;
    self.nameL.pauseInterval = 1.5;
    self.nameL.scrollSpeed = 30;
    self.nameL.fadeLength = 12.f;
    
    // Pusing setting
    self.pulseV.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    self.pulseV.colors = @[(__bridge id)HEXCOLOR(0xf6604dFF).CGColor,(__bridge id)HEXCOLOR(0xf6604dFF).CGColor];
    float buttonR = 15;
    self.pulseV.minRadius = 0.0;
    self.pulseV.maxRadius = buttonR;
    self.pulseV.duration = 2;
    self.pulseV.count = 4;
    self.pulseV.lineWidth = 2.0f;
}
@end
