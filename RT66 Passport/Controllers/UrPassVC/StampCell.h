//
//  UserListCell.h
//  ITAGG
//
//  Created by My Star on 5/2/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"

@interface StampCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userImg;
@property (weak, nonatomic) IBOutlet MMPulseView *pulseV;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *nameL;
@property (weak, nonatomic) IBOutlet UIImageView *iv_alpha;

@end
