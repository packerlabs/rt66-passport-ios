//
//  BufferVC.m
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "QuestionFourVC.h"
#import "CheckFollowCell.h"
@interface QuestionFourVC ()<UITableViewDataSource,UITableViewDelegate>{
    UIImage *checkedImg;
}

@end

@implementation QuestionFourVC

- (void)viewDidLoad {
    [super viewDidLoad];    
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}
-(void)viewWillAppear:(BOOL)animated{
    [self initialize];
    [self setUIs];
}

-(void)setUIs{
    [Global roundBorderSet:self.tableview borderColor:[UIColor whiteColor]];
    [self setTranslations];
}
- (void) setTranslations{
    self.titleTL.text = WhichVisitT[USERINS.userLanguage];   
}
- (void) initialize
{
    [NotificationCenter post:NOTI_CHANGEPROGRESS object:@(3/9.0)];
    checkedImg = [UIImage imageNamed:@"checkmark"];
}

#pragma mark - BUTTON ACTIONS

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return GLOBALINS.locationNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    CheckFollowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CheckFollowCell" forIndexPath:indexPath];
    DBLocation *dblocation = GLOBALINS.dblocations[indexPath.row];
    if ([USERINS.followlocations containsObject:dblocation.objectId]) {
        cell.checkImg.image = checkedImg;
    }else{
        cell.checkImg.image = nil;
    }
    cell.locationNameL.text = dblocation.name;
    return cell;
}
#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DBLocation *dblocation = GLOBALINS.dblocations[indexPath.row];
    
    NSMutableArray *follows = USERINS.followlocations;
    if(!follows) follows = [NSMutableArray new];
    if ([follows containsObject:dblocation.objectId]) {
        [follows removeObject:dblocation.objectId];
    }else{
        [follows addObject:dblocation.objectId];
    }
    
    FUser *user = FUser.currentUser;
    user[FUSER_FOLLOWLOCATIONS] = follows;
    [user saveInBackground:^(NSError * _Nullable error) {
        if (error == nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableview reloadData];
            });
        }
    }];   
}
#pragma mark - BUTTON ACTIONS
- (IBAction)nextBtn:(id)sender {
    [NotificationCenter post:NOTI_QUESTIONNEXT];
}
- (IBAction)backBtn:(id)sender {
    [NotificationCenter post:NOTI_QUESTIONBACK];
}
@end
