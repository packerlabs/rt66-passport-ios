
#import <UIKit/UIKit.h>
#import "utilities.h"
@interface QuestionOneVC : UIViewController
@property (weak, nonatomic) IBOutlet ACFloatingTextField *cityF;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *stateF;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *countryF;
@property (weak, nonatomic) IBOutlet UILabel *ifoutsideTL;
@end
