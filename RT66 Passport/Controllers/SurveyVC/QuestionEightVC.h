//
//  QuestionVC.h
//  ProjectH
//
//  Created by My Star on 10/20/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
@interface QuestionEightVC : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *noBtn;
@property (strong, nonatomic) IBOutlet UIButton *yesBtn;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *destinationF;
@property (weak, nonatomic) IBOutlet UILabel *titleTL;
@property (weak, nonatomic) IBOutlet UILabel *ifnotTL;
@property (weak, nonatomic) IBOutlet UILabel *noTL;
@property (weak, nonatomic) IBOutlet UILabel *yesTL;

@end
