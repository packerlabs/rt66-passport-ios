
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "QuestionThreeVC.h"

@interface QuestionThreeVC ()<UITextFieldDelegate>

@end

@implementation QuestionThreeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [NotificationCenter addObserver:self selector:@selector(setStartLocationTextField:) name:NOTI_SELECTSTARTLOCATION];
    [NotificationCenter addObserver:self selector:@selector(setEndLocationTextField:) name:NOTI_SELECTENDLOCATION];
    [NotificationCenter addObserver:self selector:@selector(setEndLocationTextField:) name:NOTI_HIDESURVEYTITLE];   
}
-(void)viewWillAppear:(BOOL)animated{
    [self initialize];
    [self setUIs];
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs{
    self.startLocationF.delegate = self;
    self.endLocationF.delegate = self;
    if (USERINS.startlocation.length) self.startLocationF.text = USERINS.startlocation;
    if (USERINS.endlocation.length) self.endLocationF.text = USERINS.endlocation;
    [self setTranslations];
}
- (void) setTranslations{
    self.titleTL.text = StartEndT[USERINS.userLanguage];
    self.startLocationF.placeholder = StartT[USERINS.userLanguage];
    self.endLocationF.placeholder = EndT[USERINS.userLanguage];
}
- (void) initialize
{
    [NotificationCenter post:NOTI_CHANGEPROGRESS object:@(2/9.0)];
}
#pragma mark - Notification process
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    switch (textField.tag) {
        case 0:
            [NotificationCenter post:NOTI_SHOWPICKER object:@YES];
            break;
        case 1:
            [NotificationCenter post:NOTI_SHOWPICKER object:@NO];
            break;
        default:
            break;
    }
}

#pragma mark - Notification process
-(void)setStartLocationTextField:(id)obj{
    NSString *startLocation = [obj object];
    NSString *endLocation = self.endLocationF.text;
    if ([startLocation isEqualToString:endLocation]) {
        NSString *msg = StartVisitT[USERINS.userLanguage];
        [self.view makeToast:msg duration:1.0 position:CSToastPositionCenter];
        return;
    }
    self.startLocationF.text = startLocation;
}
-(void)setEndLocationTextField:(id)obj{
    NSString *endLocation = [obj object];
    NSString *startLocation = self.startLocationF.text;
    if ([startLocation isEqualToString:endLocation]) {
        NSString *msg = EndVisitT[USERINS.userLanguage];
        [self.view makeToast:msg duration:1.0 position:CSToastPositionCenter];
        return;
    }
    self.endLocationF.text = endLocation;
}

#pragma mark - BUTTON ACTIONS
- (IBAction)nextBtn:(id)sender {
    NSString *startLocation = self.startLocationF.text;
    NSString *endLocation = self.endLocationF.text;
    if (startLocation.length) [CurrentUser saveUserInfo:FUSER_STARTLOCATION :startLocation];
    if (endLocation.length) [CurrentUser saveUserInfo:FUSER_ENDLOCATION :endLocation];
    [NotificationCenter post:NOTI_QUESTIONNEXT];
}
- (IBAction)backBtn:(id)sender {
    [NotificationCenter post:NOTI_QUESTIONBACK];
}
@end
