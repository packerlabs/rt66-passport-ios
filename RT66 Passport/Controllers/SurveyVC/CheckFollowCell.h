//
//  CheckFollowCell.h
//  RT66 Passport
//
//  Created by My Star on 12/15/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckFollowCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *checkImg;
@property (strong, nonatomic) IBOutlet UILabel *locationNameL;

@end
