//
//  QuestionVC.h
//  ProjectH
//
//  Created by My Star on 10/20/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
@interface QuestionNineVC : UIViewController
@property (strong, nonatomic) IBOutlet UITextView *commentTv;
@property (strong, nonatomic) IBOutlet UIButton *doneBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleTL;

@end
