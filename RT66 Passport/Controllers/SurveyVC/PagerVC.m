//
//  BufferVC.m
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "PagerVC.h"
#import "QuestionOneVC.h"
#import "QuestionTwoVC.h"
#import "QuestionThreeVC.h"
#import "QuestionFourVC.h"
#import "QuestionFiveVC.h"
#import "QuestionSixVC.h"
#import "QuestionSevenVC.h"
#import "QuestionEightVC.h"
#import "QuestionNineVC.h"

#import "LPTitleView.h"
@interface PagerVC ()<LPViewPagerDataSource, LPViewPagerDelegate, LPTitleViewDelegate>
@property (nonatomic, strong) QuestionOneVC *questionOneVC;
@property (nonatomic, strong) QuestionTwoVC *questionTwoVC;
@property (nonatomic, strong) QuestionThreeVC *questionThreeVC;
@property (nonatomic, strong) QuestionFourVC *questionFourVC;
@property (nonatomic, strong) QuestionFiveVC *questionFiveVC;
@property (nonatomic, strong) QuestionSixVC *questionSixVC;
@property (nonatomic, strong) QuestionSevenVC *questionSevenVC;
@property (nonatomic, strong) QuestionEightVC *questionEightVC;
@property (nonatomic, strong) QuestionNineVC *questionNineVC;



@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) LPTitleView *pagingTitleView;
@end

@implementation PagerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pagerSet];
    [NotificationCenter addObserver:self selector:@selector(nextView) name:NOTI_QUESTIONNEXT];
    [NotificationCenter addObserver:self selector:@selector(backView) name:NOTI_QUESTIONBACK];
}
-(void)viewWillAppear:(BOOL)animated{
    [self setUIs];
    [self initialize];
   
}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs{
    
}
- (void) initialize
{
    GLOBALINS.selectedQuestionViewIndex = 0;
}
- (void)pagerSet
{
    self.dataSource = self;
    self.delegate = self;
    // Do not auto load data
    self.manualLoadData = YES;
    self.currentIndex = 0;
//    self.navigationItem.titleView = self.pagingTitleView;
    [self reloadData];
}


#pragma mark - LPViewPagerDataSource
- (NSUInteger)numberOfTabsForViewPager:(LPViewPagerController *)viewPager
{
    return 9;
}

- (UIViewController *)viewPager:(LPViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index
{
    if(index == 0) {
        return [self createVc1];
    }else if (index == 1) {
        return [self createVc2];
    }else if (index == 2){
        return [self createVc3];
    }else if (index == 3){
        return [self createVc4];
    }else if (index == 4){
        return [self createVc5];
    }else if (index == 5){
        return [self createVc6];
    }else if (index == 6){
        return [self createVc7];
    }else if (index == 7){
        return [self createVc8];
    }else if (index == 8){
        return [self createVc9];
    }
    else{
        return [self createVc1];
    }
}
-(void)nextView{
    [self didSelectedTitleAtIndex:self.currentIndex+1];
}
-(void)backView{
    if (_currentIndex == 0) return;
    [self didSelectedTitleAtIndex:_currentIndex-1];
}
#pragma mark 🎈LPViewPagerDelegate
- (void)viewPager:(LPViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index
{
    self.currentIndex = index;
}

- (LPTitleView *)pagingTitleView
{
    if (!_pagingTitleView) {
        self.pagingTitleView          = [[LPTitleView alloc] init];
        self.pagingTitleView.frame    = CGRectMake(0, 0, 0, 40);
        self.pagingTitleView.font     = [UIFont systemFontOfSize:15];
//        self.pagingTitleView.titleNormalColor = [UIColor greenColor];
//        self.pagingTitleView.titleSelectedColor = [UIColor yellowColor];
        self.pagingTitleView.indicatorColor = [UIColor purpleColor];
        NSArray *titleArray           = @[@"Type", @"Categories"];
        CGRect ptRect                 = self.pagingTitleView.frame;
        ptRect.size.width             = [LPTitleView calcTitleWidth:titleArray withFont:self.pagingTitleView.font];
        self.pagingTitleView.frame    = ptRect;
        [self.pagingTitleView addTitles:titleArray];
        self.pagingTitleView.delegate = self;
    }
    return _pagingTitleView;
}

- (void)didSelectedTitleAtIndex:(NSUInteger)index
{
    UIPageViewControllerNavigationDirection direction;
    if (self.currentIndex == index) {
        return;
    }
    if (index > self.currentIndex) {
        direction = UIPageViewControllerNavigationDirectionForward;
    } else {
        direction = UIPageViewControllerNavigationDirectionReverse;
    }
    UIViewController *viewController = [self viewControllerAtIndex:index];
    if (viewController) {
        __weak typeof(self) weakself = self;
        [self.pageViewController setViewControllers:@[viewController] direction:direction animated:YES completion:^(BOOL finished) {
            weakself.currentIndex = index;
        }];
    }
}

- (void)setCurrentIndex:(NSInteger)index
{
    _currentIndex = index;
    [self.pagingTitleView adjustTitleViewAtIndex:index];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat contentOffsetX = scrollView.contentOffset.x;
    CGFloat screenWidth = [[UIScreen mainScreen]bounds].size.width;
    if (self.currentIndex != 0 && contentOffsetX <= screenWidth * 2) {
        contentOffsetX += screenWidth * self.currentIndex;
    }
    [self.pagingTitleView updatePageIndicatorPosition:contentOffsetX];
}

- (void)scrollEnabled:(BOOL)enabled
{
    self.scrollingLocked = !enabled;
    for (UIScrollView *view in self.pageViewController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.scrollEnabled = enabled;
            view.bounces = enabled;
        }
    }
}

#pragma mark - lazy load

- (UIViewController *)createVc1
{
    if (!self.questionOneVC) {
        self.questionOneVC = [self.storyboard instantiateViewControllerWithIdentifier:@"QuestionOneVC"];
    }
    return self.questionOneVC;
}

- (UIViewController *)createVc2
{
    if (!self.questionTwoVC) {
        self.questionTwoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"QuestionTwoVC"];
    }
    return self.questionTwoVC;
}

- (UIViewController *)createVc3
{
    if (!self.questionThreeVC) {
        self.questionThreeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"QuestionThreeVC"];
    }
    return self.questionThreeVC;
}
- (UIViewController *)createVc4
{
    if (!self.questionFourVC) {
        self.questionFourVC = [self.storyboard instantiateViewControllerWithIdentifier:@"QuestionFourVC"];
    }
    return self.questionFourVC;
}
- (UIViewController *)createVc5
{
    if (!self.questionFiveVC) {
        self.questionFiveVC = [self.storyboard instantiateViewControllerWithIdentifier:@"QuestionFiveVC"];
    }
    return self.questionFiveVC;
}
- (UIViewController *)createVc6
{
    if (!self.questionSixVC) {
        self.questionSixVC = [self.storyboard instantiateViewControllerWithIdentifier:@"QuestionSixVC"];
    }
    return self.questionSixVC;
}
- (UIViewController *)createVc7
{
    if (!self.questionSevenVC) {
        self.questionSevenVC = [self.storyboard instantiateViewControllerWithIdentifier:@"QuestionSevenVC"];
    }
    return self.questionSevenVC;
}
- (UIViewController *)createVc8
{
    if (!self.questionEightVC) {
        self.questionEightVC = [self.storyboard instantiateViewControllerWithIdentifier:@"QuestionEightVC"];
    }
    return self.questionEightVC;
}
- (UIViewController *)createVc9
{
    if (!self.questionNineVC) {
        self.questionNineVC = [self.storyboard instantiateViewControllerWithIdentifier:@"QuestionNineVC"];
    }
    return self.questionNineVC;
}
#pragma mark - BUTTON ACTIONS

@end
