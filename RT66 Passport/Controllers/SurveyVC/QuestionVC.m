//
//  QuestionVC.m
//  ProjectH
//
//  Created by My Star on 10/20/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "QuestionVC.h"
#import "CountriesView.h"
@interface QuestionVC ()<CountriesDelegate>

@end

@implementation QuestionVC
static NSString * extracted() {
    return NOTI_CHANGEPROGRESS;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [NotificationCenter addObserver:self selector:@selector(progresValue:) name:extracted()];
    [NotificationCenter addObserver:self selector:@selector(showPickerWithArray:) name:NOTI_SHOWPICKER];
    [NotificationCenter addObserver:self selector:@selector(hideReadyLabel) name:NOTI_HIDESURVEYTITLE];
    [NotificationCenter addObserver:self selector:@selector(actionBack) name:NOTI_QUESTIONDONE];
    [NotificationCenter addObserver:self selector:@selector(goCountryView) name:NOTI_GOCOUNTRYVIEW];
}

-(void)viewWillAppear:(BOOL)animated{
    [self setUIs];
    [self initialize];
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs
{
    self.navigationItem.title = SurveyT[USERINS.userLanguage];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btnBackMain"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(actionBack)];
    self.circleV.roundedCorners = YES;
    self.circleV.progress = 0.0;
    self.circleV.thicknessRatio = 0.2;
    self.circleV.progressTintColor = HEXCOLOR(0xF66420FF);  
    
    self.userImg.layer.cornerRadius = self.userImg.bounds.size.width/2;
    [[self.userImg layer] setBorderWidth: 2.0f];
    self.userImg.layer.borderColor = HEXCOLOR(0x2B6A9DFF).CGColor;
    self.userImg.layer.masksToBounds = YES;
    
    [self.userImg sd_setImageWithURL:[NSURL URLWithString:USERINS.photoUrl] placeholderImage:[UIImage imageNamed:@"imgPlaceholder"]];
    [self setTranslations];
}
- (void) setTranslations{
    self.readyL.text = About_Your_TripT[USERINS.userLanguage];
}
- (void) initialize
{
}
#pragma mark - BUTTON ACTIONS
- (void)actionBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void) progresValue:(id)obj
{
    float value = [[obj object] floatValue];
    [self.circleV setProgress:value animated:YES];
}
#pragma mark - Notificationcenter functions
-(void)goCountryView{
    CountriesView *countriesView = [[CountriesView alloc] init];
    countriesView.delegate = self;
    NavigationController *navController = [[NavigationController alloc] initWithRootViewController:countriesView];
    [self presentViewController:navController animated:YES completion:nil];
}
-(void)hideReadyLabel{
    self.readyL.hidden = YES;
}

#pragma mark - Pickers
-(void)showPickerWithArray:(id)obj{
    BOOL isStartLocation = [[obj object] boolValue];
    NSString *title;
    if (isStartLocation) {
        title = SelectStartT[USERINS.userLanguage];
    }else{
        title = SelectEndT[USERINS.userLanguage];
    }
    [ActionSheetStringPicker showPickerWithTitle:title
                                            rows:GLOBALINS.locationNames
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           if (isStartLocation) {
                                               [NotificationCenter post:NOTI_SELECTSTARTLOCATION object:selectedValue];
                                           }else{
                                               [NotificationCenter post:NOTI_SELECTENDLOCATION object:selectedValue];
                                           }
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:self.view];

}


#pragma mark - CountriesDelegate
- (void)didSelectCountry:(NSString *)country CountryCode:(NSString *)countryCode
{
    NSString *countrycode = [countryCode lowercaseString];
    FUser *user = FUser.currentUser;
    user[FUSER_COUNTRYNAME] = country;
    user[FUSER_COUNTRYCODE] = countrycode;
    [user saveInBackground:^(NSError * _Nullable error) {
        if (error == nil) {
            [self setUIs];
        }else{
            [ProgressHUD showError:error.description];
        }
        
    }];
}
@end
