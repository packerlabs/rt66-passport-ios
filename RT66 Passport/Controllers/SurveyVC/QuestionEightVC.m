//
//  QuestionVC.m
//  ProjectH
//
//  Created by My Star on 10/20/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "QuestionEightVC.h"

@interface QuestionEightVC ()<UITextFieldDelegate>

@end

@implementation QuestionEightVC
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUIs];    
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}
-(void)viewWillAppear:(BOOL)animated{
    [self initialize];
}

-(void)setUIs
{
    self.destinationF.delegate = self;
    if (USERINS.destination.length) {
        self.destinationF.text = USERINS.destination;
        self.yesBtn.selected = NO;
        self.noBtn.selected = NO;
    }else{
        if (USERINS.isdestination == YES) {
            self.yesBtn.selected = YES;
            self.noBtn.selected = NO;
        }else{
            self.yesBtn.selected = NO;
            self.noBtn.selected = YES;
        }
    }
    [self setTranslations];
}
- (void) setTranslations{
    self.titleTL.text = DestinationT[USERINS.userLanguage];
    self.ifnotTL.text = IfnotWhatT[USERINS.userLanguage];
    self.noTL.text = NoT[USERINS.userLanguage];
    self.yesTL.text = YesT[USERINS.userLanguage];
}
- (void) initialize
{
    [NotificationCenter post:NOTI_CHANGEPROGRESS object:@(7/9.0)];
}
#pragma mark - BUTTON ACTIONS
- (IBAction)noBtn:(UIButton *)sender {
    self.yesBtn.selected = NO;
    sender.selected = !sender.isSelected;
    self.destinationF.text = @"";
    [CurrentUser saveUserInfo:FUSER_ISDESTINATION :@NO];
    [NotificationCenter post:NOTI_QUESTIONNEXT];
}
- (IBAction)yesBtn:(UIButton *)sender {
    self.noBtn.selected = NO;
    sender.selected = !sender.isSelected;
    self.destinationF.text = @"";
    [CurrentUser saveUserInfo:FUSER_ISDESTINATION :@YES];
    [NotificationCenter post:NOTI_QUESTIONNEXT];
}
- (IBAction)nextBtn:(id)sender {
    NSString *destination = self.destinationF.text;
    if (destination.length) {
        [CurrentUser saveUserInfo:FUSER_DESTINATION :destination];
    }
    [NotificationCenter post:NOTI_QUESTIONNEXT];
}
- (IBAction)backBtn:(id)sender {
    [NotificationCenter post:NOTI_QUESTIONBACK];
}
#pragma mark - UITextfield delegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.text.length) {
        self.yesBtn.selected = NO;
        self.noBtn.selected = NO;
    }
}
@end
