//
//  QuestionVC.m
//  ProjectH
//
//  Created by My Star on 10/20/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "QuestionNineVC.h"

@interface QuestionNineVC ()<MFMailComposeViewControllerDelegate>

@end

@implementation QuestionNineVC
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUIs];
}
-(void)viewWillAppear:(BOOL)animated{
    [self initialize];
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs
{
    self.doneBtn.layer.cornerRadius = self.doneBtn.bounds.size.width/2;
    [[self.doneBtn layer] setBorderWidth: 2.0f];
    self.doneBtn.layer.borderColor = HEXCOLOR(0x2B6A9DFF).CGColor;
    self.doneBtn.layer.masksToBounds = YES;
    
    [Global roundBorderSet:self.commentTv borderColor:[UIColor whiteColor]];
    if (!USERINS.commentofvisiting.length) return;
    self.commentTv.text = USERINS.commentofvisiting;
    [self setTranslations];
}
- (void) setTranslations{
    self.titleTL.text = CommentT[USERINS.userLanguage];
    [self.doneBtn setTitle:DoneT[USERINS.userLanguage] forState:UIControlStateNormal];
}
- (void) initialize
{
    [NotificationCenter post:NOTI_CHANGEPROGRESS object:@(8/9.0)];
}
-(void)sendEmail:(NSString *)emailBody{
    if (![MFMailComposeViewController canSendMail])
    {
        [ProgressHUD showError:InvalidEmailT[USERINS.userLanguage]];
        return;
    }
    MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
    mailer.mailComposeDelegate = self;
    [mailer setSubject:@"Comments on my Route 66 visit"];
    
    NSArray *toRecipients = [NSArray arrayWithObjects:OWNEREMAIL, nil];
    [mailer setToRecipients:toRecipients];
    [mailer setMessageBody:emailBody isHTML:NO];
    [self presentViewController:mailer animated:YES completion:nil];
}
#pragma mark - BUTTON ACTIONS
- (IBAction)doneBtn:(id)sender {
    NSString *comment = self.commentTv.text;
    if (comment.length) {
        [CurrentUser saveUserInfo:FUSER_COMMENTVISIT :comment];
        [self sendEmail:comment];
    }    
    [NotificationCenter post:NOTI_CHANGEPROGRESS object:@(1.0)];
    [self performSelector:@selector(delayDone) withObject:nil afterDelay:0.5];
}
- (IBAction)backBtn:(id)sender {
    [NotificationCenter post:NOTI_QUESTIONBACK];
}
-(void)delayDone{
    [NotificationCenter post:NOTI_QUESTIONDONE];
}

#pragma mark - MFMailComposeViewController DelegateMethod
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultSent:{

            break;
        }
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultFailed:
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
