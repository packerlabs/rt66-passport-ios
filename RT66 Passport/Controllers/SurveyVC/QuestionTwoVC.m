//
//  BufferVC.m
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "QuestionTwoVC.h"

@interface QuestionTwoVC ()

@end

@implementation QuestionTwoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [NotificationCenter post:NOTI_HIDESURVEYTITLE];    
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}
-(void)viewWillAppear:(BOOL)animated{
    [self setUIs];
    [self initialize];
}

-(void)setUIs{
    if (USERINS.adultnum>0) self.adultF.text = [@(USERINS.adultnum) stringValue];
    if (USERINS.minornum>0) self.minorF.text = [@(USERINS.minornum) stringValue];
    [self setTranslations];
}
- (void) setTranslations{
    self.titleTL.text = TravelingPartyT[USERINS.userLanguage];
    self.adultF.placeholder = NumberAdultsT[USERINS.userLanguage];
    self.minorF.placeholder = NumberMinorsT[USERINS.userLanguage];
}
- (void) initialize
{
    [NotificationCenter post:NOTI_CHANGEPROGRESS object:@(1/9.0)];
}
#pragma mark - BUTTON ACTIONS
- (IBAction)nextBtn:(id)sender {
    NSString *adult = self.adultF.text;
    NSString *minor = self.minorF.text;
    if (adult.length) [CurrentUser saveUserInfo:FUSER_NUM_ADULT :@([adult intValue])];
    if (minor.length) [CurrentUser saveUserInfo:FUSER_NUM_MINOR :@([minor intValue])];
    [NotificationCenter post:NOTI_QUESTIONNEXT];
}
- (IBAction)backBtn:(id)sender {
    [NotificationCenter post:NOTI_QUESTIONBACK];
}
@end
