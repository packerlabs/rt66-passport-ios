//
//  BufferVC.h
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//startDrop

#import <UIKit/UIKit.h>
#import "utilities.h"

@interface QuestionThreeVC : UIViewController
@property (strong, nonatomic) IBOutlet ACFloatingTextField *startLocationF;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *endLocationF;
@property (weak, nonatomic) IBOutlet UILabel *titleTL;
@end
