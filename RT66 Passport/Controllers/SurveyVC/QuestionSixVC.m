//
//  BufferVC.m
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "QuestionSixVC.h"

@interface QuestionSixVC ()

@end

@implementation QuestionSixVC

- (void)viewDidLoad {[super viewDidLoad];}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}
-(void)viewWillAppear:(BOOL)animated{
    [self setUIs];
    [self initialize];
}

-(void)setUIs{
    if (USERINS.traveldays>0) self.traveldayF.text = [@(USERINS.traveldays) stringValue];
    [self setTranslations];
}
- (void) setTranslations{
    self.titleTL.text = DaysTravelT[USERINS.userLanguage];
}
- (void) initialize
{
    [NotificationCenter post:NOTI_CHANGEPROGRESS object:@(5/9.0)];
}

#pragma mark - BUTTON ACTIONS
- (IBAction)nextBtn:(id)sender {
    NSString *traveldays = self.traveldayF.text;
    if (traveldays.length) [CurrentUser saveUserInfo:FUSER_TRAVELDAY :@([traveldays intValue])];
    [NotificationCenter post:NOTI_QUESTIONNEXT];
}
- (IBAction)backBtn:(id)sender {
    [NotificationCenter post:NOTI_QUESTIONBACK];
}

@end
