//
//  QuestionVC.h
//  ProjectH
//
//  Created by My Star on 10/20/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
#import "DACircularProgressView.h"
#import "LPViewPagerController.h"
@interface QuestionVC : UIViewController
@property (weak, nonatomic) IBOutlet DACircularProgressView *circleV;
@property (weak, nonatomic) IBOutlet UIImageView *userImg;
@property (strong, nonatomic) IBOutlet UILabel *readyL;

@end
