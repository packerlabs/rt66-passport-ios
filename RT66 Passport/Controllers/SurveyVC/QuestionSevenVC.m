//
//  BufferVC.m
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "QuestionSevenVC.h"

@interface QuestionSevenVC ()

@end

@implementation QuestionSevenVC

- (void)viewDidLoad {[super viewDidLoad];}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}
-(void)viewWillAppear:(BOOL)animated{
    [self setUIs];
    [self initialize];
}



-(void)setUIs{
    if (USERINS.numofarizona>0) self.numarizonaF.text = [@(USERINS.numofarizona) stringValue];
    [self setTranslations];
}
- (void) setTranslations{
    self.titleTL.text = HowArizonaT[USERINS.userLanguage];
}
- (void) initialize
{
    [NotificationCenter post:NOTI_CHANGEPROGRESS object:@(6/9.0)];
}

#pragma mark - BUTTON ACTIONS
- (IBAction)nextBtn:(id)sender {
    NSString *numofarizona = self.numarizonaF.text;
    if (numofarizona.length) [CurrentUser saveUserInfo:FUSER_NUMOFARIZONA :@([numofarizona intValue])];
    [NotificationCenter post:NOTI_QUESTIONNEXT];
}
- (IBAction)backBtn:(id)sender {
    [NotificationCenter post:NOTI_QUESTIONBACK];
}

@end
