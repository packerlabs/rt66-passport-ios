//
//  BufferVC.m
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "QuestionOneVC.h"

@interface QuestionOneVC ()<UITextFieldDelegate>

@end

@implementation QuestionOneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}
-(void)viewWillAppear:(BOOL)animated{    
    [self initialize];
    [self setUIs];
}
-(void)setUIs{
    self.countryF.delegate = self;
    if ([USERINS.countrycode isEqualToString:@"US"]) {
        if (USERINS.city.length) self.cityF.text = USERINS.city;
        if (USERINS.state.length) self.stateF.text = USERINS.state;
        self.countryF.text = @"";
    }else{
        self.cityF.text = @"";
        self.stateF.text = @"";
        if (USERINS.countryname.length) self.countryF.text = USERINS.countryname;
    }
    [self setTranslations];
}
- (void) setTranslations{
    self.ifoutsideTL.text = OutsideUST[USERINS.userLanguage];
    self.cityF.placeholder = Your_CityT[USERINS.userLanguage];
    self.stateF.placeholder = StateT[USERINS.userLanguage];
    self.countryF.placeholder = CountryT[USERINS.userLanguage];
}
- (void) initialize
{
    [NotificationCenter post:NOTI_CHANGEPROGRESS object:@(0.0)];
}

#pragma mark - BUTTON ACTIONS
- (IBAction)nextBtn:(id)sender {
    NSString *city = self.cityF.text;
    NSString *state = self.stateF.text;
    if (city.length) [CurrentUser saveUserInfo:FUSER_CITY :city];
    if (state.length) [CurrentUser saveUserInfo:FUSER_STATE :state];
    [NotificationCenter post:NOTI_QUESTIONNEXT];
}


#pragma mark - TextField delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == self.countryF) {
        [textField resignFirstResponder];
        [NotificationCenter post:NOTI_GOCOUNTRYVIEW];
    }
}
@end
