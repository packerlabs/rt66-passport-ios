//
//  BufferVC.m
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "QuestionFiveVC.h"
#import "CheckFollowCell.h"
@interface QuestionFiveVC ()<UITableViewDataSource,UITableViewDelegate>{
    UIImage *checkedImg;
}

@end

@implementation QuestionFiveVC

- (void)viewDidLoad {[super viewDidLoad];}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}
-(void)viewWillAppear:(BOOL)animated{
    [self setUIs];
    [self initialize];
}


//SpendNightCell
-(void)setUIs{
    [Global roundBorderSet:self.tableview borderColor:[UIColor whiteColor]];    
    [self setTranslations];
}
- (void) setTranslations{
    self.titleTL.text = SpendNightT[USERINS.userLanguage];   
}
- (void) initialize
{
    [NotificationCenter post:NOTI_CHANGEPROGRESS object:@(4/9.0)];
    checkedImg = [UIImage imageNamed:@"checkmark"];
}
#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return GLOBALINS.communityNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CheckFollowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SpendnightCell" forIndexPath:indexPath];
    DBCommunity *dbCommunity = DBCommunity.allObjects[indexPath.row];
    if (!USERINS.spendnights){
        USERINS.spendnights = [NSMutableArray new];
    }
    NSLog(@"%@",USERINS.spendnights);
    NSLog(@"%@",dbCommunity.objectId);
    if ([USERINS.spendnights containsObject:dbCommunity.objectId]) {
        cell.checkImg.image = checkedImg;
    }else{
        cell.checkImg.image = nil;
    }
    cell.locationNameL.text = dbCommunity.name;
    return cell;
}
#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DBCommunity *dbCommunity = DBCommunity.allObjects[indexPath.row];
    
    NSMutableArray *spendnights = USERINS.spendnights;
    if(!spendnights) spendnights = [NSMutableArray new];
    if ([spendnights containsObject:dbCommunity.objectId]) {
        [spendnights removeObject:dbCommunity.objectId];
    }else{
        [spendnights addObject:dbCommunity.objectId];
    }
    
    FUser *user = FUser.currentUser;
    user[FUSER_SPENDNIGHTS] = spendnights;
    [user saveInBackground:^(NSError * _Nullable error) {
        if (error == nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableview reloadData];
            });
        }
    }];
}
#pragma mark - BUTTON ACTIONS
- (IBAction)nextBtn:(id)sender {    
    [NotificationCenter post:NOTI_QUESTIONNEXT];
}
- (IBAction)backBtn:(id)sender {
    [NotificationCenter post:NOTI_QUESTIONBACK];
}


@end
