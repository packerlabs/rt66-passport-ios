//
//  BufferVC.h
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
@interface QuestionTwoVC : UIViewController
@property (weak, nonatomic) IBOutlet ACFloatingTextField *adultF;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *minorF;
@property (weak, nonatomic) IBOutlet UILabel *titleTL;
@end
