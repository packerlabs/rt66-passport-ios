//
//  UserListCell.h
//  ITAGG
//
//  Created by My Star on 5/2/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
@interface PassPortCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userImg;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIImageView *wasemailedImg;
@property (weak, nonatomic) IBOutlet UILabel *locationNameL;
@property (weak, nonatomic) IBOutlet UILabel *stateL;

@end
