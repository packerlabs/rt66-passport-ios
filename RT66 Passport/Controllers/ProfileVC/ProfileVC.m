//
//  ProfileVC.m
//  RT66 Passport
//
//  Created by My Star on 8/4/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "ProfileVC.h"

@interface ProfileVC ()

@end

@implementation ProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setFontFamily:@"Maiden Orange" forView:self.view andSubViews:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    [self setUIs];
}
-(void)setUIs
{
    GLOBALINS.mainVC.navigationItem.title = ProfileT[USERINS.userLanguage];;
    [_userImg setNeedsLayout];
    [_userImg layoutIfNeeded];
    [self.flagImg setNeedsLayout];
    [self.flagImg layoutIfNeeded];
    
    self.userImg.layer.cornerRadius = self.userImg.bounds.size.width/2;
    [[self.userImg layer] setBorderWidth: 2.0f];
    self.userImg.layer.borderColor = HEXCOLOR(0x2B6A9DFF).CGColor;
    self.userImg.layer.masksToBounds = YES;
    
    self.flagImg.layer.cornerRadius = self.flagImg.bounds.size.width/2;
    self.flagImg.layer.masksToBounds = YES;
    self.flagImg.layer.borderColor = HEXCOLOR(0x2B6A9DFF).CGColor;
    [[self.flagImg layer] setBorderWidth: 1.0f];
    
    [self.userImg sd_setImageWithURL:[NSURL URLWithString:USERINS.photoUrl] placeholderImage:[UIImage imageNamed:@"imgPlaceholder"]];
    
    NSString *flagUrl = [NSString stringWithFormat:@"%@%@.png",API_COUNTRYFLAG,USERINS.countrycode];
    [self.flagImg sd_setImageWithURL:[NSURL URLWithString:flagUrl] placeholderImage:[UIImage imageNamed:@"planet"]];    
    
    NSString *userName;
    if (USERINS.fullname.length>1) {
        userName = USERINS.fullname;
    }else{
        userName = USERINS.email;
    }    
   
    self.userNameL.text = userName;

    [self setTranslations];
}
- (void) setTranslations{
    [self.editprofileTL setTitle:EditProfileT[USERINS.userLanguage] forState:UIControlStateNormal];
    self.welcomeTL.text = WelcomeT[USERINS.userLanguage];
}
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}
#pragma mark - BUTTON ACTION
- (IBAction)editProfileBtn:(id)sender {
    EditProfileVC *editProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileVC"];
    [GLOBALINS.mainVC.navigationController pushViewController:editProfileVC animated:YES];
}
- (IBAction)viewPhotoBtn:(id)sender {
}
- (IBAction)requestPassportBtn:(id)sender {
    RequestPassportVC *requestPassportVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RequestPassportVC"];
    [GLOBALINS.mainVC.navigationController pushViewController:requestPassportVC animated:YES];
}




- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

@end
