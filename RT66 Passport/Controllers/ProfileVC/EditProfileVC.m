//
//  EditProfileVC.m
//  RT66 Passport
//
//  Created by My Star on 8/24/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "EditProfileVC.h"
#import "CountriesView.h"
#import "QuestionVC.h"
@interface EditProfileVC ()<UIImagePickerControllerDelegate, UITextFieldDelegate,CountriesDelegate>{
    FUser *user;
}

@end

@implementation EditProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self.nameF becomeFirstResponder];
    [self setFontFamily:@"Maiden Orange" forView:self.view andSubViews:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    user = [FUser currentUser];
    [self setUIs];
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}


-(void)setUIs{
    self.navigationItem.title = EditProfileT[USERINS.userLanguage];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btnBackMain"]
                                                                             style:UIBarButtonItemStylePlain target:self action:@selector(actionBack)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:SaveT[USERINS.userLanguage]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(save)];
    
    self.nameF.delegate = self;
    
    // Set User Photo & nationality
    [_userImg setNeedsLayout];
    [_userImg layoutIfNeeded];
    [self.flagImg setNeedsLayout];
    [self.flagImg layoutIfNeeded];
    
    self.userImg.layer.cornerRadius = self.userImg.bounds.size.width/2;
    [[self.userImg layer] setBorderWidth: 2.0f];
    self.userImg.layer.borderColor = HEXCOLOR(0x2B6A9DFF).CGColor;
    self.userImg.layer.masksToBounds = YES;
    
    self.flagImg.layer.cornerRadius = self.flagImg.bounds.size.width/2;
    self.flagImg.layer.masksToBounds = YES;
    self.flagImg.layer.borderColor = HEXCOLOR(0x2B6A9DFF).CGColor;
    [[self.flagImg layer] setBorderWidth: 1.0f];
    [self.userImg sd_setImageWithURL:[NSURL URLWithString:USERINS.photoUrl] placeholderImage:[UIImage imageNamed:@"imgPlaceholder"]];
    NSString *flagUrl = [NSString stringWithFormat:@"%@%@.png",API_COUNTRYFLAG,USERINS.countrycode];
    [self.flagImg sd_setImageWithURL:[NSURL URLWithString:flagUrl] placeholderImage:[UIImage imageNamed:@"planet"]];
    
    // Set User Name
    if (USERINS.fullname.length>1) {
        self.nameF.text = USERINS.fullname;
    }
    // Set Notification
    if (USERINS.notificationallow) {
        self.notificationSw.on = YES;
    }else{
        self.notificationSw.on = NO;
    }
    // Set User Language
    self.languageL.text = GLOBALINS.originLanguages[USERINS.userLanguage];
    [self setTranslations];
}
- (void) setTranslations{
    [self.surveyBtn setTitle:AnswerSurveyT[USERINS.userLanguage] forState:UIControlStateNormal];
    self.welcomeTL.text = WelcomeT[USERINS.userLanguage];
    self.languageTL.text = LanguageT[USERINS.userLanguage];
    self.notificationTL.text = NotificationT[USERINS.userLanguage];
}
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}
#pragma mark - BUTTON ACTION
- (void)save{
    NSString *name = self.nameF.text;
    if (name.length<2) {
        [Global confirmMessage :self   :@"Error" :InvalidEmailT[USERINS.userLanguage]];
        return;
    }
    user[FUSER_FULLNAME] = name;
    user[FUSER_NOTIFICATIONALLOW] = @(self.notificationSw.isOn);
    [FIRAnalytics setUserPropertyString:ANALY_USER_NATIONALITY forName:user[FUSER_COUNTRYNAME]];
    [ProgressHUD show:nil Interaction:NO];
    [user saveInBackground:^(NSError * _Nullable error) {
        if (error) {
            [ProgressHUD showError:[error description]];
        }else{
            [ProgressHUD dismiss];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
    
}
- (IBAction)selectLangBtn:(id)sender {
    LanguageVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LanguageVC"];
    vc.contentSizeInPopup = CGSizeMake(250, 320);
    vc.landscapeContentSizeInPopup = CGSizeMake(250, 320);
    [Global popupVCwithFrom:self to:vc];
}
- (void)actionBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)openPhotoChangeV:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:TakephotoT[USERINS.userLanguage] style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) { PresentPhotoCamera(self, YES); }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:ChooseGalleryT[USERINS.userLanguage] style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) { PresentPhotoLibrary(self, YES); }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:CancelT[USERINS.userLanguage] style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:action1]; [alert addAction:action2]; [alert addAction:action3];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)goCountryVC:(id)sender {
        CountriesView *countriesView = [[CountriesView alloc] init];
        countriesView.delegate = self;
        NavigationController *navController = [[NavigationController alloc] initWithRootViewController:countriesView];
        [self presentViewController:navController animated:YES completion:nil];
}
- (IBAction)goSurveyVC:(id)sender {
    QuestionVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"QuestionVC"];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - CountriesDelegate
- (void)didSelectCountry:(NSString *)country CountryCode:(NSString *)countryCode
{
    NSString *countrycode = [countryCode lowercaseString];
    user[FUSER_COUNTRYNAME] = country;
    user[FUSER_COUNTRYCODE] = countrycode;
    [user saveInBackground:^(NSError * _Nullable error) {
        if (error == nil) {
            [self setUIs];
        }else{
            [ProgressHUD showError:error.description];
        }
        
    }];
}


#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    //-----------------------------------------------------------------------------------
    UIImage *imagePicture = [Image square:image size:140];
    //-----------------------------------------------------------------------------------
    NSData *dataPicture = UIImageJPEGRepresentation(imagePicture, 0.6);
    //-----------------------------------------------------------------------------------
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
    long long timestamp = [[NSDate date] timestamp];
    FIRStorage *storage = [FIRStorage storage];
    FIRStorageReference *reference = [[storage referenceForURL:FIREBASE_STORAGE] child:Filename(PROFILEPHOTO_STORAGE,timestamp, @"jpg")];
    FIRStorageUploadTask *task = [[FIRStorageUploadTask alloc] init];
    task = [reference putData:dataPicture metadata:nil completion:^(FIRStorageMetadata *metadata, NSError *error)
                                  {
                                      [hud hideAnimated:YES];
                                      [task removeAllObservers];
                                      if (error == nil)
                                      {
                                          NSString *linkPicture = metadata.downloadURL.absoluteString;
                                          [self showUserPicture:linkPicture];
                                      }
                                      else [ProgressHUD showError:ERROR_NETWORK];
                                  }];
    
    [task observeStatus:FIRStorageTaskStatusProgress handler:^(FIRStorageTaskSnapshot *snapshot)
     {
         hud.progress = (float) snapshot.progress.completedUnitCount / (float) snapshot.progress.totalUnitCount;
     }];
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)showUserPicture:(NSString *)url{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.userImg sd_setImageWithURL:[NSURL URLWithString:url]];
    });
    [CurrentUser saveUserInfo:FUSER_PICTURE :url];
}
#pragma mark TEXTFIELD DELEGETE.

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}
@end
