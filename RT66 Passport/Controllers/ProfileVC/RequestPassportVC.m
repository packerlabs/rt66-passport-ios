//
//  BufferVC.m
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "RequestPassportVC.h"
#import "PassPortCell.h"
#import <MessageUI/MessageUI.h>

@interface RequestPassportVC ()<UIImagePickerControllerDelegate,UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate,ELCImagePickerControllerDelegate>{
    NSMutableArray *iDMPhotos;
    BOOL fromBrowserB;
    int emailedCount;
}
@end

@implementation RequestPassportVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUIs];
    [self initialize];
    [self setFontFamily:@"Maiden Orange" forView:self.view andSubViews:YES];
}
-(void)viewWillAppear:(BOOL)animated{

}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs{
    self.navigationItem.title = @"Request Passport";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btnBackMain"]
                                                                             style:UIBarButtonItemStylePlain target:self action:@selector(actionBack)];
//    if ([USERINS.wasEmailed isEqualToString:@"YES"]) {
//        self.navigationItem.rightBarButtonItem = nil;
////        [self.requestBtn setBackgroundColor:[UIColor lightGrayColor]];
//    }else{
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"add"]
                                                                                  style:UIBarButtonItemStylePlain
                                                                                 target:self
                                                                                 action:@selector(addNewPassport)];
//    }

}
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}
#pragma mark - BUTTON ACTIONS
- (void)actionBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)addNewPassport{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Open camera" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) { fromBrowserB = true;  PresentPhotoCamera(self, YES); }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Photo library" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) { [self launchMultiSelectPhotoController]; }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:action1]; [alert addAction:action2]; [alert addAction:action3];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void) initialize
{    
    fromBrowserB = false;
    iDMPhotos = [NSMutableArray new];
    emailedCount = [self getWasEmailedRequsestPassportCount];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_main_queue(),^{
        NSError *error;
        for (int i = 0; i<USERINS.myrequestpassports.count; i++) {
            RequModel *requModel = [[RequModel alloc] initWithDictionary:USERINS.myrequestpassports[i] error:&error];
            [self saveIDMPhotosWithUrl:requModel.url];
        }
        [self.tableV reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}

-(void)deletePassport:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    NSInteger index = btn.tag;
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Do you want to remove this passport?"
                                                                  message:nil
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Yes"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    NSError *error;
                                    NSMutableArray *myrequestpassports = USERINS.myrequestpassports;
                                    RequModel *requModel = [[RequModel alloc] initWithDictionary:myrequestpassports[index] error:&error];
                                    [self deletImageFromFireStorage:index];
                                    [self deleteIDMPhotosWithUrl:requModel.url];
                                    [myrequestpassports removeObjectAtIndex:index];
                                    FUser *user = FUser.currentUser;
                                    user[FUSER_MYREQUESTPASSPORTS] = myrequestpassports;
                                    [ProgressHUD show:nil Interaction:NO];
                                    [user saveInBackground:^(NSError * _Nullable error) {
                                        if (error) {
                                            [ProgressHUD showError:error.description];
                                        }else{
                                            [ProgressHUD dismiss];
                                            [self refreshCollectionV];
                                        }
                                    }];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"Cancel"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)requestBtn:(id)sender {
    int newCount = (int)iDMPhotos.count - emailedCount;
    if (!newCount) {
        [self.view makeToast:@"Sorry, you don't have new passport(s)."];
        return;
    }
    if (![MFMailComposeViewController canSendMail])
    {
        [ProgressHUD showError:@"Please set your email account!"];
        return;
    }
        
    [ProgressHUD show:nil Interaction:NO];
    NSMutableArray *photoDatas = [NSMutableArray new];
    NSError *error;
    for (int i = 0; i<USERINS.myrequestpassports.count; i++) {
        RequModel *requModel = [[RequModel alloc] initWithDictionary:USERINS.myrequestpassports[i] error:&error];
        if (!requModel.wasEmailed){
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:requModel.url]];
            [photoDatas addObject:data];
        }
    }
    [ProgressHUD dismiss];
    if (!photoDatas.count) {
        [self.view makeToast:@"Sorry, you have already emailed with these passports."];
        return;
    }

    MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
    mailer.mailComposeDelegate = self;
    [mailer setSubject:@"Request Passport!"];

    NSArray *toRecipients = [NSArray arrayWithObjects:OWNEREMAIL, nil];
    [mailer setToRecipients:toRecipients];
    for (int i = 0; i<photoDatas.count; i++) {
        [mailer addAttachmentData: photoDatas[i] mimeType:@"image/jpg" fileName:[NSString stringWithFormat:@"Passport%d",i]];
    }

    //        [mailer setMessageBody:[self getemailBody] isHTML:NO];
    [self presentViewController:mailer animated:YES completion:nil];
    return;
   
}

#pragma mark - TABLEVIEW DELEGATE

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return USERINS.myrequestpassports.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PassPortCell *cell = (PassPortCell *)[tableView dequeueReusableCellWithIdentifier:@"PassPortCell" forIndexPath:indexPath];
    NSError *error;
    RequModel *requModel = [[RequModel alloc] initWithDictionary:USERINS.myrequestpassports[indexPath.row] error:&error];
    [cell.userImg sd_setImageWithURL:[NSURL URLWithString:requModel.url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    if (requModel.wasEmailed) {
        [cell.deleteBtn setHidden:YES];
        [cell.wasemailedImg setHidden:NO];
    }else{
        [cell.deleteBtn setHidden:NO];
        [cell.wasemailedImg setHidden:YES];
    }
    cell.deleteBtn.tag = indexPath.row;
    [cell.deleteBtn addTarget:self action:@selector(deletePassport:) forControlEvents: UIControlEventTouchUpInside];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PassPortCell *cell = (PassPortCell *)[tableView dequeueReusableCellWithIdentifier:@"PassPortCell" forIndexPath:indexPath];
    
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:iDMPhotos animatedFromView:cell.userImg];
    browser.scaleImage = cell.userImg.image;
    //    browser.delegate = self;
    browser.displayCounterLabel = YES;
    browser.displayActionButton = NO;
    [self.navigationController presentViewController:browser animated:YES completion:nil];
}

#pragma mark - Custom PickerController
- (void)launchMultiSelectPhotoController
{
    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
    
    elcPicker.maximumImagesCount = 100; //Set the maximum number of images to select to 100
    elcPicker.returnsOriginalImage = YES; //Only return the fullScreenImage, not the fullResolutionImage
    elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
    elcPicker.onOrder = YES; //For multiple image selection, display and return order of selected images
    elcPicker.mediaTypes = @[(NSString *)kUTTypeImage, (NSString *)kUTTypeMovie]; //Supports image and movie types
    
    elcPicker.imagePickerDelegate = self;
    
    [self presentViewController:elcPicker animated:YES completion:nil];
}
#pragma mark ELCImagePickerControllerDelegate Methods

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];

    NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
    for (NSDictionary *dict in info) {
        if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                [images addObject:image];

            } else {
                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
            }
        } else if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypeVideo){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                [images addObject:image];
            } else {
                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
            }
        } else {
            NSLog(@"Uknown asset type");
        }
    }
    if (images.count) {
        [self uploadImages:images];
    }
    
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    if (fromBrowserB) {
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        fromBrowserB = false;
    }
    //-----------------------------------------------------------------------------------
//    UIImage *imagePicture = [Image square:image size:140];
    NSData *dataPicture = UIImageJPEGRepresentation(image, 0.6);

    //-----------------------------------------------------------------------------------
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
    FIRStorage *storage = [FIRStorage storage];
    long long timestamp = [[NSDate date] timestamp];
    FIRStorageReference *reference = [[storage referenceForURL:FIREBASE_STORAGE] child:Filename(PASSPORT_STORAGE,timestamp, @"jpg")];
    FIRStorageUploadTask *task = [FIRStorageUploadTask new];
    task = [reference putData:dataPicture metadata:nil completion:^(FIRStorageMetadata *metadata, NSError *error)
          {
              [hud hideAnimated:YES];
              [task removeAllObservers];
              if (error == nil)
              {
                  NSString *linkPicture = metadata.downloadURL.absoluteString;
                  RequModel *requModel = [RequModel new];
                  NSString *fileName = [@(timestamp) stringValue];
                  requModel.fileName = [NSString stringWithFormat:@"%@.jpg",fileName];
                  requModel.url = linkPicture;
                  requModel.wasEmailed = NO;
                  [self saveIDMPhotosWithUrl:linkPicture];
                  [USERINS.myrequestpassports addObject:[requModel toDictionary]];
                  FUser *user = [FUser currentUser];
                  user[FUSER_MYREQUESTPASSPORTS] = USERINS.myrequestpassports;
                  [user saveInBackground:^(NSError * _Nullable error) {
                      [self refreshCollectionV];
                  }];
              }
              else [ProgressHUD showError:error.description];
          }];
    
    [task observeStatus:FIRStorageTaskStatusProgress handler:^(FIRStorageTaskSnapshot *snapshot)
     {
         hud.progress = (float) snapshot.progress.completedUnitCount / (float) snapshot.progress.totalUnitCount;
     }];
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)refreshCollectionV{
    dispatch_async(dispatch_get_main_queue(), ^{ [self.tableV reloadData];});
    
}

- (void) uploadImages:(NSArray*)infos
{
    UIImage *image = (UIImage *)infos[0];
//    UIImage *imagePicture = [Image square:image size:140];
    NSData *dataPicture = UIImageJPEGRepresentation(image, 0.6);
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
    
    long long timestamp = [[NSDate date] timestamp];
    NSInteger randomNumber = arc4random() % 1000000;
    timestamp += randomNumber;
    FIRStorage *storage = [FIRStorage storage];
    FIRStorageReference *reference = [[storage referenceForURL:FIREBASE_STORAGE] child:Filename(PASSPORT_STORAGE, timestamp, @"jpg")];
    FIRStorageUploadTask *task = [FIRStorageUploadTask new];
    task = [reference putData:dataPicture metadata:nil completion:^(FIRStorageMetadata *metadata, NSError *error)
            {
                [hud hideAnimated:YES];
                [task removeAllObservers];
                if (error == nil)
                {
                    NSString *linkPicture = metadata.downloadURL.absoluteString;
                    RequModel *requModel = [RequModel new];
                    NSString *fileName = [@(timestamp) stringValue];
                    requModel.fileName = [NSString stringWithFormat:@"%@.jpg",fileName];
                    requModel.url = linkPicture;
                    requModel.wasEmailed = NO;
                    [self saveIDMPhotosWithUrl:linkPicture];
                    NSMutableArray *passports = USERINS.myrequestpassports;
                    [passports addObject:[requModel toDictionary]];
                    FUser *user = [FUser currentUser];
                    user[FUSER_MYREQUESTPASSPORTS] = passports;
                    [user saveInBackground:^(NSError * _Nullable error) {
                        [self refreshCollectionV];
                        NSMutableArray *images = [infos mutableCopy];
                        [images removeObjectAtIndex:0];
                        if (images.count) {
                            [self uploadImages:images];
                        }
                    }];
                }
                else [ProgressHUD showError:error.description];
            }];
    
    [task observeStatus:FIRStorageTaskStatusProgress handler:^(FIRStorageTaskSnapshot *snapshot)
     {
         hud.progress = (float) snapshot.progress.completedUnitCount / (float) snapshot.progress.totalUnitCount;
     }];
}

-(void)deletImageFromFireStorage: (NSInteger)index
{
    // Delete From Firebase Storage
    FIRStorage *storage = [FIRStorage storage];
    NSError *error;
    RequModel *requModel = [[RequModel alloc] initWithDictionary:USERINS.myrequestpassports[index] error:&error];
    NSString *childPath = [NSString stringWithFormat:@"%@/%@/%@",USERINS.objectId,PASSPORT_STORAGE,requModel.fileName] ;
    [ProgressHUD show:@"Deleting..." Interaction:NO];
    
    FIRStorageReference *reference = [[storage referenceForURL:FIREBASE_STORAGE] child:childPath];
    [reference deleteWithCompletion:^(NSError * _Nullable error) {
        if (error) {
            [ProgressHUD showError:error.description];
        }else{
            [ProgressHUD dismiss];
            [self refreshCollectionV];
        }
    }];
}
#pragma mark - MFMailComposeViewController DelegateMethod
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultSent:{
            
            NSMutableArray *requModels = [NSMutableArray new];
            NSError *error;
            for (int i = 0; i<USERINS.myrequestpassports.count; i++) {
                RequModel *requModel = [[RequModel alloc] initWithDictionary:USERINS.myrequestpassports[i] error:&error];
                requModel.wasEmailed = YES;
                [requModels addObject:[requModel toDictionary]];
            }
            FUser *user = [FUser currentUser];
            user[FUSER_MYREQUESTPASSPORTS] = requModels;
            [user saveInBackground:^(NSError * _Nullable error) {
                [self refreshCollectionV];
                [self setUIs];
            }];
           break;
        }
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultFailed:
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}
#pragma mark - OTHERs
-(void)saveIDMPhotosWithUrl:(NSString *)photoUrl{
    if (!iDMPhotos) {
        iDMPhotos = [[NSMutableArray alloc] init];
    }
    IDMPhoto *photo = [IDMPhoto photoWithURL:[NSURL URLWithString:photoUrl]];
    [iDMPhotos addObject:photo];
}
-(void)deleteIDMPhotosWithUrl:(NSString *)photoUrl{
    if (!iDMPhotos) {
        return;
    }
    IDMPhoto *photo = [IDMPhoto photoWithURL:[NSURL URLWithString:photoUrl]];
    [iDMPhotos removeObject:photo];
}
-(int)getWasEmailedRequsestPassportCount{
    int emailedCount = 0;
    NSError *error;
    for (int i = 0; i < USERINS.myrequestpassports.count; i++) {
        RequModel *requModel = [[RequModel alloc] initWithDictionary:USERINS.myrequestpassports[1] error:&error];
        if (requModel.wasEmailed) {
            emailedCount++;
        }
    }
    return emailedCount;
}
@end
