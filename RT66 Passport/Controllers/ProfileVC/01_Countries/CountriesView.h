

#import "utilities.h"
@protocol CountriesDelegate

- (void)didSelectCountry:(NSString *)country CountryCode:(NSString *)countryCode;

@end

@interface CountriesView : UIViewController

@property (nonatomic, assign) IBOutlet id<CountriesDelegate>delegate;
@property (weak, nonatomic) IBOutlet UISearchBar *searchbar;
@property (weak, nonatomic) IBOutlet UITableView *tableV;

@end

