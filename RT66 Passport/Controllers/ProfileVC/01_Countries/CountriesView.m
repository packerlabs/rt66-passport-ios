

#import "CountriesView.h"
@interface CountriesView()<UISearchBarDelegate>
{
    RLMResults *dbCountries;
	NSMutableArray *countries;
	NSMutableArray *sections;
}
@end

@implementation CountriesView

@synthesize delegate;

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.title = SelectNationT[USERINS.userLanguage];
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:CancelT[USERINS.userLanguage]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(actionCancel)];
    [self setFontFamily:@"Maiden Orange" forView:self.view andSubViews:YES];
    
    [self loadCountries];
//	[self setObjects:countries];
    self.searchbar.delegate = self;
    [self setTranslations];
}
- (void) setTranslations{
    self.searchbar.placeholder = searchNationT[USERINS.userLanguage];
}
-(void)loadCountries{
    NSString *text = self.searchbar.text;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[c] %@",  text];
    dbCountries = [[DBCountry objectsWithPredicate:predicate] sortedResultsUsingKeyPath:@"name" ascending:YES];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [self refreshTableView];

}
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}
- (void)setObjects:(NSArray *)objects
{
	if (sections != nil) [sections removeAllObjects];
	NSInteger sectionTitlesCount = [[[UILocalizedIndexedCollation currentCollation] sectionTitles] count];
	sections = [[NSMutableArray alloc] initWithCapacity:sectionTitlesCount];
	for (NSUInteger i=0; i<sectionTitlesCount; i++)
	{
		[sections addObject:[NSMutableArray array]];
	}
	for (NSString *object in objects)
	{
		NSInteger section = [[UILocalizedIndexedCollation currentCollation] sectionForObject:object collationStringSelector:@selector(name)];
		[sections[section] addObject:object];
	}
}

#pragma mark - Refresh methods
- (void)refreshTableView
{
    [self setObjects];
    [self.tableV reloadData];
}
- (void)setObjects
{
    if (sections != nil) [sections removeAllObjects];
    NSInteger sectionTitlesCount = [[[UILocalizedIndexedCollation currentCollation] sectionTitles] count];
    sections = [[NSMutableArray alloc] initWithCapacity:sectionTitlesCount];
    for (NSUInteger i=0; i<sectionTitlesCount; i++)
    {
        [sections addObject:[NSMutableArray array]];
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------
    for (DBCountry *dbuser in dbCountries)
    {
        NSInteger section = [[UILocalizedIndexedCollation currentCollation] sectionForObject:dbuser collationStringSelector:@selector(name)];
        [sections[section] addObject:dbuser];
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------
//    labelContacts.text = [NSString stringWithFormat:@"(%ld contacts)", (long) [dbusers count]];
}
#pragma mark - User actions

- (void)actionCancel
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [sections[section] count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if ([sections[section] count] != 0)
	{
		return [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section];
	}
	else return nil;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
	return [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
	return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
	if (cell == nil) cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
	NSMutableArray *countriestemp = sections[indexPath.section];
	NSDictionary *country = countriestemp[indexPath.row];
	cell.textLabel.text = country[@"name"];
	return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	if (delegate != nil)
	{
		NSMutableArray *countriestemp = sections[indexPath.section];
		NSDictionary *country = countriestemp[indexPath.row];
		[delegate didSelectCountry:country[@"name"] CountryCode:country[@"code"]];
	}
	[self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - UISearchBarDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self loadCountries];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar_
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self.searchbar setShowsCancelButton:YES animated:YES];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar_
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self.searchbar setShowsCancelButton:NO animated:YES];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar_
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    self.searchbar.text = @"";
    [self.searchbar resignFirstResponder];
    [self loadCountries];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar_
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self.searchbar resignFirstResponder];
}


@end

