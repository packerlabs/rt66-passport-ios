//
//  EditProfileVC.h
//  RT66 Passport
//
//  Created by My Star on 8/24/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
@interface EditProfileVC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *userImg;
@property (weak, nonatomic) IBOutlet UIImageView *flagImg;
@property (weak, nonatomic) IBOutlet UITextField *nameF;
@property (weak, nonatomic) IBOutlet UISwitch *notificationSw;
@property (strong, nonatomic) IBOutlet UILabel *languageL;

@property (weak, nonatomic) IBOutlet UILabel *welcomeTL;
@property (weak, nonatomic) IBOutlet UILabel *notificationTL;
@property (weak, nonatomic) IBOutlet UILabel *languageTL;
@property (weak, nonatomic) IBOutlet UIButton *surveyBtn;
@end
