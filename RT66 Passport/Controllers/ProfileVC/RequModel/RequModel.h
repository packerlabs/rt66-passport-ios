//
//  RequModel.h
//  RT66 Passport
//
//  Created by My Star on 10/30/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface RequModel : JSONModel
@property NSString *fileName;
@property NSString *url;
@property BOOL wasEmailed;
@end
