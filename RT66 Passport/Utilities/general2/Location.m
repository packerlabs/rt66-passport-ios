//
// Copyright (c) 2016 Related Code - http://relatedcode.com
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "utilities.h"

@implementation Location
@synthesize address;
//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (Location *)shared
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	static dispatch_once_t once;
	static Location *location;
    
	//---------------------------------------------------------------------------------------------------------------------------------------------
	dispatch_once(&once, ^{ location = [[Location alloc] init]; });
	//---------------------------------------------------------------------------------------------------------------------------------------------
	return location;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (void)start
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[[self shared].locationManager startUpdatingLocation];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (void)stop
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[[self shared].locationManager stopUpdatingLocation];    
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (CLLocationDegrees)latitude
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	return [self shared].coordinate.latitude;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (CLLocationDegrees)longitude
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	return [self shared].coordinate.longitude;
}
+ (NSString*)address{
    return [self shared].address;
}
+ (NSString*)gpsLocation{
    return [self shared].strLocation;
}

#pragma mark - Instance methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id)init
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	self = [super init];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	self.locationManager = [[CLLocationManager alloc] init];
	[self.locationManager setDelegate:self];
	[self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
	[self.locationManager requestWhenInUseAuthorization];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	return self;
}

#pragma mark - CLLocationManagerDelegate

//---------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------   Search  Location & Address   --------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    self.coordinate = newLocation.coordinate;   
    [self getAddressFromPosition:newLocation :^(LMAddress *address){
        if (!address) {
            return;
        }
        [self setMyAddressAndLocation:address];
    } failure:^(NSError *error){
        
    }];
}
-(void)setMyAddressAndLocation:(LMAddress*)address_
{
    NSString *locality;
    NSString *country;
    NSString *fullAddress;
    NSString *state;
    NSString *strLocation;
    NSString *nomarlAddress;
    float latitu;
    float longitu;
    
    locality = address_.locality;
    country = address_.ISOcountryCode;
    fullAddress = address_.formattedAddress;
    state = address_.administrativeArea;
    latitu = address_.coordinate.latitude;
    longitu = address_.coordinate.longitude;
    strLocation = [NSString stringWithFormat:@"%f,%f",latitu,longitu];
    nomarlAddress = [[NSString stringWithFormat:@"%@ %@",locality,state] uppercaseString];
    self.address = nomarlAddress;
    self.strLocation = strLocation;
    FUser *user = [FUser currentUser];
    if (!user.objectId)    return;
    user[FUSER_ADDRESS] = nomarlAddress;
    user[FLATITUDE] = [NSString stringWithFormat:@"%f",latitu];
    user[FLONGITUDE] = [NSString stringWithFormat:@"%f",longitu];
//    [user saveInBackground:^(NSError *error)
//     {
//         [Location stop];
//     }];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	
}
# pragma mark "ReGeocoding"
-(void)getAddressFromPosition :(CLLocation *)location :(void (^)(LMAddress *))completion failure:(void (^)(NSError *))failure{
    [[LMGeocoder sharedInstance] reverseGeocodeCoordinate:location.coordinate
                                                  service:kLMGeocoderGoogleService
                                        completionHandler:^(NSArray *results, NSError *error) {
                                            
                                            if (results.count && !error) {
                                                LMAddress *address = [results firstObject];
                                                completion(address);
                                            }
                                            else {
                                                failure(error);
                                            }
                                        }];   

}

# pragma mark GOOGLE MAPS SDK
+(void)getActualAdress :(NSString *)address completion:(void (^)(LMAddress *))completion failure:(void (^)(NSError *))failure{
    [[LMGeocoder sharedInstance] geocodeAddressString:address
                                              service:(LMGeocoderService)kLMGeocoderGoogleService
                                    completionHandler:^(NSArray *results, NSError *error){
                                        
                                        
                                        if (results.count && !error)
                                        {
                                            LMAddress *address1 = [results firstObject];
                                            completion(address1);
                                        }
                                        else {
                                            failure(error);
                                        }
                                    }];
    
}

@end

