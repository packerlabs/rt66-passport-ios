

#import <UIKit/UIKit.h>
#import "DraggableView.h"
#import "utilities.h"
@interface DraggableViewBackground : UIView <DraggableViewDelegate>

//methods called in DraggableView
-(void)cardSwipedLeft:(UIView *)card;
-(void)cardSwipedRight:(UIView *)card;
- (id)initWithFrame:(CGRect)frame photoArray:(NSArray *)photos startIndex:(NSInteger)index;
@property (retain,nonatomic)NSMutableArray* allCards; //%%% the labels the cards
@end
