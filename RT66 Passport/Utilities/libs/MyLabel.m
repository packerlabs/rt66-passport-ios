//
//  MyLabel.m
//  PWFM
//
//  Created by My Star on 6/26/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "MyLabel.h"

@implementation MyLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)drawTextInRect:(CGRect)rect{
    UIEdgeInsets insets = {15, 15, 15, 15};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

@end
