

#import <Realm/Realm.h>

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface DBLocation : RLMObject
    @property NSString *objectId;
    @property NSString *name;
    @property NSString *photos;
    @property NSString *address;
    @property NSString *desc;
    @property NSString *phone;
    @property NSString *website;
    @property NSString *latitude;
    @property NSString *longitude;
    @property NSString *openHours;
    @property int sector;
    @property long order;
@end

