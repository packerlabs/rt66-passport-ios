//
//  DBHelpful.h
//  SIEclipse
//
//  Created by My Star on 8/15/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <Realm/Realm.h>
@interface DBCommunity : RLMObject
@property NSString *objectId;
@property NSString *name;
@property NSString *desc;
@property NSString *icon;
@property NSString *latitude;
@property NSString *longitude;
@end
