//
//  DBHelpful.h
//  SIEclipse
//
//  Created by My Star on 8/15/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <Realm/Realm.h>
@interface DBCountry : RLMObject
@property NSString *name;
@property NSString *code;
@property NSString *dial_code;
@end
