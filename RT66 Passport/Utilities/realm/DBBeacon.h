//
//  DBHelpful.h
//  SIEclipse
//
//  Created by My Star on 8/15/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <Realm/Realm.h>
@interface DBBeacon : RLMObject
@property NSString *objectId;
@property NSString *locationId;
@property int major;
@property int minor;
@property NSString *uuid;
@end
