
#import <Foundation/Foundation.h>

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface UserDefaults : NSObject
//-------------------------------------------------------------------------------------------------------------------------------------------------

+ (void)setObject:(id)value forKey:(NSString *)key;
+ (void)removeObjectForKey:(NSString *)key;
+ (void)removeObjectForKey:(NSString *)key afterDelay:(NSTimeInterval)delay;

+ (id)objectForKey:(NSString *)key;
+ (NSString *)stringForKey:(NSString *)key;
+ (NSInteger)integerForKey:(NSString *)key;
+ (BOOL)boolForKey:(NSString *)key;

@end

