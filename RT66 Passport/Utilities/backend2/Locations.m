

#import "utilities.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface Locations()
{
	FIRDatabaseReference *firebase;    
}
@end
//-------------------------------------------------------------------------------------------------------------------------------------------------

@implementation Locations

//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (Locations *)shared
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	static dispatch_once_t once;
	static Locations *events;
	//---------------------------------------------------------------------------------------------------------------------------------------------
	dispatch_once(&once, ^{ events = [[Locations alloc] init]; });
	//---------------------------------------------------------------------------------------------------------------------------------------------
	return events;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id)init
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	self = [super init];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[NotificationCenter addObserver:self selector:@selector(initObservers) name:NOTI_APP_STARTED];
	[NotificationCenter addObserver:self selector:@selector(initObservers) name:NOTI_USER_LOGGED_IN];
	[NotificationCenter addObserver:self selector:@selector(actionCleanup) name:NOTI_USER_LOGGED_OUT];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	
	return self;
}

#pragma mark - Backend methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)initObservers
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if ([FUser currentId] != nil)
	{
		if (firebase == nil) [self createObservers];
	}
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)createObservers
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    firebase = [[FIRDatabase database] referenceWithPath:FLOCATION_PATH];
    [firebase observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot)
     {
         if (snapshot.exists)
         {
             GLOBALINS.locationCount = [snapshot.value count];
             for (NSDictionary *location in [snapshot.value allValues])
             {
                
                 dispatch_async(dispatch_queue_create(nil, DISPATCH_QUEUE_SERIAL), ^{
                     [self updateRealm:location];
                 });
             }
         }
     }];
}
- (void)updateRealm:(NSDictionary *)location
{
    NSMutableDictionary *temp = [NSMutableDictionary dictionaryWithDictionary:location];
    if (location[FLOCATION_PHOTOS] != nil)
        temp[FLOCATION_PHOTOS] = [location[FLOCATION_PHOTOS] componentsJoinedByString:@","];
    if (location[FLOCATION_OPENHOURS] != nil)
        temp[FLOCATION_OPENHOURS] = [location[FLOCATION_OPENHOURS] componentsJoinedByString:@","];
    if (location[FLOCATION_DESC] != nil)
        temp[FLOCATION_DESC] = [location[FLOCATION_DESC] componentsJoinedByString:SENTENCEPOINT];
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [DBLocation createOrUpdateInRealm:realm withValue:temp];
    [realm commitWriteTransaction];
}

#pragma mark - Cleanup methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionCleanup
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[firebase removeAllObservers]; firebase = nil;
}
@end

