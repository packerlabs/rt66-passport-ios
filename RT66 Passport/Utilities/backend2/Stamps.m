

#import "utilities.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface Stamps()
{
	FIRDatabaseReference *firebase;    
}
@end
//-------------------------------------------------------------------------------------------------------------------------------------------------

@implementation Stamps

//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (Stamps *)shared
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	static dispatch_once_t once;
	static Stamps *events;
	//---------------------------------------------------------------------------------------------------------------------------------------------
	dispatch_once(&once, ^{ events = [[Stamps alloc] init]; });
	//---------------------------------------------------------------------------------------------------------------------------------------------
	return events;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id)init
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	self = [super init];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[NotificationCenter addObserver:self selector:@selector(initObservers) name:NOTI_APP_STARTED];
	[NotificationCenter addObserver:self selector:@selector(initObservers) name:NOTI_USER_LOGGED_IN];
	[NotificationCenter addObserver:self selector:@selector(actionCleanup) name:NOTI_USER_LOGGED_OUT];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	
	return self;
}

#pragma mark - Backend methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)initObservers
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if ([FUser currentId] != nil)
	{
		if (firebase == nil) [self createObservers];
	}
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)createObservers
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    firebase = [[FIRDatabase database] referenceWithPath:FSTAMP_PATH];
    [firebase observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot)
     {
         if (snapshot.exists)
         {
             GLOBALINS.stampCount = [snapshot.value count];
             for (NSDictionary *stamp in [snapshot.value allValues])
             {
                
                 dispatch_async(dispatch_queue_create(nil, DISPATCH_QUEUE_SERIAL), ^{
                     [self updateRealm:stamp];
                 });
             }
         }
     }];
}
- (void)updateRealm:(NSDictionary *)event
{
    NSMutableDictionary *temp = [NSMutableDictionary dictionaryWithDictionary:event];
    if (event[FSTAMP_DESC] != nil){
        temp[FSTAMP_DESC] = [event[FSTAMP_DESC] componentsJoinedByString:SENTENCEPOINT];
    }
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [DBStamp createOrUpdateInRealm:realm withValue:temp];
    [realm commitWriteTransaction];
}

#pragma mark - Cleanup methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionCleanup
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[firebase removeAllObservers]; firebase = nil;
}
@end

