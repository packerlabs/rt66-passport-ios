

#import "utilities.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface Communities()
{
	FIRDatabaseReference *firebase;    
}
@end
//-------------------------------------------------------------------------------------------------------------------------------------------------

@implementation Communities

//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (Communities *)shared
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	static dispatch_once_t once;
	static Communities *events;
	//---------------------------------------------------------------------------------------------------------------------------------------------
	dispatch_once(&once, ^{ events = [[Communities alloc] init]; });
	//---------------------------------------------------------------------------------------------------------------------------------------------
	return events;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id)init
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	self = [super init];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[NotificationCenter addObserver:self selector:@selector(initObservers) name:NOTI_APP_STARTED];
	[NotificationCenter addObserver:self selector:@selector(initObservers) name:NOTI_USER_LOGGED_IN];
	[NotificationCenter addObserver:self selector:@selector(actionCleanup) name:NOTI_USER_LOGGED_OUT];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	
	return self;
}

#pragma mark - Backend methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)initObservers
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if ([FUser currentId] != nil)
	{
		if (firebase == nil) [self createObservers];
	}
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)createObservers
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    firebase = [[FIRDatabase database] referenceWithPath:FCOMMUNITY_PATH];
    [firebase observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot)
     {
         if (snapshot.exists)
         {
             for (NSDictionary *location in [snapshot.value allValues])
             {
                
                 dispatch_async(dispatch_queue_create(nil, DISPATCH_QUEUE_SERIAL), ^{
                     [self updateRealm:location];
                 });
             }
         }
     }];
}
- (void)updateRealm:(NSDictionary *)location
{
    NSMutableDictionary *temp = [NSMutableDictionary dictionaryWithDictionary:location];
    if (location[FCOMMUNITY_DESC] != nil){
        temp[FCOMMUNITY_DESC] = [location[FCOMMUNITY_DESC] componentsJoinedByString:SENTENCEPOINT];
    }    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [DBCommunity createOrUpdateInRealm:realm withValue:temp];
    [realm commitWriteTransaction];
}

#pragma mark - Cleanup methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionCleanup
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[firebase removeAllObservers]; firebase = nil;
}
@end

