

#import "utilities.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface Beacons()
{
	FIRDatabaseReference *firebase;    
}
@end
//-------------------------------------------------------------------------------------------------------------------------------------------------

@implementation Beacons

//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (Beacons *)shared
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	static dispatch_once_t once;
	static Beacons *beacons;
	//---------------------------------------------------------------------------------------------------------------------------------------------
	dispatch_once(&once, ^{ beacons = [[Beacons alloc] init]; });
	//---------------------------------------------------------------------------------------------------------------------------------------------
	return beacons;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id)init
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	self = [super init];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[NotificationCenter addObserver:self selector:@selector(initObservers) name:NOTI_APP_STARTED];
	[NotificationCenter addObserver:self selector:@selector(initObservers) name:NOTI_USER_LOGGED_IN];
	[NotificationCenter addObserver:self selector:@selector(actionCleanup) name:NOTI_USER_LOGGED_OUT];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	
	return self;
}

#pragma mark - Backend methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)initObservers
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if ([FUser currentId] != nil)
	{
		if (firebase == nil) [self createObservers];
	}
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)createObservers
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    firebase = [[FIRDatabase database] referenceWithPath:FBEACON_PATH];
    [firebase observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot)
     {
         if (snapshot.exists)
         {
             for (NSDictionary *beacon in [snapshot.value allValues])
             {
                
                 dispatch_async(dispatch_queue_create(nil, DISPATCH_QUEUE_SERIAL), ^{
                     [self updateRealm:beacon];
                 });
             }
         }
     }];
}
- (void)updateRealm:(NSDictionary *)beacon
{
    NSMutableDictionary *temp = [NSMutableDictionary dictionaryWithDictionary:beacon];
    if (beacon[FBEACON_UUID] != nil){
        temp[FBEACON_UUID] = ((NSString *)beacon[FBEACON_UUID]).lowercaseString;
    }
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [DBBeacon createOrUpdateInRealm:realm withValue:temp];
    [realm commitWriteTransaction];
}

#pragma mark - Cleanup methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionCleanup
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[firebase removeAllObservers]; firebase = nil;
}
@end

