//
//  PhotoModel.h
//  RT66 Passport
//
//  Created by My Star on 11/2/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "utilities.h"
@interface GeofenceModel : JSONModel
@property NSString *dbLocationId;
@property NSString *dbStampId;
@property BOOL isEntered;
@property long milliSecons;
@property NSString *dateStr;
@end
