//
//  PhotoModel.h
//  RT66 Passport
//
//  Created by My Star on 11/2/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "utilities.h"
@interface PhotoModel : JSONModel
@property NSString *photoName;
@property NSString *stampId;
@property NSString *photoStr;
@property NSString *date;
@end
