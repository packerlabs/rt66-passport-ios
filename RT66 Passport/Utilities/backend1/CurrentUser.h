//
//  CurrentUser.h
//  RT66 Passport
//
//  Created by My Star on 8/7/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "utilities.h"
@interface CurrentUser : NSObject
#pragma mark - Parameter Declaration
// User Info
@property NSString *objectId;
@property NSString *fullname;
@property NSString *firstNameChar;
@property NSString *email;
@property NSString *gender;
@property NSString *photoUrl;
@property NSString *latitude;
@property NSString *longitude;
@property NSString *countrycode;
@property NSString *countryname;
@property NSMutableArray *mytrips;
@property NSMutableArray *mystamps;
@property NSMutableArray *myrequestpassports;
@property NSMutableArray *attractionphotos;
@property BOOL notificationallow;
@property int userLanguage;
// Survey
@property NSString *city;
@property NSString *state;
@property int adultnum;
@property int minornum;
@property NSString *startlocation;
@property NSString *endlocation;
@property NSMutableArray *followlocations;
@property NSMutableArray *spendnights;
@property int traveldays;
@property int numofarizona;
@property BOOL isdestination;
@property NSString *destination;
@property NSString *commentofvisiting;
#pragma mark - Functions
+ (CurrentUser *)instance;
+(void) saveUserInfo:(NSString *)key :(id)value;
-(void) getUserStatus:(NSDictionary *)dic;
@end
