//
//  CurrentUser.m
//  RT66 Passport
//
//  Created by My Star on 8/7/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "CurrentUser.h"

@implementation CurrentUser
+ (CurrentUser *)instance
{
    static CurrentUser *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [CurrentUser new];
    });
    return sharedInstance;
}
+(void)saveUserInfo:(NSString *)key :(id)value{
    FUser *user = FUser.currentUser;
    user[key] = value;
    [user saveInBackground:^(NSError * _Nullable error) {
        
    }];
}
-(void)getUserStatus:(NSDictionary *)dic{
    @try { self.objectId = (NSString *)dic[FUSER_OBJECTID];}
    @catch (NSException *e){self.objectId = @"";}
    
    @try {self.fullname = (NSString *)dic[FUSER_FULLNAME];}
    @catch (NSException *e){self.fullname = @"";}
    
    @try {self.email = (NSString *)dic[FUSER_EMAIL];}
    @catch (NSException *e){self.email = @"";}
    
    @try {self.photoUrl = (NSString *)dic[FUSER_PICTURE];}
    @catch (NSException *e){self.photoUrl = @"";}
    
    @try {self.latitude = (NSString *)dic[FLATITUDE];}
    @catch (NSException *e){self.latitude = @"";}
    
    @try {self.longitude = (NSString *)dic[FLONGITUDE];}
    @catch (NSException *e){self.longitude = @"";}
    
    @try {self.countrycode = (NSString *)dic[FUSER_COUNTRYCODE];}
    @catch (NSException *e){self.countrycode = @"";}
    
    @try {self.countryname = (NSString *)dic[FUSER_COUNTRYNAME];}
    @catch (NSException *e){self.countryname = @"";}
    
    @try {self.notificationallow = [dic[FUSER_NOTIFICATIONALLOW] boolValue];}
    @catch (NSException *e){self.notificationallow = YES;}
    
    @try {self.mytrips = [(NSArray *)dic[FUSER_MYTRIPS] mutableCopy];
        if (!self.mytrips) {self.mytrips = [NSMutableArray new];}
    }@catch (NSException *e){self.mytrips = [NSMutableArray new];}
    
    @try {self.mystamps = [(NSArray *)dic[FUSER_MYSTAMPS] mutableCopy];
        if (!self.mystamps) {self.mystamps = [NSMutableArray new];}
    }@catch (NSException *e){self.mystamps = [NSMutableArray new];}
    
    @try {self.myrequestpassports = [(NSArray *)dic[FUSER_MYREQUESTPASSPORTS] mutableCopy];
        if (!self.myrequestpassports) {self.myrequestpassports = [NSMutableArray new];}
    }@catch (NSException *e){self.myrequestpassports = [NSMutableArray new];}
    
    @try {self.attractionphotos = [dic[FUSER_ATTRACTIONPHOTOS]  mutableCopy];
        if (!self.attractionphotos) {self.attractionphotos = [NSMutableArray new];}
    }@catch (NSException *e){self.attractionphotos = [NSMutableArray new];}
    @try {self.userLanguage = [dic[FUSER_LANGUAGE] intValue];}
    @catch (NSException *e){self.userLanguage = 0;}
    // Survey
    @try {self.city = (NSString *)dic[FUSER_CITY];}
    @catch (NSException *e){self.city = @"";}
    
    @try {self.state = (NSString *)dic[FUSER_STATE];}
    @catch (NSException *e){self.state = @"";}
    
    @try {self.startlocation = (NSString *)dic[FUSER_STARTLOCATION];}
    @catch (NSException *e){self.startlocation = @"";}
    
    @try {self.endlocation = (NSString *)dic[FUSER_ENDLOCATION];}
    @catch (NSException *e){self.endlocation = @"";}
    
    @try {self.destination = (NSString *)dic[FUSER_DESTINATION];}
    @catch (NSException *e){self.destination = @"";}
    
    @try {self.commentofvisiting = (NSString *)dic[FUSER_COMMENTVISIT];}
    @catch (NSException *e){self.commentofvisiting = @"";}
    
    @try {self.isdestination = [dic[FUSER_ISDESTINATION] boolValue];}
    @catch (NSException *e){self.isdestination = YES;}
    
    @try {self.followlocations = [dic[FUSER_FOLLOWLOCATIONS]  mutableCopy];
        if (!self.followlocations) {self.followlocations = [NSMutableArray new];}
    }@catch (NSException *e){self.followlocations = [NSMutableArray new];}
    
    @try {self.spendnights = [dic[FUSER_SPENDNIGHTS]mutableCopy];}
    @catch (NSException *e){self.spendnights = [NSMutableArray new];}
    
    @try {self.adultnum = [dic[FUSER_NUM_ADULT] intValue];}
    @catch (NSException *e){self.adultnum = 0;}
    
    @try {self.minornum = [dic[FUSER_NUM_MINOR] intValue];}
    @catch (NSException *e){self.minornum = 0;}
    
    @try {self.traveldays = [dic[FUSER_TRAVELDAY] intValue];}
    @catch (NSException *e){self.traveldays = 0;}
    
    @try {self.numofarizona = [dic[FUSER_NUMOFARIZONA] intValue];}
    @catch (NSException *e){self.numofarizona = 0;}
    
}

@end
