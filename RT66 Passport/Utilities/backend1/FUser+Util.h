
#import "FUser.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface FUser (Util)
//-------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark - Class methods

+ (NSString *)fullname;
+ (NSString *)initials;
+ (NSString *)picture;
+ (NSString *)status;
+ (NSString *)loginMethod;
+ (NSString *)oneSignalId;

+ (NSInteger)keepMedia;
+ (NSInteger)networkImage;
+ (NSInteger)networkVideo;
+ (NSInteger)networkAudio;
+ (NSString *)wallpaper;

+ (BOOL)autoSaveMedia;
+ (BOOL)isOnboardOk;

#pragma mark - Instance methods

- (NSString *)initials;

@end

