
#import "AppConstant.h"

#import "FUser+Util.h"

@implementation FUser (Util)

#pragma mark - Class methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (NSString *)fullname				{	return [[FUser currentUser] fullname];					}
+ (NSString *)initials				{	return [[FUser currentUser] initials];					}
+ (NSString *)picture				{	return [[FUser currentUser] picture];					}

//-------------------------------------------------------------------------------------------------------------------------------------------------

+ (BOOL)isOnboardOk					{	return [[FUser currentUser] isOnboardOk];				}
//-------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark - Instance methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSString *)fullname				{	return self[FUSER_FULLNAME];						}

- (NSString *)picture				{	return self[FUSER_PICTURE];								}



- (BOOL)isOnboardOk					{	return (self[FUSER_FULLNAME] != nil);					}




@end

